/*eslint-disable*/
export default {
  "version": "12.2.0",
  "customList": [],
  "sheetCount": 2,
  "activeSheetIndex": 0,
  "sheets": {
    "Баланс": {
      "name": "Баланс",
      "activeRow": 9,
      "activeCol": 1,
      "theme": "Office",
      "data": {
        "dataTable": {
          "0": {
            "0": {
              "value": "БАЛАНС",
              "tag": "Some Great UIID",
              "style": {
                "vAlign": 1,
                "font": "10.6667px Times"
              }
            },
            "1": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "2": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "3": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "4": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "6": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "7": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "8": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "9": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            }
          },
          "1": {
            "0": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "1": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "2": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "3": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "4": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "6": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "7": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "8": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "9": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            }
          },
          "2": {
            "0": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "1": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "2": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "3": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "4": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "6": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "7": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "8": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "9": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            }
          },
          "3": {
            "0": {
              "value": "Активы",
              "tag": "Some another great uid",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": "/OADate(42736)/",
              "style": {
                "autoFormatter": {
                  "formatCached": "M/d/yyyy"
                },
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": "%",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": "/OADate(43100)/",
              "style": {
                "autoFormatter": {
                  "formatCached": "M/d/yyyy"
                },
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": "%",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Пассивы",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": "/OADate(42736)/",
              "style": {
                "autoFormatter": {
                  "formatCached": "M/d/yyyy"
                },
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": "%",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": "/OADate(43100)/",
              "style": {
                "autoFormatter": {
                  "formatCached": "M/d/yyyy"
                },
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": "%",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "4": {
            "0": {
              "value": "Здания и сооружения",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 176838481.96,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 114.69,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 176962740.89,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 119.86,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Уставной капитал",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 127300,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0.08,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 127300,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0.09,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "5": {
            "0": {
              "value": "Машины и оборудование",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Нераспределенная прибыль",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 154055524.94,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 99.92,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 147519574.16,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 99.91,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "6": {
            "0": {
              "value": "Прочие фиксированные активы",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Прочий капитал",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "7": {
            "0": {
              "value": "Незавершенное строительство",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Незакрытый капитал",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "8": {
            "0": {
              "value": "Амортизация и резервы",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": -89907462.74,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": -58.31,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": -106004991.42,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": -71.8,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "9": {
            "0": {
              "value": "Итого нематериальные активы",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 86931019.22,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              },
              "formula": "SUM(B5:B9)"
            },
            "2": {
              "value": 56.38,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "imeMode": 1
              },
              "formula": "SUM(C5:C9)"
            },
            "3": {
              "value": 70957749.47,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "imeMode": 1
              },
              "formula": "SUM(D5:D9)"
            },
            "4": {
              "value": 48.06,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "imeMode": 1
              },
              "formula": "SUM(E5:E9)"
            },
            "5": {
              "value": "Итого собственный капитал",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 154182824.94,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "imeMode": 1
              },
              "formula": "SUM(G5:G9)"
            },
            "7": {
              "value": 100,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "imeMode": 1
              },
              "formula": "SUM(H5:H9)"
            },
            "8": {
              "value": 147646874.16,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "imeMode": 1
              },
              "formula": "SUM(I5:I9)"
            },
            "9": {
              "value": 100,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "imeMode": 1
              },
              "formula": "SUM(J5:J9)"
            }
          },
          "10": {
            "0": {
              "value": "Внутригруп. долгосроч. займы, векселя, счета к получ.",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Долгосроч. банк. ссуды и кредиты",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "11": {
            "0": {
              "value": "Инвестиции",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Долгосроч. задолжен., небанковская",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "12": {
            "0": {
              "value": "Долгосроч. ДЗ покупателей и заказчиков",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Задолжен. по пенс. и соц. отч.",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "13": {
            "0": {
              "value": "Биологические активы",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Внутригрупповая долгосрочная задолженность",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "14": {
            "0": {
              "value": "Ценные бумаги, долгосроч. векселя",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Прочие долгосрочные обязательства",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "15": {
            "0": {
              "value": "Прочие займы и долгосрочные счета к получению",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "16": {
            "0": {
              "value": "Итого финансовые активы",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Итого долгосрочные обязательства",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "17": {
            "0": {
              "value": "Сырье и матер. постав.",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 474586.96,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0.31,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 434045.53,
              "style": {
                "backColor": "rgb(255, 217, 102)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0.29,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Краткосроч. банк. ссуды и кред.",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "18": {
            "0": {
              "value": "Полуфабр. и готовая продукция",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Прочие фин. об-ва",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "19": {
            "0": {
              "value": "Незаверш. произв., невыст. сум. р-ции товаров и услуг",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Авансы полученные",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "20": {
            "0": {
              "value": "Авансы, получен. по незавер. пр-ву",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Торговая кред. зад-ность",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 251.47,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "21": {
            "0": {
              "value": "Предоплата за ТМЗ",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 38500000,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 24.97,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 42602611.47,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 28.85,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Налоговые обязательства",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": -3.78,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": -3.78,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "22": {
            "0": {
              "value": "ТМЗ резервы",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Внутригруп. кредит. зад-ность",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "23": {
            "0": {
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Проч. краткосроч. обяз-ва, отложенные",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "24": {
            "0": {
              "value": "Итого ТМЗ",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 38974586.96,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 25.28,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 43036657,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 29.15,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Итого краткосрочные обязательства",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": -3.78,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 247.69,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "25": {
            "0": {
              "value": "Торговая дебиторская задолженность",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 18800000,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 12.19,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 29300000,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 19.84,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "26": {
            "0": {
              "value": "Внутригрупповая дебиторская задолженность",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "27": {
            "0": {
              "value": "Прочие краткосрочные активы, отложенные расходы",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 3382364.94,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 2.19,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 1894544.86,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 1.28,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "28": {
            "0": {
              "value": "Денежные средства и эквиваленты",
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 6094850.04,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 3.95,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 2458170.52,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 1.66,
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "style": {
                "hAlign": 1,
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "29": {
            "0": {
              "value": "Итого ликвидные активы",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 28277214.98,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 18.34,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 33652715.38,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 22.79,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Вынесено за баланс",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "30": {
            "0": {
              "value": "Итого активы",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 154182821.16,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 100,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 147647121.85,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 100,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "Итого пассивы",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 154182821.16,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 100,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 147647121.85,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 100,
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          }
        },
        "defaultDataNode": {
          "style": {
            "themeFont": "Body"
          }
        }
      },
      "rowHeaderData": {
        "defaultDataNode": {
          "style": {
            "themeFont": "Body"
          }
        }
      },
      "colHeaderData": {
        "defaultDataNode": {
          "style": {
            "themeFont": "Body"
          }
        }
      },
      "columns": [{
        "size": 309
      }, {
        "size": 80
      }, {
        "size": 39
      }, {
        "size": 84
      }, {
        "size": 39
      }, {
        "size": 267
      }, {
        "size": 80
      }, {
        "size": 34
      }, {
        "size": 80
      }, {
        "size": 34
      }],
      "selections": {
        "0": {
          "row": 9,
          "rowCount": 1,
          "col": 1,
          "colCount": 1
        },
        "length": 1
      },
      "outlineColumnOptions": {},
      "index": 0
    },
    "ОПИУ": {
      "name": "ОПИУ",
      "activeRow": 4,
      "activeCol": 13,
      "theme": "Office",
      "data": {
        "dataTable": {
          "0": {
            "0": {
              "value": "Отчет о прибылях и убытках",
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "1": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "2": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "3": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "4": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "6": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "7": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "8": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "9": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "10": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "11": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "12": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "13": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "14": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            }
          },
          "1": {
            "0": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "1": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "2": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "3": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "4": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "6": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "7": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "8": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "9": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "10": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "11": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "12": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "13": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "14": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            }
          },
          "2": {
            "0": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "1": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "2": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "3": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "4": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "6": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "7": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "8": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "9": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "10": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "11": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "12": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "13": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            },
            "14": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial"
              }
            }
          },
          "3": {
            "0": {
              "value": "Период",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": "янв. 2017",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": "фев. 2017",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": "мар. 2017",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": "апр. 2017",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": "май 2017",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": "июнь 2017",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": "июль 2017",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": "авг. 2017",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": "сент. 2017",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": "окт. 2017",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": "ноя. 2017",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": "дек. 2017",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": "Итого",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "hAlign": 1,
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": "Среднее",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "4": {
            "0": {
              "value": "Выручка",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 13392857,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "5": {
            "0": {
              "value": "Возвраты",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "6": {
            "0": {
              "value": "Доход от реализации продукции",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 13392857,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "7": {
            "0": {
              "value": "Себестоимость реализованной продукции",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "8": {
            "0": {
              "value": "Валовая прибыль",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 13392857,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 1116071,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "9": {
            "0": {
              "value": "Маржа",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 1,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 1,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 1,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 1,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 1,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 1,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 1,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 1,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 1,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 1,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 1,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 1,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 12,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 1,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "10": {
            "0": {
              "value": "Прочие операционные доходы",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "11": {
            "0": {
              "value": "Заработная плата",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 87788,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 87788,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 87788,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 87788,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 87788,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 90486,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 87788,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 87788,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 87188,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 87788,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 90965,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 87788,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 1058731,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 88228,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "12": {
            "0": {
              "value": "Налоги",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 8691,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 430680,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 8691,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 8691,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 430679,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 8958,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 9569,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 431557,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 9504,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 9569,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 431903,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 9569,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 1798060,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 149838,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "13": {
            "0": {
              "value": "Банковские услуги",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 5545,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 3400,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 5305,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 6635,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 5195,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 3515,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 2675,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 5305,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 4755,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 4885,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 3715,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 3715,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 54645,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 4554,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "14": {
            "0": {
              "value": "Расходы по лизингу / аренде",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "15": {
            "0": {
              "value": "Транспортные расходы",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "16": {
            "0": {
              "value": "Коммунальные услуги",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "17": {
            "0": {
              "value": "Прочие операционные расходы",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 93039,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 230665,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 90166,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 79414,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 49357,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 49254,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 56043,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 56703,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 54639,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 47724,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 50914,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 89426,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 947344,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 78945,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "18": {
            "0": {
              "value": "EBITDA",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 921009,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 363538,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 924122,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 933544,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 543053,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 963858,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 959997,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 534719,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 959986,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 966105,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 538575,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 925574,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 9534078,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 794506,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "19": {
            "0": {
              "value": "Амортизация (add-back)",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 1339045,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 1339045,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 1339045,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 1339045,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 1339045,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 1343187,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 1343187,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 1343187,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 1343187,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 1343187,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 1343187,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 1343187,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 16097529,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 1341461,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "20": {
            "0": {
              "value": "EBIT",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": -418036,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": -975506,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": -414923,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": -405501,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": -795992,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": -379329,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": -383190,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": -808468,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": -383201,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": -377081,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": -804611,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": -417613,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": -6563451,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": -546954,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "21": {
            "0": {
              "value": "Доходы от финансирования",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "22": {
            "0": {
              "value": "Процентные расходы",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "23": {
            "0": {
              "value": "Доход от участия в аффилированных компаниях и инвестиций",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "24": {
            "0": {
              "value": "Прибыль (убыток) от финансовых операций",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "25": {
            "0": {
              "value": "Прибыль (убыток) от обычных операций",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": -418036,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": -975506,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": -414923,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": -405501,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": -795992,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": -379329,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": -383190,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": -808468,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": -383201,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": -377081,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": -804611,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": -417613,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": -6563451,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": -546954,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "26": {
            "0": {
              "value": "Прибыль (убыток) от чрезвычайной деятельности",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 27500,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 27500,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 2292,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "27": {
            "0": {
              "value": "Прибыль (убыток) до налогооблажения",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": -418036,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": -948006,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": -414923,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": -405501,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": -795992,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": -379329,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": -383190,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": -808468,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": -383201,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": -377081,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": -804611,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": -417613,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": -6535951,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": -544663,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "28": {
            "0": {
              "value": "Налог на прибыль и иные обязательные платежи",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "29": {
            "0": {
              "value": "Чистая прибыль (убыток) за отчетный период",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": -418036,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": -948006,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": -414923,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": -405501,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": -795992,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": -379329,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": -383190,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": -808468,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": -383201,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": -377081,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": -804611,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": -417613,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": -6535951,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": -544663,
              "style": {
                "autoFormatter": {
                  "formatCached": "#,##0"
                },
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "30": {
            "0": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "31": {
            "0": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "32": {
            "0": {
              "value": "Прочие доходы",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "33": {
            "0": {
              "value": "Прочие расходы",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "34": {
            "0": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "style": {
                "vAlign": 1,
                "font": "13.3333px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "35": {
            "0": {
              "value": "Вынесено за ОПИУ",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          },
          "36": {
            "0": {
              "value": "Сторнирование",
              "style": {
                "backColor": "rgb(189, 215, 238)",
                "vAlign": 1,
                "font": "700 12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "1": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "2": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "3": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "4": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "5": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "6": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "7": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "8": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "9": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "10": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "11": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "12": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "13": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            },
            "14": {
              "value": 0,
              "style": {
                "vAlign": 1,
                "font": "12px Arial",
                "borderLeft": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderTop": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderRight": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                },
                "borderBottom": {
                  "color": "rgb(0, 0, 0)",
                  "style": 1
                }
              }
            }
          }
        },
        "defaultDataNode": {
          "style": {
            "themeFont": "Body"
          }
        }
      },
      "rowHeaderData": {
        "defaultDataNode": {
          "style": {
            "themeFont": "Body"
          }
        }
      },
      "colHeaderData": {
        "defaultDataNode": {
          "style": {
            "themeFont": "Body"
          }
        }
      },
      "columns": [{
        "size": 383
      }, {
        "size": 58
      }, {
        "size": 61
      }, {
        "size": 60
      }, {
        "size": 58
      }, {
        "size": 57
      }, {
        "size": 66
      }, {
        "size": 66
      }, {
        "size": 57
      }, null, {
        "size": 57
      }, {
        "size": 58
      }, {
        "size": 57
      }, {
        "size": 64
      }, {
        "size": 57
      }],
      "selections": {
        "0": {
          "row": 4,
          "rowCount": 1,
          "col": 13,
          "colCount": 1
        },
        "length": 1
      },
      "outlineColumnOptions": {},
      "index": 1
    }
  }
}