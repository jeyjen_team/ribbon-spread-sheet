export default {
  components: [
    {
      name: "nginx",
      operations: [
        {
          id: 'nginx.proxy',
          type: 'MessageNode',
          model: "proxy",
          desc: "message proxy",
          nodes: [
            {
              type: "IfNode",
              desc: "1 if",
              nodes: [
                { type: "OpNode", desc: "1 op" },
                { type: "OpNode", desc: "2 op", },
                {
                  type: "IfNode",
                  desc: "2 if",
                  nodes: [
                    { type: "OpNode", desc: "3 op", },
                    { type: "OpNode", desc: "4 op", },
                    {
                      type: "IfNode",
                      nodes: [
                        { type: "OpNode" },
                        { type: "OpNode" }
                      ]
                    }
                  ]
                },
                {
                  type: "ElseIfNode",
                  nodes:[ { type: "OpNode" }, { type: "OpNode" } ]
                },
                {
                  type: "ElseNode",
                  nodes:[ { type: "OpNode" }, { type: "OpNode" } ]
                }
              ]
            },
            { type: "OpNode" },
            { type: "OpNode" },
            { type: 'CallNode', model: 'add', receiver: 'sumator',
              nodes: [
                {
                  type: 'MessageNode',
                  model: 'person',
                  nodes: [ { type: "OpNode" }]
                }
              ]
            }
          ]
        },
        {
          id: 'nginx.getImage',
          type: 'MessageNode',
          model: "getImage",
          nodes: [
            { type: "OpNode" },
            { type : "LoopNode", nodes: [{ type: "OpNode" }, { type: "OpNode" }]},
            { type: "OpNode" },
            { type: "OpNode" }
            ]
        }
      ]
    },
    {
      name: "sumator",
      operations: [
        {
          type: 'MessageNode',
          model: "add",
          nodes: [
            { type: "OpNode" },
            { type: "OpNode" },
            { type: 'CallNode', model: 'person', receiver: 'nginx'}
          ]
        }
      ]
    }
  ]
};
