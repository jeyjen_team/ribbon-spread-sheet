# Ribbon spread sheet integration

## Installation
```bash
npm install
```
## Run project
```bash
npm run serve
```

## Fonts
- [x] Bold
- [x] Italic
- [x] Underline
- [x] Double underline
- [x] Font family
- [x] Font size 
- [x] Borders
- [x] Fill color
- [x] Color
- [x] Up font size
- [x] Down fint size
- [ ] Clear style

## Numbers
- [x] Формата ячейки
- [x] Проценты
- [x] Смена разрядности [doc](https://help.grapecity.com/spread/SpreadSheets12/webframe.html#featselect.html)
- [x] Установка запятой

## Data
- [X] Sort [doc](https://help.grapecity.com/spread/SpreadSheets12/webframe.html#sorting.html) [doc2](https://help.grapecity.com/spread/SpreadSheets12/webframe.html#SpreadJS~GC.Spread.Sheets.Worksheet~sortRange.html) 
- [X] Filter [doc](https://help.grapecity.com/spread/SpreadSheets12/webframe.html#filter.html)
- [X] Clear 

## Input string
- [X] Formula editor 

## Outline
- [X] Group [doc](https://help.grapecity.com/spread/SpreadSheets12/webframe.html#grouping.html)
- [X] Ungroup
- [X] Subtotal
- [X] Show detail (expand)
- [X] Hide detail (collapse)

## Viewport
### Freeze Pane
- [X] Freeze Pane [doc](https://help.grapecity.com/spread/SpreadSheets12/webframe.html#frozen.html)
- [X] Freeze top row
- [X] Freeze first column
- [X] Freeze bottom Row
- [X] Freeze last column
- [X] Unfreeze Pane


## Paragraph
- [X] Horizontal alignment
- [X] Vertical alignment
- [X] Indent 

## Clipboard [doc](https://help.grapecity.com/spread/SpreadSheets12/webframe.html#clipboard.html)
### Paste
- [ ] Formulas 
- [ ] Values
- [ ] Formating
### Other
- [ ] Cut
- [ ] Copy [doc](https://help.grapecity.com/spread/SpreadSheets12/webframe.html#sccopy.html)

### Quick launch
- [X] Save
- [X] Undo
- [X] Redo
- [X] Copy
- [X] Paste
- [X] Cut

## Data bar
- [1](https://help.grapecity.com/spread/SpreadSheets12/webframe.html#SpreadJS~GC.Spread.Sheets.ConditionalFormatting~DataBarRule.html)
- [2](https://help.grapecity.com/spread/SpreadSheets12/webframe.html#scalerule.html)

[Работа с формулами](https://help.grapecity.com/spread/SpreadSheets12/webframe.html#formulasbuilt.html)

## Styles
## Format table
## Cell Style
* [ТЗ](https://docs.google.com/document/d/1Z6pRLlWRyClZTIexeasxSK62BAfxsU9ifrDJTafRuew/edit?ts=5d5f7512#heading=h.j99mnb6122bs)
* [Features](https://help.grapecity.com/spread/SpreadSheets12/webframe.html#features.html)
* [Example Spread Sheet](https://www.grapecity.com/demos/spread/JS/WebDesigner/content/index.html)
* [Spread Optimization](https://www.grapecity.com/blogs/optimize-spreadjs-performance-in-javascript-applications)
* [Commands](https://help.grapecity.com/spread/SpreadSheets12/webframe.html#SpreadJS~GC.Spread.Sheets.Commands.html)
* [Ribbon](https://vueribbon.com/documentation)
* [Icons](https://cdn.materialdesignicons.com/4.2.95/) [link2](https://materialdesignicons.com/)
* [Colors](https://material-ui.com/customization/color/) 