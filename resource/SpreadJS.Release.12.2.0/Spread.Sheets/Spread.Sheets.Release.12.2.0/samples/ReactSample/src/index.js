import React from 'react';
import ReactDOM from 'react-dom';
import App from './Sample'
import '@grapecity/spread-sheets/styles/gc.spread.sheets.excel2013white.css';

ReactDOM.render(<App />, document.getElementById('root'));
