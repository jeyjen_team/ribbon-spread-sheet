$(function() {
    jQuery.fx.off = true;
    // Search Items
    $('#search').on('keyup', onSearchKeyUp);

    // Toggle when click an item element
    $('.navigation').on('click', '.title', function(event) {
        var title = $(this);
        var target = $(event.target);

        if (event.target.getAttribute('href') === '#') {
            if (target.parent().hasClass('group-title')) {
                var els = title.parent().find('.title.extensible:not(.group-title)');
                if (els.eq(0).css('display') === 'none') {
                    title.parent().find('.title.extensible:not(.group-title)').css('display', 'block');
                } else {
                    title.parent().find('.title.extensible:not(.group-title)').css('display', 'none');
                    title.parent().find('.itemMembers').hide();
                }
            } else if (target.parent().hasClass('extensible')) {
                var $element = title.parent().find('.itemMembers[data-list-id=' + (title.data('list-id') || title.parent().data('name')) + ']');
                $element.toggle();
                if ($element.css('display') === 'none') {
                    title.parent().find('.link-header img').first().removeClass('open');
                } else {
                    title.parent().find('.link-header img').first().addClass('open');
                }
            }
        } else if (target.parent().hasClass('link-header') && target.parent().parent().hasClass('extensible')) {
            var $element = target.parent().parent().parent().find('.itemMembers[data-list-id=' + (title.data('list-id') || title.parent().data('name')) + ']');
            $element.toggle();
            if ($element.css('display') === 'none') {
                title.parent().find('.link-header img').first().removeClass('open');
            } else {
                title.parent().find('.link-header img').first().addClass('open');
            }
        }
        event.preventDefault();

        return false;
    });
    $('.navigation a:not(.link-header)').on('click', function() {
        setTimeout(updateNav, 100);
    });

    var $current;

    // Auto resizing on navigation
    function _onResize() {
        var height = $(window).height(),
            $el = $('.navigation');

        $el.height(height).find('.list').height(height - 85);

        // Scroll to the currently selected element
        if ($current) {
            $('.navigation').find('ul.list').first().scrollTop($current.position().top);
        }
    }
    $(window).on('resize', _onResize);
    _onResize();

    $current = updateNav(true);
});

function linkto2(name) {
    return name.replace(/\./g, '')
        .replace(/\'/g, '')
        .replace(/ /g, '');
}

function updateNav(scroll) {
    // Show an item related a current documentation automatically
    var filename = $('.page-title').data('filename').replace(/\.[a-z]+$/, '');
    var $currentItem = $('.navigation .item[data-name="' + linkto2(filename) + '"]:not(.multiple):eq(0), .navigation .item .extensible[data-name="' + linkto2(filename) + '"]:eq(0)');
    var $currentSubItem = $('.navigation .sub-item[data-name="' + linkto2(filename) + '"]:eq(0)');
    var $current;

    //get the current method element
    var urlElement = window.location.href.split('/');
    urlElement = urlElement[urlElement.length - 1].replace('.html', '');
    var $currentMethod = $currentItem.find('li[data-name="' + urlElement + '"]:eq(0)');

    if ($currentItem.length) {
        if ($currentItem.eq(0).hasClass('inner')) {
            $currentItem.eq(0).parent().find('.title.extensible:not(.group-title)').css('display', 'block');
            $currentItem.eq(0).parent().find('.itemMembers[data-list-id=' + linkto2(filename) + ']').css('display', 'block');
            $currentMethod = $currentItem.eq(0).parent().find('.itemMembers[data-list-id=' + linkto2(filename) + '] li[data-name="' + urlElement + '"]:eq(0)');
        } else {
            $currentItem
                .find('.itemMembers')
                .show();
            $currentItem.find('.link-header img').first().addClass('open');
        }
        $current = $currentItem;

    } else if ($currentSubItem.length) {
        var $parentNode = $currentSubItem.parent('.itemMembers');
        while ($parentNode.length) {
            $parentNode.show();
            $parentNode.parent().find('.link-header img').first().addClass('open');
            $parentNode = $parentNode.parents('.itemMembers');
        }
        $current = $currentSubItem;
    }
    if ($currentMethod.length) {
        $current = $currentMethod;
    }
    $('.navigation a').removeClass('active-link');

    // Add the 'active-link' class to the active page
    if ($currentSubItem.length) {
        $currentSubItem.find('a').first().addClass('active-link');
    }

    // Add the 'active-link' class to the active method
    if ($currentMethod.length) {
        $currentMethod.find('a').first().addClass('active-link');
    }

    return $current;
}

function onSearchKeyUp() {
    var value = $(this).val(),
        $el = $('.navigation'),
        $notFound = $('.sublist.not-found'),
        regexp;

    if (value) {
        regexp = new RegExp(value, 'i');
        $el.find('div.item, .itemMembers, .subheader, .sublist, .title.inner,div.content-item').hide();

        $el.find('div.content-item').each(function(i, v) {
            var $item = $(v);

            if (regexp.test($item.find('a').first().text())) {
                $item.show();

                var parent  = $item.parent();
                while(!parent.hasClass('navigation')){
                    if(parent.hasClass('item') || parent.hasClass('itemMembers')){
                        parent.show();
                    }
                    parent = parent.parent();
                }
                $item.closest('.content-item').show();
                $item.parents('.content-item').prevAll('p').first().show();
                $item.parents('.sublist').show();
            }
        });

        if ($('.sublist:not([style*="display: none"]), .tutorial:not([style*="display: none"])').length) {
            $notFound.hide();
        } else {
            $notFound.show();
        }
    } else {
        $el.find('.item, .sub-item, .itemMembers div, .subheader, .sublist').show();
        $el.find('.item .itemMembers').hide();
        $notFound.hide();
    }

    $el.find('.list').scrollTop(0);
}
