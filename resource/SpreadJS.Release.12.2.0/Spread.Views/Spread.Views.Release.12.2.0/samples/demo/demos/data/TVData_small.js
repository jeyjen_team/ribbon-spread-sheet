var data = [{
    "size": 42,
    "refreshRate": 60,
    "resolution": "1080p",
    "price": 399.49,
    "brand": "LG",
    "starsIcon": "stars-4-5",
    "starsValue": 4.5,
    "image": "./images/1.png",
    "shipToChina": true,
    "description": "LG Electronics 42LF5600 42-Inch 1080p 60Hz LED TV"

}, {
    "size": 32,
    "refreshRate": 60,
    "resolution": "720p",
    "price": 237.99,
    "brand": "Samsung",
    "starsIcon": "stars-5",
    "starsValue": 5,
    "image": "./images/2.png",
    "shipToChina": false,
    "description": "Samsung UN32J4000 32-Inch 720p LED TV"
}, {
    "size": 22,
    "refreshRate": 60,
    "resolution": "1080p",
    "price": 167.99,
    "brand": "Samsung",
    "starsIcon": "stars-4-5",
    "starsValue": 4.5,
    "image": "./images/3.png",
    "shipToChina": false,
    "description": "Samsung UN22F5000 22-Inch 1080p 60Hz LED HDTV (2013 Model)"
}, {
    "size": 42,
    "refreshRate": 60,
    "resolution": "1080p",
    "price": 499.99,
    "brand": "LG",
    "starsIcon": "stars-3",
    "starsValue": 3,
    "image": "./images/4.png",
    "shipToChina": false,
    "description": "LG 42LB5600 42-Inch TV (2014 Model)"
}];
