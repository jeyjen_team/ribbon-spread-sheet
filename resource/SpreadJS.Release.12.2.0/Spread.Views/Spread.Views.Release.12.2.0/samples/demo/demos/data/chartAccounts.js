var data = [{
    "id": 0,
    "accountName": "Assets",
    "type": "Asset",
    "commodity": "US Dollar",
    "total": 10,
    "parentID": null
}, {
    "id": 1,
    "accountName": "Checking",
    "type": "Bank",
    "commodity": "US Dollar",
    "total": 300,
    "parentID": 0
}, {
    "id": 2,
    "accountName": "Saving",
    "type": "Bank",
    "commodity": "US Dollar",
    "total": 630,
    "parentID": 0
}, {
    "id": 3,
    "accountName": "Equity",
    "type": "Equity",
    "commodity": "US Dollar",
    "total": 500,
    "parentID": null
}, {
    "id": 4,
    "accountName": "Opening Balance",
    "type": "Equity",
    "commodity": "US Dollar",
    "total": 100,
    "parentID": 3
}, {
    "id": 5,
    "accountName": "Expenses",
    "type": "Expense",
    "commodity": "US Dollar",
    "total": 250,
    "parentID": null
}, {
    "id": 6,
    "accountName": "Electricity",
    "type": "Expense",
    "commodity": "US Dollar",
    "total": 460,
    "parentID": 5
}, {
    "id": 7,
    "accountName": "Groceries",
    "type": "Expense",
    "commodity": "US Dollar",
    "total": 360,
    "parentID": 5
}, {
    "id": 8,
    "accountName": "Phone",
    "type": "Expense",
    "commodity": "US Dollar",
    "total": 120,
    "parentID": 5
}, {
    "id": 9,
    "accountName": "Rent",
    "type": "Expense",
    "commodity": "US Dollar",
    "total": 250,
    "parentID": 5
}, {
    "id": 10,
    "accountName": "Taxes",
    "type": "Expense",
    "commodity": "US Dollar",
    "total": 420,
    "parentID": 5
}, {
    "id": 11,
    "accountName": "Federal",
    "type": "Expense",
    "commodity": "US Dollar",
    "total": 120,
    "parentID": 10
}, {
    "id": 12,
    "accountName": "Medicare",
    "type": "Expense",
    "commodity": "US Dollar",
    "total": 510,
    "parentID": 10
}, {
    "id": 13,
    "accountName": "Social Security",
    "type": "Expense",
    "commodity": "US Dollar",
    "total": 150,
    "parentID": 10
}, {
    "id": 14,
    "accountName": "Income",
    "type": "Income",
    "commodity": "US Dollar",
    "total": 120,
    "parentID": null
}, {
    "id": 15,
    "accountName": "Salary",
    "type": "Income",
    "commodity": "US Dollar",
    "total": 410,
    "parentID": 14
}, {
    "id": 16,
    "accountName": "Liabilities",
    "type": "Liability",
    "commodity": "US Dollar",
    "total": 150,
    "parentID": null
}, {
    "id": 17,
    "accountName": "Visa",
    "type": "Credit Card",
    "commodity": "US Dollar",
    "total": 580,
    "parentID": 16
}];
