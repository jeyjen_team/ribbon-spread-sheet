var data = [
    {
        icon: "./images/422235_10151032573439821_1751755910_n.jpg",
        message:
            "Sorry Manchester United, but Thomas Müller 'will never leave FC Bayern München' and is set to enter talks over a new contract.",
        img: [
            {
                url: "./images/safe_image.jpg",
                name: "dsf"
            }
        ],
        created_time: "2015-07-23",
        comment_link: "",
        total_likes: 5151,
        total_comments: 716,
        total_shares: 1002,
        like: false,
        show_comment: false,
        name: "ESPN FC",
        comments: [
            {
                icon:
                    "./images/11705203_967495243271934_7689192744351291869_n.jpg",
                id: "",
                name: "Monica Wright",
                message:
                    "Not really surprised. Disappointed, but not surprised. Müller is a Bayern man to the core.",
                like: 320
            },
            {
                icon:
                    "./images/10475927_10205400743359717_1062978122411598368_n.jpg",
                id: "",
                name: "Ikenna Uba",
                message:
                    "As a United fan, one of the issues I find as a fault with my team is that they chase targets that even we as fans see as unrealistic. Unless it's all paper and media speculation, how  do we go on chasing Muller and Sergio Ramos knowing how their teams rate and need them? First they will be very very expensive then secondly are United currently faring better than Bayern and Real? I even saw with annoyance the news that United felt betrayed by Ramos. They ought to chase more realistic targets like the ones they signed and that way they get to finish their business early",
                like: 45
            }
        ],
        id: "126649677680649_126663614345922"
    },
    {
        icon: "./images/adminIcon.png",
        message: "add a test video",
        story: "---- changed his profile picture.",
        video: [
            {
                url: "./videos/small.mp4",
                name: "dsf",
                mask: "./images/small_mask.png"
            }
        ],
        created_time: "2015-07-23",
        comment_link: "",
        total_likes: 0,
        total_comments: 0,
        like: false,
        show_comment: false,
        name: "admin",
        id: "126649677680649_126663614345922"
    },
    {
        icon: "./images/10367724_670710096376504_1870387794767022053_n.png",
        message:
            "It's official: Windows 10 Free Upgrade Available in 190 Countries Today. More details: http://goo.gl/w8HEo6",
        img: [
            {
                url:
                    "./images/11250080_804150326365813_8469925214036973868_n.jpg",
                name: "windows 10"
            }
        ],
        created_time: "2015-07-23",
        comment_link: "",
        total_likes: 893,
        total_comments: 124,
        total_shares: 334,
        like: false,
        show_comment: false,
        name: "windows 10",
        comments: [
            {
                icon:
                    "./images/11755754_10153472039559450_6352765373024169501_n.jpg",
                id: "",
                name: "Michael Spicer",
                image: null,
                message:
                    "What if windows 10 is actually skynet and that's why it's free",
                like: 22
            },
            {
                icon:
                    "./images/11781630_1653208208224949_3083924032222009022_n.jpg",
                id: "",
                name: "Washim Akram ",
                message: "can i install windows 10 on my Lumia 535",
                like: 3
            },
            {
                icon:
                    "./images/11174909_1569325130007814_4064407784428092347_n.jpg",
                id: "",
                name: "Jayanta Mondal",
                message:
                    "I'm using windows 7 ultimate. To which version of windows 10 I'll be upgraded ??",
                like: 1
            },
            {
                icon:
                    "./images/11709646_1671221133099161_4931750095412417467_n.jpg",
                id: "",
                name: "Benjamin Warnes",
                message: "When is the update for Xbox one for windows 10?",
                like: 1
            },
            {
                icon:
                    "./images/10351001_847144001980909_670768594903394812_n.jpg",
                id: "",
                name: "Jim Hubbard",
                message:
                    "I cannot hear all of the good features of windows 10 over the sound of Windows phone being strangled to death in the background.",
                like: 1
            },
            {
                icon:
                    "./images/10923589_422649317885250_5703039298525449364_n.jpg",
                id: "",
                name: "Md Mujaffar",
                message: "can i install windows 10 on my Lumia 535",
                like: 1
            },
            {
                icon:
                    "./images/983861_351993501666179_3801089980485991915_n.jpg",
                id: "",
                name: "Girish Sharma",
                message:
                    "How to Upgrade windows 10 for Windows 8.1 Pro (hel me)",
                like: 0
            },
            {
                icon:
                    "./images/11264807_736812496445934_4906802356591564244_n.jpg",
                id: "",
                name: "Akash Nandi",
                message: "When we can update Windows 10 in Microsoft mobile?",
                like: 0
            }
        ],
        id: "126649677680649_126663614345922"
    },
    {
        icon: "./images/11214056_1164537530229998_387023009016067972_n.jpg",
        message:
            "Finished up training in the Shanghai stadium looking forward to playing in front of the fans here! ‪#‎halamadrid‬",
        created_time: "2015-07-23",
        comment_link: "",
        total_likes: 79561,
        total_comments: 388,
        total_shares: 108,
        like: false,
        show_comment: false,
        name: "Gareth Bale",
        img: [
            {
                url:
                    "./images/11060456_1175568005793617_1722936294722692591_n.jpg",
                name: "training"
            }
        ],
        comments: [
            {
                icon:
                    "./images/1654359_568453466592406_8167856579169666620_n.jpg",
                id: "",
                name: "Mèhdi Gàreth",
                message: "Gareth you can be the best player in the world",
                like: 107
            },
            {
                icon:
                    "./images/11200621_696876883769074_6899152148938545612_n.jpg",
                id: "",
                name: "Hawre Jamal",
                image: "",
                message: "My favorite player is gareth bale",
                like: 28
            }
        ],
        id: "126649677680649_126663614345922"
    }
];
