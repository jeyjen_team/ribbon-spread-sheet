var data = [{
        "topic": "Arrivals",
        "start": "June 1,2018 9:00:00",
        "end": "June 1,2018 20:00:00",
        "content": "All members arrive today and registration begins.",
        "speaker": "Alice"
    },

    {
        "topic": "Introductions - Name Game, Hopes, Fears & Expectations",
        "start": "June 2,2018 9:00:00",
        "end": "June 2,2018 10:30:00",
        "content": "Learn who your peers are by introducing yourselfs and playing the name game. Following that, we will discuss what you hope toget out of this conference and what concerns you have.",
        "speaker": "Steve"
    }, {
        "topic": "COFFEE BREAK",
        "start": "June 2,2018 10:30:00",
        "end": "June 2,2018 11:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "What is Permaculture & Why do we need it",
        "start": "June 2,2018 11:00:00",
        "end": "June 2,2018 12:30:00",
        "content": "Permaculture is a multi-faceted, in depth design system of agricultural and social design principles centered around simulating or directly utilizing the patterns and features observed in natural ecosystems. They will help us create appropriate, site-specific designs that are both sustainable and regenerative. ",
        "speaker": "Steve"
    }, {
        "topic": "LUNCH BREAK",
        "start": "June 2,2018 12:30:00",
        "end": "June 2,2018 13:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Permaculture Ethics & Philosophies",
        "start": "June 2,2018 13:30:00",
        "end": "June 2,2018 15:00:00",
        "content": "Permaculture principles are a list of attitudes, approaches, and actions that are practical and not system specific.",
        "speaker": "Steve"
    }, {
        "topic": "COFFEE BREAK",
        "start": "June 2,2018 15:00:00",
        "end": "June 2,2018 15:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Permaculture Principals",
        "start": "June 2,2018 15:30:00",
        "end": "June 2,2018 16:30:00",
        "content": "Permaculture was created through the synthesis of many design systems, with the emphasis on nature as the penultimate one. The creation of a modern agricultural sector with a much smaller number of large-scale industrial operations would help.",
        "speaker": "Steve"
    }, {
        "topic": "DINNER",
        "start": "June 2,2018 17:00:00",
        "end": "June 2,2018 19:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Film: Journey to planet earth",
        "start": "June 2,2018 19:00:00",
        "end": "June 2,2018 21:00:00",
        "content": "The opening episode journeys across the planet, from Pole to Pole, discovering how its seasonal journey affects the lives on earth.",
        "speaker": ""
    },


    {
        "topic": "Natural Systems & Succesion",
        "start": "June 3,2018 9:00:00",
        "end": "June 3,2018 10:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": "Steve"
    }, {
        "topic": "COFFEE BREAK",
        "start": "June 3,2018 10:30:00",
        "end": "June 3,2018 11:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Theory into Practice Site Tour",
        "start": "June 3,2018 11:00:00",
        "end": "June 3,2018 12:30:00",
        "content": "We will see our organization theories applied in the real world with a site tour of the model agricutlural system. If you expereiments work, we will begin applying this in Xi'an. After that, the order goes Raleigh, Pittsburgh, Kirkland, Sendai, Shanghai, Hanoi, Noida, and, finally Rome. The model is on site and very small, so now special travel required.",
        "speaker": "Tony"
    }, {
        "topic": "LUNCH BREAK",
        "start": "June 3,2018 12:30:00",
        "end": "June 3,2018 13:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Web of Life",
        "start": "June 3,2018 13:30:00",
        "end": "June 3,2018 15:00:00",
        "content": "Goal is to maximise the sustainability of the web of life. The creation of a modern agricultural sector with a much smaller number of large-scale industrial operations would help.The modern agriculture and forest need high precision measurement of plant leaf, in order to confirm the best measure force, it needs exactite measurement of elastic mould quantity of the plant.",
        "speaker": "Tony"
    }, {
        "topic": "COFFEE BREAK",
        "start": "June 3,2018 15:00:00",
        "end": "June 3,2018 15:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Economics",
        "start": "June 3,2018 15:30:00",
        "end": "June 3,2018 16:30:00",
        "content": "One view of economics is that it is the study of the economy. The first priority in developing a new socialist countryside is to develop modern agriculture and promote steady expansion of grain production and sustained increase in rural incomes. ",
        "speaker": "Tony"
    }, {
        "topic": "DINNER",
        "start": "June 3,2018 17:00:00",
        "end": "June 3,2018 19:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Film: Money as Debt",
        "start": "June 3,2018 19:00:00",
        "end": "June 3,2018 21:00:00",
        "content": "You can have money on paper. This film proves it.",
        "speaker": ""
    },


    {
        "topic": "Design Tools SADIMET",
        "start": "June 4,2018 9:00:00",
        "end": "June 4,2018 10:30:00",
        "content": "Design tools introduction.The modern agriculture and forest need high precision measurement of plant leaf, in order to confirm the best measure force, it needs exactite measurement of elastic mould quantity of the plant. ",
        "speaker": "Steve"
    }, {
        "topic": "COFFEE BREAK",
        "start": "June 4,2018 10:30:00",
        "end": "June 4,2018 11:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Zone & Sector Analysis",
        "start": "June 4,2018 11:00:00",
        "end": "June 4,2018 12:30:00",
        "content": "This session gives a systematic approach to surveying agricultral zones.",
        "speaker": "Steve"
    }, {
        "topic": "LUNCH BREAK",
        "start": "June 4,2018 12:30:00",
        "end": "June 4,2018 13:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners. ",
        "speaker": ""
    }, {
        "topic": "Group Dynamics",
        "start": "June 4,2018 13:30:00",
        "end": "June 4,2018 15:00:00",
        "content": "This session starts with introducing the basic characteristics of courses and then analyzes the problems in teaching groups of educated people. Corresponding creative measure and advice is provided. There is no I in team but there certainly is on in win. Steve, our speaker is a winner, but very humble. ",
        "speaker": "Steve"
    }, {
        "topic": "COFFEE BREAK",
        "start": "June 4,2018 15:00:00",
        "end": "June 4,2018 15:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Group Dynamics",
        "start": "June 4,2018 15:30:00",
        "end": "June 4,2018 16:30:00",
        "content": "This session starts with introducing the basic characteristics of courses and then analyzes the problems in teaching groups of educated people. Corresponding creative measure and advice is provided. There is no I in team but there certainly is on in win. Steve, our speaker is a winner, but very humble.  ",
        "speaker": "Steve"
    }, {
        "topic": "DINNER",
        "start": "June 4,2018 17:00:00",
        "end": "June 4,2018 19:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Film: A Farm for the Future",
        "start": "June 4,2018 19:00:00",
        "end": "June 4,2018 21:00:00",
        "content": "Modern agriculture and animal husbandry in the Netherlands lead the world in technology, facilities, education and professionalism. ",
        "speaker": ""
    },


    {
        "topic": "Soil Introduction",
        "start": "June 5,2018 9:00:00",
        "end": "June 5,2018 10:30:00",
        "content": "Permaculture principles are a list of attitudes, approaches, and actions that are practical and not system specific. We will develop modern agriculture and promote the building of a new socialist countryside.   ",
        "speaker": "Fiona"
    }, {
        "topic": "COFFEE BREAK",
        "start": "June 5,2018 10:30:00",
        "end": "June 5,2018 11:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Soil Erosion & Conservation",
        "start": "June 5,2018 11:00:00",
        "end": "June 5,2018 12:30:00",
        "content": "Soil erosion is a naturally occurring process on all land. The agents of soil erosion are water and wind, each contributing a significant amount of soil loss each year. Soil erosion may be a slow process that continues relatively unnoticed, or it may occur at an alarming rate causing serious loss of topsoil. ",
        "speaker": "Steve"
    }, {
        "topic": "LUNCH BREAK",
        "start": "June 5,2018 12:30:00",
        "end": "June 5,2018 13:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Soil & Practical Gardening",
        "start": "June 5,2018 13:30:00",
        "end": "June 5,2018 15:00:00",
        "content": "Practical gardening tips for the everyday scientist. Including tips, tricks for specific soil types and helps for an abundant harvest.",
        "speaker": "Tony"
    }, {
        "topic": "COFFEE BREAK",
        "start": "June 5,2018 15:00:00",
        "end": "June 5,2018 15:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Soil Practical Gardening",
        "start": "June 5,2018 15:30:00",
        "end": "June 5,2018 16:30:00",
        "content": "Practical gardening tips for the everyday scientist. Including tips, tricks for specific soil types and helps for an abundant harvest. ",
        "speaker": "Tony"
    }, {
        "topic": "DINNER",
        "start": "June 5,2018 17:00:00",
        "end": "June 5,2018 19:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Film: Dirt the Movie",
        "start": "June 5,2018 19:00:00",
        "end": "June 5,2018 21:00:00",
        "content": "DIRT! The Movie–narrated by Jamie Lee Curtis–brings to life the environmental, economic, social and political impact that the soil has. It shares the stories of experts from all over the world who study and are able to harness the beauty and power of a respectful and mutually beneficial relationship with soil. ",
        "speaker": ""
    },


    {
        "topic": "Climate",
        "start": "June 6,2018 9:00:00",
        "end": "June 6,2018 10:30:00",
        "content": "This session will insist on the fact that agriculture will not be confronted with unknown situations and that solutions do exist; that we must work with climate change, not against it. ",
        "speaker": "Fiona"
    }, {
        "topic": "TEA BREAK",
        "start": "June 6,2018 10:30:00",
        "end": "June 6,2018 11:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners. ",
        "speaker": ""
    }, {
        "topic": "Micro Climate",
        "start": "June 6,2018 11:00:00",
        "end": "June 6,2018 12:30:00",
        "content": "A microclimate is the climate of a small area that is different from the area around it. It may be warmer or colder, wetter or drier, or more or less prone to frosts. Microclimates may be quite small - a protected courtyard next to a building, for example, that is warmer than an exposed field nearby.",
        "speaker": "Fiona"
    }, {
        "topic": "LUNCH BREAK",
        "start": "June 6,2018 12:30:00",
        "end": "June 6,2018 13:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Design Tools - Site Surveying",
        "start": "June 6,2018 13:30:00",
        "end": "June 6,2018 15:00:00",
        "content": "VisiSite is a wireless site survey tool that creates high-quality soil maps that allow you to visualize your farm on any screen.  ",
        "speaker": "Steve"
    }, {
        "topic": "TEA BREAK",
        "start": "June 6,2018 15:00:00",
        "end": "June 6,2018 15:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Cycles of Degredation & Solutions",
        "start": "June 6,2018 15:30:00",
        "end": "June 6,2018 16:30:00",
        "content": "Soil is the earth’s fragile skin that anchors all life on Earth. It is comprised of countless species that create a dynamic and complex ecosystem and is among the most precious resources to humans. Increased demand for agriculture commodities generates incentives to convert forests and grasslands to farm fields and pastures. The transition to agriculture from natural vegetation often cannot hold onto the soil and many of these plants, such as coffee, cotton, palm oil, soybean and wheat, can actually increase soil erosion beyond the soil’s ability to maintain itself.",
        "speaker": "Tony"
    }, {
        "topic": "DINNER",
        "start": "June 6,2018 17:00:00",
        "end": "June 6,2018 19:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Film: Global Gardener 1",
        "start": "June 6,2018 19:00:00",
        "end": "June 6,2018 21:00:00",
        "content": "Global Gardener is a documentary film series about the permaculture approach to sustainable agriculture. The series was produced by Julian Russell for the Australian Broadcasting Corporation; it premiered on Australian television in 1991.",
        "speaker": ""
    },


    {
        "topic": "Trees",
        "start": "June 7,2018 9:00:00",
        "end": "June 7,2018 10:30:00",
        "content": "Trees are those big things that growing out of the ground. Go hug one..",
        "speaker": "Steve"
    }, {
        "topic": "COFFEE BREAK",
        "start": "June 7,2018 10:30:00",
        "end": "June 7,2018 11:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners. ",
        "speaker": ""
    }, {
        "topic": "Forest Systems",
        "start": "June 7,2018 11:00:00",
        "end": "June 7,2018 12:30:00",
        "content": "This is a sales pitch for Forest systems. They want you to pay them money to go play in the woods.",
        "speaker": "Steve"
    }, {
        "topic": "LUNCH BREAK",
        "start": "June 7,2018 12:30:00",
        "end": "June 7,2018 13:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Earthworks",
        "start": "June 7,2018 13:30:00",
        "end": "June 7,2018 15:00:00",
        "content": "A resource for soils and foundations, this book is a bridge between the world of geotechnical engineering and foundation design and construction activities and inspections dealing with soil conditions and building foundations. ",
        "speaker": "Tony"
    }, {
        "topic": "COFFEE BREAK",
        "start": "June 7,2018 15:00:00",
        "end": "June 7,2018 15:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Tree Planting",
        "start": "June 7,2018 15:30:00",
        "end": "June 7,2018 16:30:00",
        "content": "Snippet - Set the tree in the middle of the hole. Avoid planting the tree too deep. If the root collar sits below the top of the hole, compact some soil under the tree so that the root flare at the base of the trunk is slightly above ground level.",
        "speaker": "Tony"
    }, {
        "topic": "DINNER",
        "start": "June 7,2018 17:00:00",
        "end": "June 7,2018 19:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Film: Global Gardener 2",
        "start": "June 7,2018 19:00:00",
        "end": "June 7,2018 21:00:00",
        "content": "Global Gardener is a series of four half-hour programs looking at examples of garden in different bioregions. BILL MOLLISON is a practical visionary.  ",
        "speaker": ""
    },

    {
        "topic": "Water",
        "start": "June 8,2018 9:00:00",
        "end": "June 8,2018 10:30:00",
        "content": "Water is important. You should attend.",
        "speaker": "Fiona"
    }, {
        "topic": "COFFEE BREAK",
        "start": "June 8,2018 10:30:00",
        "end": "June 8,2018 11:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Earthworks",
        "start": "June 8,2018 11:00:00",
        "end": "June 8,2018 12:30:00",
        "content": "A resource for soils and foundations, this book is a bridge between the world of geotechnical engineering and foundation design and construction activities and inspections dealing with soil conditions and building foundations. ",
        "speaker": "Steve"
    }, {
        "topic": "LUNCH BREAK",
        "start": "June 8,2018 12:30:00",
        "end": "June 8,2018 13:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Aquaculture",
        "start": "June 8,2018 13:30:00",
        "end": "June 8,2018 15:00:00",
        "content": "Aquaculture, also known as aquafarming, is the farming of aquatic organisms such as fish, crustaceans, molluscs and aquatic plants. Aquaculture involves cultivating freshwater and saltwater populations under controlled conditions, and can be contrasted with commercial fishing, which is the harvesting of wild fish.",
        "speaker": "Steve"
    }, {
        "topic": "TEA BREAK",
        "start": "June 8,2018 15:00:00",
        "end": "June 8,2018 15:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Review",
        "start": "June 8,2018 15:30:00",
        "end": "June 8,2018 16:30:00",
        "content": "Tony is going to give a really boring review of what has happend so far. Probably a good time to take a walk.",
        "speaker": "Tony"
    }, {
        "topic": "DINNER",
        "start": "June 8,2018 17:00:00",
        "end": "June 8,2018 19:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Film: Global Gardener 3",
        "start": "June 8,2018 19:00:00",
        "end": "June 8,2018 21:00:00",
        "content": "Global Gardener is a documentary film series about the permaculture approach to sustainable agriculture.",
        "speaker": ""
    },

    {
        "topic": "Herb Introduction",
        "start": "June 9,2018 9:00:00",
        "end": "June 9,2018 10:30:00",
        "content": "No, not Herb, the security gard in the lobby. This session introduces herbs that you grow.",
        "speaker": "Fiona"
    }, {
        "topic": "TEA BREAK",
        "start": "June 9,2018 10:30:00",
        "end": "June 9,2018 11:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Herb Practical",
        "start": "June 9,2018 11:00:00",
        "end": "June 9,2018 12:30:00",
        "content": "While Herb the Security Guard is a pretty practical guy, this covers how herbs are farmed in practive.",
        "speaker": "Fiona"
    }, {
        "topic": "LUNCH BREAK",
        "start": "June 9,2018 12:30:00",
        "end": "June 9,2018 13:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Afternooon Off",
        "start": "June 9,2018 13:30:00",
        "end": "June 9,2018 16:30:00",
        "content": "You are probably so bored that you are questioning your reason for being here. Take a walk in the woods.",
        "speaker": ""
    },

    {
        "topic": "DINNER",
        "start": "June 9,2018 17:00:00",
        "end": "June 9,2018 19:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Film: Global Gardener 4",
        "start": "June 9,2018 19:00:00",
        "end": "June 9,2018 21:00:00",
        "content": "Global Gardener is a documentary film series about the permaculture approach to sustainable agriculture.",
        "speaker": ""
    },

    {
        "topic": "Building Environment Buildings",
        "start": "June 10,2018 9:00:00",
        "end": "June 10,2018 10:30:00",
        "content": "Green building (also known as green construction or sustainable building) refers to both a structure and the using of processes that are environmentally responsible and resource-efficient throughout a building's life-cycle: from siting to design, construction, operation, maintenance, renovation, and demolition. In other words, green building design involves finding the balance between homebuilding and the sustainable environment. This requires close cooperation of the design team, the architects, the engineers, and the client at all project stages. The Green Building practice expands and complements the classical building design concerns of economy, utility, durability, and comfort",
        "speaker": "Steve"
    }, {
        "topic": "TEA BREAK",
        "start": "June 10,2018 10:30:00",
        "end": "June 10,2018 11:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners. ",
        "speaker": ""
    }, {
        "topic": "Design Tools Methods",
        "start": "June 10,2018 11:00:00",
        "end": "June 10,2018 12:30:00",
        "content": "There are tons of design tools out there, but do you know how to use them?",
        "speaker": "Tony"
    }, {
        "topic": "LUNCH BREAK",
        "start": "June 10,2018 12:30:00",
        "end": "June 10,2018 13:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Environmental Energy",
        "start": "June 10,2018 13:30:00",
        "end": "June 10,2018 15:00:00",
        "content": "Hippies think that they can get energy from the environment just by concentrating on it. Let us try that. ",
        "speaker": "Steve"
    }, {
        "topic": "COFFEE BREAK",
        "start": "June 10,2018 15:00:00",
        "end": "June 10,2018 15:30:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners.",
        "speaker": ""
    }, {
        "topic": "Design Groups Site Introduction - Customer Brief",
        "start": "June 10,2018 15:30:00",
        "end": "June 10,2018 16:30:00",
        "content": "This group actually did something for the customer and got paid real money. We will cover their case so you can too.",
        "speaker": "Tony"
    }, {
        "topic": "DINNER",
        "start": "June 10,2018 17:00:00",
        "end": "June 10,2018 19:00:00",
        "content": "Mingle with speakers at coffee and lunch breaks, and conference dinners. ",
        "speaker": ""
    }, {
        "topic": "Film: Spaceballs",
        "start": "June 10,2018 19:00:00",
        "end": "June 10,2018 21:00:00",
        "content": "Planet Spaceballs President Skroob sends Lord Dark Helmet to steal planet Druidia's abundant supply of air to replenish their own, and only Lone Starr can stop them.",
        "speaker": ""
    }
];
