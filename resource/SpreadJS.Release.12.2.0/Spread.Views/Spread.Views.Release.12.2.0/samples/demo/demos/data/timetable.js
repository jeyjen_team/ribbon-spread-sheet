var data = [{
        "topic": "Leaving",
        "start": "June 15,2018 9:00:00",
        "end": "June 15,2018 20:00:00",
        "speaker": ""
    },

    {
        "topic": "Introductions, Name Game. Hopes Fears & Expectations",
        "start": "June 4,2018 8:00:00",
        "end": "June 4,2018 11:30:00",
        "speaker": "Steve"
    },

    {
        "topic": "Permaculture Ethics & Philosophies",
        "start": "June 4,2018 13:30:00",
        "end": "June 4,2018 16:00:00",
        "speaker": "Steve"
    }, {
        "topic": "DINNER",
        "start": "June 4,2018 17:00:00",
        "end": "June 4,2018 20:00:00",
        "speaker": ""
    }, {
        "topic": "Soil Erosion&Conservation",
        "start": "June 6,2018 9:00:00",
        "end": "June 6,2018 15:30:00",
        "speaker": "Steve"
    }, {
        "topic": "Soil Practical Gardening",
        "start": "June 6,2018 17:30:00",
        "end": "June 6,2018 19:30:00",
        "speaker": "Tony"
    }, {
        "topic": "Micro Climate",
        "start": "June 8,2018 13:00:00",
        "end": "June 8,2018 17:30:00",
        "speaker": "Fiona"
    }, {
        "topic": "Earthworks",
        "start": "June 11,2018 9:45:00",
        "end": "June 11,2018 13:15:00",
        "speaker": "Steve"
    },

    {
        "topic": "Herb Introduction",
        "start": "June 11,2018 14:00:00",
        "end": "June 11,2018 15:30:00",
        "speaker": "Fiona"
    }, {
        "topic": "TEA BREAK",
        "start": "June 11,2018 17:30:00",
        "end": "June 11,2018 19:00:00",
        "speaker": ""
    },

    {
        "topic": "Organic intelligent algorithm introduction",
        "start": "June 14,2018 11:00:00",
        "end": "June 14,2018 19:00:00",
        "speaker": ""
    }, {
        "topic": "Built Environment Buildings",
        "start": "June 1,2018 9:00:00",
        "end": "June 1,2018 10:30:00",
        "speaker": "Steve"
    }, {
        "topic": "Short introduction",
        "start": "June 1,2018 13:30:00",
        "end": "June 1,2018 15:00:00",
        "speaker": "Steve"
    }, {
        "topic": "Design Groups Site Introduction Customer Brief",
        "start": "June 1,2018 15:30:00",
        "end": "June 1,2018 16:30:00",
        "speaker": "Tony"
    }, {
        "topic": "DINNER",
        "start": "June 1,2018 17:00:00",
        "end": "June 1,2018 19:00:00",
        "speaker": ""
    }, {
        "topic": "Film: Global Gardener 4",
        "start": "June 1,2018 19:00:00",
        "end": "June 1,2018 21:00:00",
        "speaker": ""
    }
];
