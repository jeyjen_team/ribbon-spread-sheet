var data = [{
        "firstName": "Alexa",
        "lastName": "Wilder",
        "registeredDate": "Oct 14, 2018",
        "score": 90,
        "position": "Web Developer",
        "advantage": "Very smart. He can solve problems in a short time."
    },

    {
        "firstName": "Victor",
        "lastName": "Wooten",
        "registeredDate": "May 14, 2018",
        "score": 70,
        "position": "Web Developer",
        "advantage": "Positive and optimistic, always first to work on latest technology in company."
    }, {
        "firstName": "Darrin",
        "lastName": "Sweeney",
        "registeredDate": "June 14, 2018",
        "score": 60,
        "position": ".NET Developer",
        "advantage": "Loves games day, family day, and outings."
    }, {
        "firstName": "Jen",
        "lastName": "Coulter",
        "registeredDate": "April 14, 2018",
        "score": "95",
        "position": "Admin Assistant",
        "advantage": "Very good communication skills."
    }, {
        "firstName": "Ifeoma",
        "lastName": "Mays",
        "registeredDate": "April 14,2018",
        "score": 85,
        "position": "Sales Manager",
        "advantage": "Manages his team well. They are always ready to resolve conflict."
    }, {
        "firstName": "Akeem",
        "lastName": "Carr",
        "registeredDate": "April 14, 2018",
        "score": 80,
        "position": "Sales Manager",
        "advantage": "Has experience in managing teams over 20 employees."
    }, {
        "firstName": "Buffy",
        "lastName": "Weber",
        "registeredDate": "April 25, 2018",
        "score": 80,
        "position": "Sales",
        "advantage": "Is eager to communicate with others."
    }, {
        "firstName": "Akeem",
        "lastName": "Abdul",
        "registeredDate": "April 14, 2018",
        "score": 80,
        "position": "Web Developer",
        "advantage": "Positive and optimistic, always first to work on latest technology in company."
    }, {
        "firstName": "Buffy",
        "lastName": "Carr",
        "registeredDate": "April 14, 2018",
        "score": 90,
        "position": "Web Developer",
        "advantage": "Very smart. He can solve problems quickly."
    }, {
        "firstName": "Guy",
        "lastName": "Wooten",
        "registeredDate": "July 14, 2018",
        "score": 90,
        "position": "Web Developer",
        "advantage": "A lot of experience on latest technology."
    }, {
        "firstName": "Gina",
        "lastName": "Wilder",
        "registeredDate": "Oct 14, 2018",
        "score": 70,
        "position": "Web Developer",
        "advantage": "Very smart. She can solve problems quickly."
    }, {
        "firstName": "Guy",
        "lastName": "Wooten",
        "registeredDate": "May 14, 2018",
        "score": 90,
        "position": "Sales",
        "advantage": "Is eager to communicate with others Would be good at networking."
    }, {
        "firstName": "Daryl",
        "lastName": "Sweeney",
        "registeredDate": "June 14, 2018",
        "score": 90,
        "position": "Sales",
        "advantage": "Love games day, family day, and outings."
    }, {
        "firstName": "Sally",
        "lastName": "Weber",
        "registeredDate": "April 14, 2018",
        "score": 90,
        "position": "Web Developer",
        "advantage": "Very smart. She can solve problems quickly."
    }, {
        "firstName": "Euna Lee",
        "lastName": "Mays",
        "registeredDate": "April 14,2018",
        "score": 90,
        "position": "Admin Assistant",
        "advantage": "HR department is good. They always ready to resolve confusions if any."
    }, {
        "firstName": "Akeem",
        "lastName": "Carr",
        "registeredDate": "April 14, 2018",
        "score": 60,
        "position": "Admin Assistant",
        "advantage": "Manages his team well. They are always ready to resolve conflict."
    }, {
        "firstName": "Buffy",
        "lastName": "Weber",
        "registeredDate": "April 25, 2018",
        "score": 65,
        "position": "Admin Assistant",
        "advantage": "Manages her team well. They are always ready to resolve conflict."
    }, {
        "firstName": "Aaron",
        "lastName": "Carr",
        "registeredDate": "April 14, 2018",
        "score": 60,
        "position": ".NET Developer",
        "advantage": "Very smart. He can solve problems quickly."
    }, {
        "firstName": "Buffy",
        "lastName": "Carr",
        "registeredDate": "April 14, 2018",
        "score": 70,
        "position": ".NET Developer",
        "advantage": "A lot of exposure on latest technology."
    }, {
        "firstName": "Guy",
        "lastName": "Junker",
        "registeredDate": "July 14, 2018",
        "score": 80,
        "position": ".NET Developer",
        "advantage": "A lot of exposure on latest technology."
    }
];
