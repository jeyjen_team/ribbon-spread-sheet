var data = [{
    "location": "US District",
    "dept": "Development",
    "name": "Jack",
    "BeLate": 3,
    "LeaveEarly": 1,
    "Absent": 1,
    "Leave": 2,
    "SickLeave": 1
}, {
    "location": "US District",
    "dept": "Development",
    "name": "Tom",
    "BeLate": 0,
    "LeaveEarly": 1,
    "Absent": 1,
    "Leave": 1,
    "SickLeave": 1
}, {
    "location": "US District",
    "dept": "Development",
    "name": "Tim",
    "BeLate": 4,
    "LeaveEarly": 1,
    "Absent": 2,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "US District",
    "dept": "Development",
    "name": "Paul",
    "BeLate": 1,
    "LeaveEarly": 1,
    "Absent": 1,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "US District",
    "dept": "Development",
    "name": "Lucy",
    "BeLate": 5,
    "LeaveEarly": 1,
    "Absent": 1,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "US District",
    "dept": "Personnel",
    "name": "Lily",
    "BeLate": 4,
    "LeaveEarly": 4,
    "Absent": 1,
    "Leave": 1,
    "SickLeave": 1
}, {
    "location": "US District",
    "dept": "Personnel",
    "name": "Jackson",
    "BeLate": 2,
    "LeaveEarly": 2,
    "Absent": 2,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "US District",
    "dept": "Personnel",
    "name": "Robert",
    "BeLate": 1,
    "LeaveEarly": 1,
    "Absent": 1,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "US District",
    "dept": "Personnel",
    "name": "John",
    "BeLate": 0,
    "LeaveEarly": 0,
    "Absent": 0,
    "Leave": 1,
    "SickLeave": 1
}, {
    "location": "US District",
    "dept": "Sales",
    "name": "Young",
    "BeLate": 3,
    "LeaveEarly": 0,
    "Absent": 0,
    "Leave": 1,
    "SickLeave": 1
}, {
    "location": "US District",
    "dept": "Sales",
    "name": "Rose",
    "BeLate": 1,
    "LeaveEarly": 0,
    "Absent": 0,
    "Leave": 1,
    "SickLeave": 1
}, {
    "location": "US District",
    "dept": "Sales",
    "name": "Ben",
    "BeLate": 0,
    "LeaveEarly": 0,
    "Absent": 0,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "US District",
    "dept": "Logistics",
    "name": "Ailsa",
    "BeLate": 1,
    "LeaveEarly": 0,
    "Absent": 0,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "US District",
    "dept": "Logistics",
    "name": "Selina",
    "BeLate": 0,
    "LeaveEarly": 0,
    "Absent": 0,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "US District",
    "dept": "Logistics",
    "name": "Philip",
    "BeLate": 0,
    "LeaveEarly": 0,
    "Absent": 0,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "China District",
    "dept": "Development",
    "name": "Jack",
    "BeLate": 3,
    "LeaveEarly": 1,
    "Absent": 1,
    "Leave": 2,
    "SickLeave": 1
}, {
    "location": "China District",
    "dept": "Development",
    "name": "Tom",
    "BeLate": 0,
    "LeaveEarly": 1,
    "Absent": 1,
    "Leave": 1,
    "SickLeave": 1
}, {
    "location": "China District",
    "dept": "Development",
    "name": "Tim",
    "BeLate": 4,
    "LeaveEarly": 1,
    "Absent": 2,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "China District",
    "dept": "Development",
    "name": "Paul",
    "BeLate": 1,
    "LeaveEarly": 1,
    "Absent": 1,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "China District",
    "dept": "Development",
    "name": "Lucy",
    "BeLate": 5,
    "LeaveEarly": 1,
    "Absent": 1,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "China District",
    "dept": "Personnel",
    "name": "Lily",
    "BeLate": 4,
    "LeaveEarly": 4,
    "Absent": 1,
    "Leave": 1,
    "SickLeave": 1
}, {
    "location": "China District",
    "dept": "Personnel",
    "name": "Jackson",
    "BeLate": 2,
    "LeaveEarly": 2,
    "Absent": 2,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "China District",
    "dept": "Personnel",
    "name": "Robert",
    "BeLate": 1,
    "LeaveEarly": 1,
    "Absent": 1,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "China District",
    "dept": "Personnel",
    "name": "John",
    "BeLate": 0,
    "LeaveEarly": 0,
    "Absent": 0,
    "Leave": 1,
    "SickLeave": 1
}, {
    "location": "China District",
    "dept": "Sales",
    "name": "Young",
    "BeLate": 3,
    "LeaveEarly": 0,
    "Absent": 0,
    "Leave": 1,
    "SickLeave": 1
}, {
    "location": "China District",
    "dept": "Sales",
    "name": "Rose",
    "BeLate": 1,
    "LeaveEarly": 0,
    "Absent": 0,
    "Leave": 1,
    "SickLeave": 1
}, {
    "location": "China District",
    "dept": "Sales",
    "name": "Ben",
    "BeLate": 0,
    "LeaveEarly": 0,
    "Absent": 0,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "China District",
    "dept": "Logistics",
    "name": "Ailsa",
    "BeLate": 1,
    "LeaveEarly": 0,
    "Absent": 0,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "China District",
    "dept": "Logistics",
    "name": "Selina",
    "BeLate": 0,
    "LeaveEarly": 0,
    "Absent": 0,
    "Leave": 1,
    "SickLeave": 0
}, {
    "location": "China District",
    "dept": "Logistics",
    "name": "Philip",
    "BeLate": 0,
    "LeaveEarly": 0,
    "Absent": 0,
    "Leave": 1,
    "SickLeave": 0
}];
