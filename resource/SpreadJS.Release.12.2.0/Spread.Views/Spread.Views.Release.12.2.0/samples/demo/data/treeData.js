var data = [{
    "id": 1,
    "department": "Corporate Headquarters",
    "budget": 13000,
    "location": "Monterey",
    "phone": "(408) 555-1234",
    "country": "US",
    "parent": null
}, {
    "id": 2,
    "department": "Sales and Marketing",
    "budget": 6000,
    "location": "San Francisco",
    "phone": "(408) 555-1234",
    "country": "US",
    "parent": 1
}, {
    "id": 3,
    "department": "Finance",
    "budget": 4000,
    "location": "Monterey",
    "phone": "(408) 555-1234",
    "country": "US",
    "parent": 1
}, {
    "id": 4,
    "department": "Engineering",
    "budget": 11000,
    "location": "Monterey",
    "phone": "(408) 555-1234",
    "country": "US",
    "parent": 1
}, {
    "id": 5,
    "department": "Field Office: East Coast",
    "budget": 5000,
    "location": "Toronto",
    "phone": "(416) 677-1000",
    "country": "US",
    "parent": 2
}, {
    "id": 6,
    "department": "Field Office: East Coast",
    "budget": 5000,
    "location": "Boston",
    "phone": "(408) 555-1234",
    "country": "US",
    "parent": 2
}, {
    "id": 7,
    "department": "Pacific Rim Headquarters",
    "budget": 3000,
    "location": "Kuaui",
    "phone": "(408) 555-1234",
    "country": "US",
    "parent": 2
}, {
    "id": 8,
    "department": "Marketing",
    "budget": 15000,
    "location": "San Francisco",
    "phone": "(408) 555-1234",
    "country": "US",
    "parent": 2
}, {
    "id": 9,
    "department": "Field Office: Singapore",
    "budget": 3000,
    "location": "Singapore",
    "phone": "(606) 555-5486",
    "country": "US",
    "parent": 7
}, {
    "id": 10,
    "department": "Field Office: Japan",
    "budget": 5000,
    "location": "Tokyo",
    "phone": "(707) 555-1526",
    "country": "US",
    "parent": 7
}, {
    "id": 11,
    "department": "Consumer Electronics Div",
    "budget": 11000,
    "location": "Burlington, VT",
    "phone": "(802) 555-1234",
    "country": "US",
    "parent": 4
}, {
    "id": 12,
    "department": "Software Products Div",
    "budget": 12000,
    "location": "Monterey",
    "phone": "(408) 555-1234",
    "country": "US",
    "parent": 4
}, {
    "id": 13,
    "department": "Software Development",
    "budget": 4000,
    "location": "Monterey",
    "phone": "(802) 555-1234",
    "country": "US",
    "parent": 11
}, {
    "id": 14,
    "department": "Quality Assurance",
    "budget": 4800,
    "location": "Monterey",
    "phone": "(408) 555-1234",
    "country": "US",
    "parent": 12
}, {
    "id": 15,
    "department": "Customer Support",
    "budget": 3800,
    "location": "Monterey",
    "phone": "(408) 555-1234",
    "country": "US",
    "parent": 12
}, {
    "id": 16,
    "department": "Research and Development",
    "budget": 4600,
    "location": "Burlington, VT",
    "phone": "(408) 555-1234",
    "country": "US",
    "parent": 12
}, {
    "id": 17,
    "department": "Customer Services",
    "budget": 8500,
    "location": "Burlington, VT",
    "phone": "(408) 555-1234",
    "country": "US",
    "parent": 12
}, {
    "id": 18,
    "department": "Corporate Headquarters",
    "budget": 16000,
    "location": "ShangHai",
    "phone": "(2108) 555-29321",
    "country": "China",
    "parent": null
}, {
    "id": 19,
    "department": "Sales and Marketing",
    "budget": 5000,
    "location": "Xian",
    "phone": "(2108) 555-29321",
    "country": "China",
    "parent": 18
}, {
    "id": 20,
    "department": "Finance",
    "budget": 21000,
    "location": "ShangHai",
    "phone": "(2108) 555-29321",
    "country": "China",
    "parent": 18
}, {
    "id": 21,
    "department": "Engineering",
    "budget": 10000,
    "location": "ShangHai",
    "phone": "(2108) 555-29321",
    "country": "China",
    "parent": 18
}, {
    "id": 22,
    "department": "Field Office: East Coast",
    "budget": 21000,
    "location": "Beijing",
    "phone": "(2286) 62424-1000",
    "country": "China",
    "parent": 19
}, {
    "id": 23,
    "department": "Field Office: East Coast",
    "budget": 5000,
    "location": "Boston",
    "phone": "(2108) 555-29321",
    "country": "China",
    "parent": 19
}, {
    "id": 24,
    "department": "Pacific Rim Headquarters",
    "budget": 3000,
    "location": "Kuaui",
    "phone": "(2108) 555-29321",
    "country": "China",
    "parent": 19
}, {
    "id": 25,
    "department": "Marketing",
    "budget": 15000,
    "location": "Xian",
    "phone": "(2108) 555-29321",
    "country": "China",
    "parent": 19
}, {
    "id": 26,
    "department": "Field Office: Singapore",
    "budget": 24000,
    "location": "Singapore",
    "phone": "(606) 555-52186",
    "country": "China",
    "parent": 24
}, {
    "id": 27,
    "department": "Field Office: Japan",
    "budget": 5000,
    "location": "Tokyo",
    "phone": "(24024) 555-15196",
    "country": "China",
    "parent": 24
}, {
    "id": 28,
    "department": "Consumer Electronics Div",
    "budget": 13000,
    "location": "Burlington, VT",
    "phone": "(8019) 555-29321",
    "country": "China",
    "parent": 21
}, {
    "id": 29,
    "department": "Software Products Div",
    "budget": 8000,
    "location": "ShangHai",
    "phone": "(2108) 555-29321",
    "country": "China",
    "parent": 21
}, {
    "id": 30,
    "department": "Software Development",
    "budget": 21000,
    "location": "ShangHai",
    "phone": "(8019) 555-29321",
    "country": "China",
    "parent": 28
}, {
    "id": 31,
    "department": "Quality Assurance",
    "budget": 51900,
    "location": "ShangHai",
    "phone": "(2108) 555-29321",
    "country": "China",
    "parent": 29
}, {
    "id": 32,
    "department": "CChinatomer Support",
    "budget": 32100,
    "location": "ShangHai",
    "phone": "(2108) 555-29321",
    "country": "China",
    "parent": 29
}, {
    "id": 33,
    "department": "Research and Development",
    "budget": 1800,
    "location": "Burlington, VT",
    "phone": "(2108) 555-29321",
    "country": "China",
    "parent": 29
}, {
    "id": 34,
    "department": "CChinatomer Services",
    "budget": 26600,
    "location": "Burlington, VT",
    "phone": "(2108) 555-29321",
    "country": "China",
    "parent": 29
}];
