var data = [{
    "id": 1,
    "name": "Retail Sales",
    "image": "./images/ds.jpg",
    "parent": null
}, {
    "id": 2,
    "name": "Product",
    "image": "./images/gw.jpg",
    "parent": 1
}, {
    "id": 3,
    "name": "Software",
    "image": "./images/hh.jpg",
    "sales": 1762000,
    "salesLastYear": 1600000,
    "salesTwoYearBefore": 1480300,
    "salesThreeYearBefore": 1404000,
    "parent": 2
}, {
    "id": 4,
    "name": "Hardware",
    "image": "./images/bw.jpg",
    "sales": 783000,
    "salesLastYear": 700000,
    "salesTwoYearBefore": 680000,
    "salesThreeYearBefore": 580000,
    "parent": 2
}, {
    "id": 5,
    "name": "Services",
    "image": "./images/ac.jpg",
    "parent": 1
}, {
    "id": 6,
    "name": "Consulting",
    "image": "./images/David Archer.jpg",
    "sales": 1000000,
    "salesLastYear": 900000,
    "salesTwoYearBefore": 800000,
    "salesThreeYearBefore": 700000,
    "parent": 5
}, {
    "id": 7,
    "name": "Support",
    "image": "./images/Derek Smyth.jpg",
    "sales": 240000,
    "salesLastYear": 250000,
    "salesTwoYearBefore": 160000,
    "salesThreeYearBefore": 120000,
    "parent": 5
}, {
    "id": 8,
    "name": "Design",
    "image": "./images/Greg Boudreau.jpg",
    "sales": 90000,
    "salesLastYear": 70000,
    "salesTwoYearBefore": 20000,
    "salesThreeYearBefore": 0,
    "parent": 5
}];
