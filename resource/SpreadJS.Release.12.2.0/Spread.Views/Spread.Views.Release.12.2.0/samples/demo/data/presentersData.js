var data = [
    {
        title: "Task0",
        duration: 82,
        percentComplete: 22,
        start: 43165,
        finish: 43247,
        billable: true
    },
    {
        title: "Task1",
        duration: 59,
        percentComplete: 86,
        start: 43174,
        finish: 43233,
        billable: false
    },
    {
        title: "Task2",
        duration: 10,
        percentComplete: 2,
        start: 43186,
        finish: 43196,
        billable: false
    },
    {
        title: "Task3",
        duration: 6,
        percentComplete: 66,
        start: 43408,
        finish: 43414,
        billable: false
    },
    {
        title: "Task4",
        duration: 3,
        percentComplete: 43,
        start: 43358,
        finish: 43361,
        billable: false
    },
    {
        title: "Task5",
        duration: 100,
        percentComplete: 6,
        start: 43384,
        finish: 43119,
        billable: true
    },
    {
        title: "Task6",
        duration: 69,
        percentComplete: 66,
        start: 43359,
        finish: 43428,
        billable: false
    },
    {
        title: "Task7",
        duration: 13,
        percentComplete: 17,
        start: 43344,
        finish: 43357,
        billable: false
    },
    {
        title: "Task8",
        duration: 73,
        percentComplete: 44,
        start: 43346,
        finish: 43419,
        billable: false
    },
    {
        title: "Task9",
        duration: 29,
        percentComplete: 74,
        start: 43221,
        finish: 43250,
        billable: false
    },
    {
        title: "Task10",
        duration: 51,
        percentComplete: 3,
        start: 43126,
        finish: 43176,
        billable: true
    },
    {
        title: "Task11",
        duration: 47,
        percentComplete: 9,
        start: 43155,
        finish: 43202,
        billable: false
    },
    {
        title: "Task12",
        duration: 21,
        percentComplete: 57,
        start: 43154,
        finish: 43174,
        billable: false
    },
    {
        title: "Task13",
        duration: 93,
        percentComplete: 30,
        start: 43193,
        finish: 43286,
        billable: false
    },
    {
        title: "Task14",
        duration: 37,
        percentComplete: 62,
        start: 43218,
        finish: 43255,
        billable: false
    },
    {
        title: "Task15",
        duration: 55,
        percentComplete: 42,
        start: 43442,
        finish: 43132,
        billable: true
    },
    {
        title: "Task16",
        duration: 1,
        percentComplete: 75,
        start: 43267,
        finish: 43268,
        billable: false
    },
    {
        title: "Task17",
        duration: 54,
        percentComplete: 49,
        start: 43295,
        finish: 43349,
        billable: false
    },
    {
        title: "Task18",
        duration: 70,
        percentComplete: 61,
        start: 43115,
        finish: 43185,
        billable: false
    },
    {
        title: "Task19",
        duration: 96,
        percentComplete: 97,
        start: 43127,
        finish: 43222,
        billable: false
    },
    {
        title: "Task20",
        duration: 85,
        percentComplete: 66,
        start: 43103,
        finish: 43188,
        billable: true
    },
    {
        title: "Task21",
        duration: 19,
        percentComplete: 39,
        start: 43168,
        finish: 43187,
        billable: false
    },
    {
        title: "Task22",
        duration: 1,
        percentComplete: 54,
        start: 43356,
        finish: 43357,
        billable: false
    },
    {
        title: "Task23",
        duration: 8,
        percentComplete: 61,
        start: 43140,
        finish: 43148,
        billable: false
    },
    {
        title: "Task24",
        duration: 53,
        percentComplete: 0,
        start: 43401,
        finish: 43454,
        billable: false
    },
    {
        title: "Task25",
        duration: 30,
        percentComplete: 92,
        start: 43179,
        finish: 43209,
        billable: true
    },
    {
        title: "Task26",
        duration: 54,
        percentComplete: 52,
        start: 43184,
        finish: 43238,
        billable: false
    },
    {
        title: "Task27",
        duration: 56,
        percentComplete: 37,
        start: 43108,
        finish: 43164,
        billable: false
    },
    {
        title: "Task28",
        duration: 57,
        percentComplete: 49,
        start: 43121,
        finish: 43178,
        billable: false
    },
    {
        title: "Task29",
        duration: 65,
        percentComplete: 49,
        start: 43445,
        finish: 43145,
        billable: false
    },
    {
        title: "Task30",
        duration: 99,
        percentComplete: 67,
        start: 43277,
        finish: 43376,
        billable: true
    },
    {
        title: "Task31",
        duration: 95,
        percentComplete: 76,
        start: 43195,
        finish: 43290,
        billable: false
    },
    {
        title: "Task32",
        duration: 38,
        percentComplete: 13,
        start: 43379,
        finish: 43417,
        billable: false
    },
    {
        title: "Task33",
        duration: 66,
        percentComplete: 86,
        start: 43142,
        finish: 43208,
        billable: false
    },
    {
        title: "Task34",
        duration: 32,
        percentComplete: 35,
        start: 43366,
        finish: 43398,
        billable: false
    },
    {
        title: "Task35",
        duration: 46,
        percentComplete: 99,
        start: 43268,
        finish: 43314,
        billable: true
    },
    {
        title: "Task36",
        duration: 45,
        percentComplete: 96,
        start: 43229,
        finish: 43274,
        billable: false
    },
    {
        title: "Task37",
        duration: 2,
        percentComplete: 82,
        start: 43234,
        finish: 43236,
        billable: false
    },
    {
        title: "Task38",
        duration: 87,
        percentComplete: 37,
        start: 43272,
        finish: 43359,
        billable: false
    },
    {
        title: "Task39",
        duration: 11,
        percentComplete: 89,
        start: 43321,
        finish: 43332,
        billable: false
    },
    {
        title: "Task40",
        duration: 89,
        percentComplete: 78,
        start: 43225,
        finish: 43314,
        billable: true
    },
    {
        title: "Task41",
        duration: 40,
        percentComplete: 99,
        start: 43348,
        finish: 43388,
        billable: false
    },
    {
        title: "Task42",
        duration: 44,
        percentComplete: 10,
        start: 43179,
        finish: 43223,
        billable: false
    },
    {
        title: "Task43",
        duration: 67,
        percentComplete: 78,
        start: 43162,
        finish: 43229,
        billable: false
    },
    {
        title: "Task44",
        duration: 9,
        percentComplete: 35,
        start: 43417,
        finish: 43426,
        billable: false
    },
    {
        title: "Task45",
        duration: 36,
        percentComplete: 99,
        start: 43116,
        finish: 43152,
        billable: true
    },
    {
        title: "Task46",
        duration: 35,
        percentComplete: 20,
        start: 43166,
        finish: 43201,
        billable: false
    },
    {
        title: "Task47",
        duration: 77,
        percentComplete: 28,
        start: 43326,
        finish: 43403,
        billable: false
    },
    {
        title: "Task48",
        duration: 8,
        percentComplete: 19,
        start: 43465,
        finish: 43108,
        billable: false
    },
    {
        title: "Task49",
        duration: 32,
        percentComplete: 30,
        start: 43386,
        finish: 43418,
        billable: false
    },
    {
        title: "Task50",
        duration: 37,
        percentComplete: 47,
        start: 43270,
        finish: 43307,
        billable: true
    },
    {
        title: "Task51",
        duration: 74,
        percentComplete: 99,
        start: 43217,
        finish: 43291,
        billable: false
    },
    {
        title: "Task52",
        duration: 61,
        percentComplete: 54,
        start: 43118,
        finish: 43178,
        billable: false
    },
    {
        title: "Task53",
        duration: 81,
        percentComplete: 69,
        start: 43135,
        finish: 43216,
        billable: false
    },
    {
        title: "Task54",
        duration: 38,
        percentComplete: 19,
        start: 43453,
        finish: 43126,
        billable: false
    },
    {
        title: "Task55",
        duration: 78,
        percentComplete: 28,
        start: 43121,
        finish: 43199,
        billable: true
    },
    {
        title: "Task56",
        duration: 63,
        percentComplete: 5,
        start: 43342,
        finish: 43405,
        billable: false
    },
    {
        title: "Task57",
        duration: 21,
        percentComplete: 8,
        start: 43319,
        finish: 43340,
        billable: false
    },
    {
        title: "Task58",
        duration: 88,
        percentComplete: 16,
        start: 43230,
        finish: 43318,
        billable: false
    },
    {
        title: "Task59",
        duration: 69,
        percentComplete: 46,
        start: 43217,
        finish: 43286,
        billable: false
    },
    {
        title: "Task60",
        duration: 19,
        percentComplete: 42,
        start: 43392,
        finish: 43411,
        billable: true
    },
    {
        title: "Task61",
        duration: 45,
        percentComplete: 23,
        start: 43309,
        finish: 43354,
        billable: false
    },
    {
        title: "Task62",
        duration: 73,
        percentComplete: 9,
        start: 43380,
        finish: 43453,
        billable: false
    },
    {
        title: "Task63",
        duration: 65,
        percentComplete: 94,
        start: 43353,
        finish: 43418,
        billable: false
    },
    {
        title: "Task64",
        duration: 19,
        percentComplete: 2,
        start: 43429,
        finish: 43448,
        billable: false
    },
    {
        title: "Task65",
        duration: 90,
        percentComplete: 82,
        start: 43201,
        finish: 43291,
        billable: true
    },
    {
        title: "Task66",
        duration: 59,
        percentComplete: 75,
        start: 43293,
        finish: 43352,
        billable: false
    },
    {
        title: "Task67",
        duration: 89,
        percentComplete: 65,
        start: 43117,
        finish: 43206,
        billable: false
    },
    {
        title: "Task68",
        duration: 41,
        percentComplete: 93,
        start: 43410,
        finish: 43451,
        billable: false
    },
    {
        title: "Task69",
        duration: 17,
        percentComplete: 92,
        start: 43154,
        finish: 43170,
        billable: false
    },
    {
        title: "Task70",
        duration: 32,
        percentComplete: 12,
        start: 43347,
        finish: 43379,
        billable: true
    },
    {
        title: "Task71",
        duration: 90,
        percentComplete: 76,
        start: 43452,
        finish: 43176,
        billable: false
    },
    {
        title: "Task72",
        duration: 94,
        percentComplete: 33,
        start: 43203,
        finish: 43297,
        billable: false
    },
    {
        title: "Task73",
        duration: 5,
        percentComplete: 28,
        start: 43364,
        finish: 43369,
        billable: false
    },
    {
        title: "Task74",
        duration: 45,
        percentComplete: 60,
        start: 43289,
        finish: 43334,
        billable: false
    },
    {
        title: "Task75",
        duration: 81,
        percentComplete: 29,
        start: 43462,
        finish: 43178,
        billable: true
    },
    {
        title: "Task76",
        duration: 48,
        percentComplete: 48,
        start: 43120,
        finish: 43168,
        billable: false
    },
    {
        title: "Task77",
        duration: 1,
        percentComplete: 88,
        start: 43206,
        finish: 43207,
        billable: false
    },
    {
        title: "Task78",
        duration: 92,
        percentComplete: 95,
        start: 43263,
        finish: 43355,
        billable: false
    },
    {
        title: "Task79",
        duration: 50,
        percentComplete: 63,
        start: 43296,
        finish: 43346,
        billable: false
    },
    {
        title: "Task80",
        duration: 85,
        percentComplete: 75,
        start: 43149,
        finish: 43233,
        billable: true
    },
    {
        title: "Task81",
        duration: 77,
        percentComplete: 4,
        start: 43353,
        finish: 43430,
        billable: false
    },
    {
        title: "Task82",
        duration: 14,
        percentComplete: 15,
        start: 43409,
        finish: 43423,
        billable: false
    },
    {
        title: "Task83",
        duration: 66,
        percentComplete: 30,
        start: 43393,
        finish: 43459,
        billable: false
    },
    {
        title: "Task84",
        duration: 34,
        percentComplete: 98,
        start: 43221,
        finish: 43255,
        billable: false
    },
    {
        title: "Task85",
        duration: 48,
        percentComplete: 19,
        start: 43438,
        finish: 43121,
        billable: true
    },
    {
        title: "Task86",
        duration: 26,
        percentComplete: 72,
        start: 43113,
        finish: 43139,
        billable: false
    },
    {
        title: "Task87",
        duration: 84,
        percentComplete: 48,
        start: 43367,
        finish: 43451,
        billable: false
    },
    {
        title: "Task88",
        duration: 59,
        percentComplete: 92,
        start: 43103,
        finish: 43161,
        billable: false
    },
    {
        title: "Task89",
        duration: 53,
        percentComplete: 15,
        start: 43209,
        finish: 43262,
        billable: false
    },
    {
        title: "Task90",
        duration: 96,
        percentComplete: 61,
        start: 43147,
        finish: 43242,
        billable: true
    },
    {
        title: "Task91",
        duration: 23,
        percentComplete: 37,
        start: 43330,
        finish: 43353,
        billable: false
    },
    {
        title: "Task92",
        duration: 71,
        percentComplete: 84,
        start: 43427,
        finish: 43133,
        billable: false
    },
    {
        title: "Task93",
        duration: 39,
        percentComplete: 36,
        start: 43464,
        finish: 43138,
        billable: false
    },
    {
        title: "Task94",
        duration: 97,
        percentComplete: 94,
        start: 43174,
        finish: 43271,
        billable: false
    },
    {
        title: "Task95",
        duration: 98,
        percentComplete: 87,
        start: 43395,
        finish: 43128,
        billable: true
    },
    {
        title: "Task96",
        duration: 38,
        percentComplete: 94,
        start: 43316,
        finish: 43354,
        billable: false
    },
    {
        title: "Task97",
        duration: 67,
        percentComplete: 62,
        start: 43107,
        finish: 43174,
        billable: false
    },
    {
        title: "Task98",
        duration: 15,
        percentComplete: 59,
        start: 43390,
        finish: 43405,
        billable: false
    },
    {
        title: "Task99",
        duration: 17,
        percentComplete: 78,
        start: 43158,
        finish: 43174,
        billable: false
    },
    {
        title: "Task100",
        duration: 39,
        percentComplete: 43,
        start: 43366,
        finish: 43405,
        billable: true
    },
    {
        title: "Task101",
        duration: 50,
        percentComplete: 73,
        start: 43154,
        finish: 43203,
        billable: false
    },
    {
        title: "Task102",
        duration: 58,
        percentComplete: 18,
        start: 43423,
        finish: 43116,
        billable: false
    },
    {
        title: "Task103",
        duration: 59,
        percentComplete: 42,
        start: 43222,
        finish: 43281,
        billable: false
    },
    {
        title: "Task104",
        duration: 86,
        percentComplete: 67,
        start: 43240,
        finish: 43326,
        billable: false
    },
    {
        title: "Task105",
        duration: 33,
        percentComplete: 88,
        start: 43364,
        finish: 43397,
        billable: true
    },
    {
        title: "Task106",
        duration: 95,
        percentComplete: 82,
        start: 43349,
        finish: 43444,
        billable: false
    },
    {
        title: "Task107",
        duration: 96,
        percentComplete: 56,
        start: 43352,
        finish: 43448,
        billable: false
    },
    {
        title: "Task108",
        duration: 43,
        percentComplete: 31,
        start: 43250,
        finish: 43293,
        billable: false
    },
    {
        title: "Task109",
        duration: 7,
        percentComplete: 38,
        start: 43225,
        finish: 43232,
        billable: false
    },
    {
        title: "Task110",
        duration: 57,
        percentComplete: 54,
        start: 43317,
        finish: 43374,
        billable: true
    },
    {
        title: "Task111",
        duration: 96,
        percentComplete: 96,
        start: 43345,
        finish: 43441,
        billable: false
    },
    {
        title: "Task112",
        duration: 52,
        percentComplete: 26,
        start: 43456,
        finish: 43143,
        billable: false
    },
    {
        title: "Task113",
        duration: 44,
        percentComplete: 39,
        start: 43448,
        finish: 43127,
        billable: false
    },
    {
        title: "Task114",
        duration: 88,
        percentComplete: 61,
        start: 43162,
        finish: 43250,
        billable: false
    },
    {
        title: "Task115",
        duration: 21,
        percentComplete: 98,
        start: 43263,
        finish: 43284,
        billable: true
    },
    {
        title: "Task116",
        duration: 74,
        percentComplete: 5,
        start: 43138,
        finish: 43211,
        billable: false
    },
    {
        title: "Task117",
        duration: 13,
        percentComplete: 66,
        start: 43208,
        finish: 43221,
        billable: false
    },
    {
        title: "Task118",
        duration: 97,
        percentComplete: 9,
        start: 43120,
        finish: 43217,
        billable: false
    },
    {
        title: "Task119",
        duration: 27,
        percentComplete: 8,
        start: 43175,
        finish: 43202,
        billable: false
    },
    {
        title: "Task120",
        duration: 42,
        percentComplete: 51,
        start: 43146,
        finish: 43188,
        billable: true
    },
    {
        title: "Task121",
        duration: 79,
        percentComplete: 65,
        start: 43287,
        finish: 43366,
        billable: false
    },
    {
        title: "Task122",
        duration: 8,
        percentComplete: 33,
        start: 43118,
        finish: 43126,
        billable: false
    },
    {
        title: "Task123",
        duration: 54,
        percentComplete: 61,
        start: 43395,
        finish: 43449,
        billable: false
    },
    {
        title: "Task124",
        duration: 63,
        percentComplete: 13,
        start: 43264,
        finish: 43327,
        billable: false
    },
    {
        title: "Task125",
        duration: 39,
        percentComplete: 35,
        start: 43307,
        finish: 43346,
        billable: true
    },
    {
        title: "Task126",
        duration: 32,
        percentComplete: 81,
        start: 43132,
        finish: 43164,
        billable: false
    },
    {
        title: "Task127",
        duration: 8,
        percentComplete: 80,
        start: 43456,
        finish: 43464,
        billable: false
    },
    {
        title: "Task128",
        duration: 9,
        percentComplete: 45,
        start: 43463,
        finish: 43107,
        billable: false
    },
    {
        title: "Task129",
        duration: 62,
        percentComplete: 86,
        start: 43242,
        finish: 43304,
        billable: false
    },
    {
        title: "Task130",
        duration: 31,
        percentComplete: 32,
        start: 43185,
        finish: 43216,
        billable: true
    },
    {
        title: "Task131",
        duration: 62,
        percentComplete: 87,
        start: 43374,
        finish: 43436,
        billable: false
    },
    {
        title: "Task132",
        duration: 66,
        percentComplete: 11,
        start: 43163,
        finish: 43229,
        billable: false
    },
    {
        title: "Task133",
        duration: 17,
        percentComplete: 79,
        start: 43163,
        finish: 43180,
        billable: false
    },
    {
        title: "Task134",
        duration: 56,
        percentComplete: 7,
        start: 43459,
        finish: 43150,
        billable: false
    },
    {
        title: "Task135",
        duration: 35,
        percentComplete: 14,
        start: 43305,
        finish: 43340,
        billable: true
    },
    {
        title: "Task136",
        duration: 31,
        percentComplete: 40,
        start: 43265,
        finish: 43296,
        billable: false
    },
    {
        title: "Task137",
        duration: 31,
        percentComplete: 31,
        start: 43326,
        finish: 43357,
        billable: false
    },
    {
        title: "Task138",
        duration: 73,
        percentComplete: 85,
        start: 43277,
        finish: 43350,
        billable: false
    },
    {
        title: "Task139",
        duration: 42,
        percentComplete: 85,
        start: 43378,
        finish: 43420,
        billable: false
    },
    {
        title: "Task140",
        duration: 45,
        percentComplete: 26,
        start: 43209,
        finish: 43254,
        billable: true
    },
    {
        title: "Task141",
        duration: 45,
        percentComplete: 20,
        start: 43212,
        finish: 43257,
        billable: false
    },
    {
        title: "Task142",
        duration: 93,
        percentComplete: 57,
        start: 43439,
        finish: 43167,
        billable: false
    },
    {
        title: "Task143",
        duration: 90,
        percentComplete: 28,
        start: 43172,
        finish: 43262,
        billable: false
    },
    {
        title: "Task144",
        duration: 51,
        percentComplete: 81,
        start: 43424,
        finish: 43110,
        billable: false
    },
    {
        title: "Task145",
        duration: 35,
        percentComplete: 12,
        start: 43380,
        finish: 43415,
        billable: true
    },
    {
        title: "Task146",
        duration: 2,
        percentComplete: 35,
        start: 43344,
        finish: 43346,
        billable: false
    },
    {
        title: "Task147",
        duration: 45,
        percentComplete: 74,
        start: 43400,
        finish: 43445,
        billable: false
    },
    {
        title: "Task148",
        duration: 25,
        percentComplete: 48,
        start: 43218,
        finish: 43243,
        billable: false
    },
    {
        title: "Task149",
        duration: 66,
        percentComplete: 2,
        start: 43180,
        finish: 43246,
        billable: false
    },
    {
        title: "Task150",
        duration: 72,
        percentComplete: 72,
        start: 43288,
        finish: 43360,
        billable: true
    },
    {
        title: "Task151",
        duration: 56,
        percentComplete: 50,
        start: 43363,
        finish: 43419,
        billable: false
    },
    {
        title: "Task152",
        duration: 53,
        percentComplete: 77,
        start: 43175,
        finish: 43228,
        billable: false
    },
    {
        title: "Task153",
        duration: 68,
        percentComplete: 87,
        start: 43160,
        finish: 43227,
        billable: false
    },
    {
        title: "Task154",
        duration: 12,
        percentComplete: 8,
        start: 43342,
        finish: 43354,
        billable: false
    },
    {
        title: "Task155",
        duration: 39,
        percentComplete: 86,
        start: 43135,
        finish: 43174,
        billable: true
    },
    {
        title: "Task156",
        duration: 55,
        percentComplete: 54,
        start: 43399,
        finish: 43454,
        billable: false
    },
    {
        title: "Task157",
        duration: 44,
        percentComplete: 57,
        start: 43213,
        finish: 43257,
        billable: false
    },
    {
        title: "Task158",
        duration: 45,
        percentComplete: 9,
        start: 43166,
        finish: 43211,
        billable: false
    },
    {
        title: "Task159",
        duration: 55,
        percentComplete: 99,
        start: 43151,
        finish: 43206,
        billable: false
    },
    {
        title: "Task160",
        duration: 86,
        percentComplete: 17,
        start: 43141,
        finish: 43226,
        billable: true
    },
    {
        title: "Task161",
        duration: 98,
        percentComplete: 62,
        start: 43336,
        finish: 43434,
        billable: false
    },
    {
        title: "Task162",
        duration: 99,
        percentComplete: 71,
        start: 43137,
        finish: 43236,
        billable: false
    },
    {
        title: "Task163",
        duration: 48,
        percentComplete: 41,
        start: 43119,
        finish: 43166,
        billable: false
    },
    {
        title: "Task164",
        duration: 89,
        percentComplete: 40,
        start: 43256,
        finish: 43345,
        billable: false
    },
    {
        title: "Task165",
        duration: 92,
        percentComplete: 90,
        start: 43338,
        finish: 43430,
        billable: true
    },
    {
        title: "Task166",
        duration: 48,
        percentComplete: 82,
        start: 43401,
        finish: 43449,
        billable: false
    },
    {
        title: "Task167",
        duration: 32,
        percentComplete: 71,
        start: 43108,
        finish: 43140,
        billable: false
    },
    {
        title: "Task168",
        duration: 34,
        percentComplete: 61,
        start: 43134,
        finish: 43167,
        billable: false
    },
    {
        title: "Task169",
        duration: 14,
        percentComplete: 61,
        start: 43216,
        finish: 43230,
        billable: false
    },
    {
        title: "Task170",
        duration: 75,
        percentComplete: 26,
        start: 43309,
        finish: 43384,
        billable: true
    },
    {
        title: "Task171",
        duration: 10,
        percentComplete: 33,
        start: 43165,
        finish: 43175,
        billable: false
    },
    {
        title: "Task172",
        duration: 73,
        percentComplete: 60,
        start: 43198,
        finish: 43271,
        billable: false
    },
    {
        title: "Task173",
        duration: 55,
        percentComplete: 96,
        start: 43194,
        finish: 43249,
        billable: false
    },
    {
        title: "Task174",
        duration: 76,
        percentComplete: 54,
        start: 43440,
        finish: 43151,
        billable: false
    },
    {
        title: "Task175",
        duration: 22,
        percentComplete: 75,
        start: 43435,
        finish: 43457,
        billable: true
    },
    {
        title: "Task176",
        duration: 88,
        percentComplete: 41,
        start: 43453,
        finish: 43176,
        billable: false
    },
    {
        title: "Task177",
        duration: 32,
        percentComplete: 25,
        start: 43442,
        finish: 43109,
        billable: false
    },
    {
        title: "Task178",
        duration: 50,
        percentComplete: 5,
        start: 43120,
        finish: 43170,
        billable: false
    },
    {
        title: "Task179",
        duration: 66,
        percentComplete: 81,
        start: 43163,
        finish: 43229,
        billable: false
    },
    {
        title: "Task180",
        duration: 65,
        percentComplete: 59,
        start: 43427,
        finish: 43127,
        billable: true
    },
    {
        title: "Task181",
        duration: 56,
        percentComplete: 32,
        start: 43458,
        finish: 43149,
        billable: false
    },
    {
        title: "Task182",
        duration: 78,
        percentComplete: 35,
        start: 43113,
        finish: 43190,
        billable: false
    },
    {
        title: "Task183",
        duration: 18,
        percentComplete: 22,
        start: 43166,
        finish: 43184,
        billable: false
    },
    {
        title: "Task184",
        duration: 58,
        percentComplete: 27,
        start: 43409,
        finish: 43102,
        billable: false
    },
    {
        title: "Task185",
        duration: 4,
        percentComplete: 59,
        start: 43113,
        finish: 43117,
        billable: true
    },
    {
        title: "Task186",
        duration: 68,
        percentComplete: 4,
        start: 43173,
        finish: 43241,
        billable: false
    },
    {
        title: "Task187",
        duration: 72,
        percentComplete: 35,
        start: 43450,
        finish: 43157,
        billable: false
    },
    {
        title: "Task188",
        duration: 90,
        percentComplete: 85,
        start: 43378,
        finish: 43103,
        billable: false
    },
    {
        title: "Task189",
        duration: 24,
        percentComplete: 88,
        start: 43162,
        finish: 43186,
        billable: false
    },
    {
        title: "Task190",
        duration: 27,
        percentComplete: 53,
        start: 43224,
        finish: 43251,
        billable: true
    },
    {
        title: "Task191",
        duration: 92,
        percentComplete: 62,
        start: 43345,
        finish: 43437,
        billable: false
    },
    {
        title: "Task192",
        duration: 73,
        percentComplete: 50,
        start: 43197,
        finish: 43270,
        billable: false
    },
    {
        title: "Task193",
        duration: 9,
        percentComplete: 48,
        start: 43345,
        finish: 43354,
        billable: false
    },
    {
        title: "Task194",
        duration: 43,
        percentComplete: 54,
        start: 43116,
        finish: 43159,
        billable: false
    },
    {
        title: "Task195",
        duration: 69,
        percentComplete: 21,
        start: 43317,
        finish: 43386,
        billable: true
    },
    {
        title: "Task196",
        duration: 97,
        percentComplete: 67,
        start: 43126,
        finish: 43222,
        billable: false
    },
    {
        title: "Task197",
        duration: 93,
        percentComplete: 7,
        start: 43410,
        finish: 43138,
        billable: false
    },
    {
        title: "Task198",
        duration: 61,
        percentComplete: 11,
        start: 43441,
        finish: 43137,
        billable: false
    },
    {
        title: "Task199",
        duration: 20,
        percentComplete: 22,
        start: 43336,
        finish: 43356,
        billable: false
    },
    {
        title: "Task200",
        duration: 86,
        percentComplete: 57,
        start: 43175,
        finish: 43261,
        billable: true
    },
    {
        title: "Task201",
        duration: 53,
        percentComplete: 88,
        start: 43275,
        finish: 43328,
        billable: false
    },
    {
        title: "Task202",
        duration: 99,
        percentComplete: 77,
        start: 43156,
        finish: 43254,
        billable: false
    },
    {
        title: "Task203",
        duration: 83,
        percentComplete: 92,
        start: 43414,
        finish: 43132,
        billable: false
    },
    {
        title: "Task204",
        duration: 12,
        percentComplete: 100,
        start: 43417,
        finish: 43429,
        billable: false
    },
    {
        title: "Task205",
        duration: 40,
        percentComplete: 8,
        start: 43375,
        finish: 43415,
        billable: true
    },
    {
        title: "Task206",
        duration: 55,
        percentComplete: 71,
        start: 43251,
        finish: 43306,
        billable: false
    },
    {
        title: "Task207",
        duration: 19,
        percentComplete: 80,
        start: 43207,
        finish: 43226,
        billable: false
    },
    {
        title: "Task208",
        duration: 32,
        percentComplete: 43,
        start: 43298,
        finish: 43330,
        billable: false
    },
    {
        title: "Task209",
        duration: 87,
        percentComplete: 64,
        start: 43355,
        finish: 43442,
        billable: false
    },
    {
        title: "Task210",
        duration: 27,
        percentComplete: 27,
        start: 43361,
        finish: 43388,
        billable: true
    },
    {
        title: "Task211",
        duration: 91,
        percentComplete: 63,
        start: 43159,
        finish: 43250,
        billable: false
    },
    {
        title: "Task212",
        duration: 44,
        percentComplete: 30,
        start: 43169,
        finish: 43213,
        billable: false
    },
    {
        title: "Task213",
        duration: 64,
        percentComplete: 42,
        start: 43315,
        finish: 43379,
        billable: false
    },
    {
        title: "Task214",
        duration: 23,
        percentComplete: 39,
        start: 43428,
        finish: 43451,
        billable: false
    },
    {
        title: "Task215",
        duration: 57,
        percentComplete: 99,
        start: 43443,
        finish: 43135,
        billable: true
    },
    {
        title: "Task216",
        duration: 66,
        percentComplete: 93,
        start: 43193,
        finish: 43259,
        billable: false
    },
    {
        title: "Task217",
        duration: 86,
        percentComplete: 74,
        start: 43428,
        finish: 43149,
        billable: false
    },
    {
        title: "Task218",
        duration: 48,
        percentComplete: 99,
        start: 43347,
        finish: 43395,
        billable: false
    },
    {
        title: "Task219",
        duration: 70,
        percentComplete: 7,
        start: 43236,
        finish: 43306,
        billable: false
    },
    {
        title: "Task220",
        duration: 81,
        percentComplete: 36,
        start: 43256,
        finish: 43337,
        billable: true
    },
    {
        title: "Task221",
        duration: 76,
        percentComplete: 87,
        start: 43302,
        finish: 43378,
        billable: false
    },
    {
        title: "Task222",
        duration: 90,
        percentComplete: 42,
        start: 43439,
        finish: 43164,
        billable: false
    },
    {
        title: "Task223",
        duration: 92,
        percentComplete: 77,
        start: 43457,
        finish: 43183,
        billable: false
    },
    {
        title: "Task224",
        duration: 66,
        percentComplete: 76,
        start: 43237,
        finish: 43303,
        billable: false
    },
    {
        title: "Task225",
        duration: 37,
        percentComplete: 49,
        start: 43211,
        finish: 43248,
        billable: true
    },
    {
        title: "Task226",
        duration: 63,
        percentComplete: 38,
        start: 43401,
        finish: 43464,
        billable: false
    },
    {
        title: "Task227",
        duration: 33,
        percentComplete: 17,
        start: 43221,
        finish: 43254,
        billable: false
    },
    {
        title: "Task228",
        duration: 47,
        percentComplete: 47,
        start: 43405,
        finish: 43452,
        billable: false
    },
    {
        title: "Task229",
        duration: 26,
        percentComplete: 51,
        start: 43445,
        finish: 43106,
        billable: false
    },
    {
        title: "Task230",
        duration: 4,
        percentComplete: 78,
        start: 43128,
        finish: 43132,
        billable: true
    },
    {
        title: "Task231",
        duration: 29,
        percentComplete: 90,
        start: 43174,
        finish: 43203,
        billable: false
    },
    {
        title: "Task232",
        duration: 2,
        percentComplete: 39,
        start: 43384,
        finish: 43386,
        billable: false
    },
    {
        title: "Task233",
        duration: 93,
        percentComplete: 2,
        start: 43288,
        finish: 43381,
        billable: false
    },
    {
        title: "Task234",
        duration: 84,
        percentComplete: 30,
        start: 43127,
        finish: 43211,
        billable: false
    },
    {
        title: "Task235",
        duration: 46,
        percentComplete: 87,
        start: 43126,
        finish: 43172,
        billable: true
    },
    {
        title: "Task236",
        duration: 23,
        percentComplete: 94,
        start: 43121,
        finish: 43144,
        billable: false
    },
    {
        title: "Task237",
        duration: 95,
        percentComplete: 98,
        start: 43116,
        finish: 43210,
        billable: false
    },
    {
        title: "Task238",
        duration: 17,
        percentComplete: 41,
        start: 43258,
        finish: 43275,
        billable: false
    },
    {
        title: "Task239",
        duration: 13,
        percentComplete: 87,
        start: 43220,
        finish: 43233,
        billable: false
    },
    {
        title: "Task240",
        duration: 100,
        percentComplete: 27,
        start: 43149,
        finish: 43249,
        billable: true
    },
    {
        title: "Task241",
        duration: 85,
        percentComplete: 56,
        start: 43119,
        finish: 43204,
        billable: false
    },
    {
        title: "Task242",
        duration: 67,
        percentComplete: 59,
        start: 43204,
        finish: 43271,
        billable: false
    },
    {
        title: "Task243",
        duration: 59,
        percentComplete: 85,
        start: 43131,
        finish: 43190,
        billable: false
    },
    {
        title: "Task244",
        duration: 92,
        percentComplete: 35,
        start: 43167,
        finish: 43259,
        billable: false
    },
    {
        title: "Task245",
        duration: 83,
        percentComplete: 48,
        start: 43209,
        finish: 43292,
        billable: true
    },
    {
        title: "Task246",
        duration: 17,
        percentComplete: 73,
        start: 43205,
        finish: 43222,
        billable: false
    },
    {
        title: "Task247",
        duration: 62,
        percentComplete: 62,
        start: 43421,
        finish: 43118,
        billable: false
    },
    {
        title: "Task248",
        duration: 90,
        percentComplete: 75,
        start: 43110,
        finish: 43200,
        billable: false
    },
    {
        title: "Task249",
        duration: 34,
        percentComplete: 73,
        start: 43371,
        finish: 43405,
        billable: false
    },
    {
        title: "Task250",
        duration: 53,
        percentComplete: 79,
        start: 43373,
        finish: 43426,
        billable: true
    },
    {
        title: "Task251",
        duration: 30,
        percentComplete: 82,
        start: 43444,
        finish: 43109,
        billable: false
    },
    {
        title: "Task252",
        duration: 16,
        percentComplete: 28,
        start: 43238,
        finish: 43254,
        billable: false
    },
    {
        title: "Task253",
        duration: 30,
        percentComplete: 56,
        start: 43274,
        finish: 43304,
        billable: false
    },
    {
        title: "Task254",
        duration: 63,
        percentComplete: 74,
        start: 43236,
        finish: 43299,
        billable: false
    },
    {
        title: "Task255",
        duration: 33,
        percentComplete: 49,
        start: 43382,
        finish: 43415,
        billable: true
    },
    {
        title: "Task256",
        duration: 80,
        percentComplete: 45,
        start: 43330,
        finish: 43410,
        billable: false
    },
    {
        title: "Task257",
        duration: 87,
        percentComplete: 79,
        start: 43337,
        finish: 43424,
        billable: false
    },
    {
        title: "Task258",
        duration: 32,
        percentComplete: 59,
        start: 43253,
        finish: 43285,
        billable: false
    },
    {
        title: "Task259",
        duration: 93,
        percentComplete: 40,
        start: 43452,
        finish: 43180,
        billable: false
    },
    {
        title: "Task260",
        duration: 22,
        percentComplete: 98,
        start: 43437,
        finish: 43459,
        billable: true
    },
    {
        title: "Task261",
        duration: 24,
        percentComplete: 53,
        start: 43174,
        finish: 43198,
        billable: false
    },
    {
        title: "Task262",
        duration: 100,
        percentComplete: 68,
        start: 43329,
        finish: 43429,
        billable: false
    },
    {
        title: "Task263",
        duration: 7,
        percentComplete: 98,
        start: 43144,
        finish: 43151,
        billable: false
    },
    {
        title: "Task264",
        duration: 5,
        percentComplete: 39,
        start: 43152,
        finish: 43157,
        billable: false
    },
    {
        title: "Task265",
        duration: 49,
        percentComplete: 92,
        start: 43194,
        finish: 43243,
        billable: true
    },
    {
        title: "Task266",
        duration: 38,
        percentComplete: 59,
        start: 43271,
        finish: 43309,
        billable: false
    },
    {
        title: "Task267",
        duration: 24,
        percentComplete: 23,
        start: 43436,
        finish: 43460,
        billable: false
    },
    {
        title: "Task268",
        duration: 5,
        percentComplete: 67,
        start: 43289,
        finish: 43294,
        billable: false
    },
    {
        title: "Task269",
        duration: 47,
        percentComplete: 92,
        start: 43373,
        finish: 43420,
        billable: false
    },
    {
        title: "Task270",
        duration: 22,
        percentComplete: 51,
        start: 43392,
        finish: 43414,
        billable: true
    },
    {
        title: "Task271",
        duration: 26,
        percentComplete: 42,
        start: 43322,
        finish: 43348,
        billable: false
    },
    {
        title: "Task272",
        duration: 2,
        percentComplete: 10,
        start: 43276,
        finish: 43278,
        billable: false
    },
    {
        title: "Task273",
        duration: 93,
        percentComplete: 90,
        start: 43364,
        finish: 43457,
        billable: false
    },
    {
        title: "Task274",
        duration: 72,
        percentComplete: 69,
        start: 43250,
        finish: 43322,
        billable: false
    },
    {
        title: "Task275",
        duration: 2,
        percentComplete: 99,
        start: 43391,
        finish: 43393,
        billable: true
    },
    {
        title: "Task276",
        duration: 56,
        percentComplete: 85,
        start: 43355,
        finish: 43411,
        billable: false
    },
    {
        title: "Task277",
        duration: 26,
        percentComplete: 63,
        start: 43242,
        finish: 43268,
        billable: false
    },
    {
        title: "Task278",
        duration: 44,
        percentComplete: 49,
        start: 43437,
        finish: 43116,
        billable: false
    },
    {
        title: "Task279",
        duration: 66,
        percentComplete: 36,
        start: 43142,
        finish: 43208,
        billable: false
    },
    {
        title: "Task280",
        duration: 25,
        percentComplete: 55,
        start: 43341,
        finish: 43366,
        billable: true
    },
    {
        title: "Task281",
        duration: 7,
        percentComplete: 44,
        start: 43206,
        finish: 43213,
        billable: false
    },
    {
        title: "Task282",
        duration: 3,
        percentComplete: 64,
        start: 43290,
        finish: 43293,
        billable: false
    },
    {
        title: "Task283",
        duration: 58,
        percentComplete: 48,
        start: 43425,
        finish: 43118,
        billable: false
    },
    {
        title: "Task284",
        duration: 49,
        percentComplete: 29,
        start: 43296,
        finish: 43345,
        billable: false
    },
    {
        title: "Task285",
        duration: 18,
        percentComplete: 34,
        start: 43195,
        finish: 43213,
        billable: true
    },
    {
        title: "Task286",
        duration: 46,
        percentComplete: 80,
        start: 43428,
        finish: 43109,
        billable: false
    },
    {
        title: "Task287",
        duration: 12,
        percentComplete: 26,
        start: 43335,
        finish: 43347,
        billable: false
    },
    {
        title: "Task288",
        duration: 84,
        percentComplete: 93,
        start: 43132,
        finish: 43216,
        billable: false
    },
    {
        title: "Task289",
        duration: 46,
        percentComplete: 87,
        start: 43356,
        finish: 43402,
        billable: false
    },
    {
        title: "Task290",
        duration: 66,
        percentComplete: 93,
        start: 43401,
        finish: 43102,
        billable: true
    },
    {
        title: "Task291",
        duration: 9,
        percentComplete: 54,
        start: 43233,
        finish: 43242,
        billable: false
    },
    {
        title: "Task292",
        duration: 84,
        percentComplete: 18,
        start: 43131,
        finish: 43214,
        billable: false
    },
    {
        title: "Task293",
        duration: 6,
        percentComplete: 39,
        start: 43151,
        finish: 43157,
        billable: false
    },
    {
        title: "Task294",
        duration: 46,
        percentComplete: 93,
        start: 43433,
        finish: 43114,
        billable: false
    },
    {
        title: "Task295",
        duration: 32,
        percentComplete: 8,
        start: 43211,
        finish: 43243,
        billable: true
    },
    {
        title: "Task296",
        duration: 14,
        percentComplete: 6,
        start: 43160,
        finish: 43174,
        billable: false
    },
    {
        title: "Task297",
        duration: 13,
        percentComplete: 12,
        start: 43184,
        finish: 43197,
        billable: false
    },
    {
        title: "Task298",
        duration: 81,
        percentComplete: 53,
        start: 43449,
        finish: 43164,
        billable: false
    },
    {
        title: "Task299",
        duration: 81,
        percentComplete: 56,
        start: 43272,
        finish: 43353,
        billable: false
    },
    {
        title: "Task300",
        duration: 83,
        percentComplete: 92,
        start: 43214,
        finish: 43297,
        billable: true
    },
    {
        title: "Task301",
        duration: 40,
        percentComplete: 0,
        start: 43363,
        finish: 43403,
        billable: false
    },
    {
        title: "Task302",
        duration: 3,
        percentComplete: 35,
        start: 43332,
        finish: 43335,
        billable: false
    },
    {
        title: "Task303",
        duration: 96,
        percentComplete: 41,
        start: 43300,
        finish: 43396,
        billable: false
    },
    {
        title: "Task304",
        duration: 86,
        percentComplete: 57,
        start: 43391,
        finish: 43112,
        billable: false
    },
    {
        title: "Task305",
        duration: 50,
        percentComplete: 33,
        start: 43316,
        finish: 43366,
        billable: true
    },
    {
        title: "Task306",
        duration: 34,
        percentComplete: 69,
        start: 43439,
        finish: 43108,
        billable: false
    },
    {
        title: "Task307",
        duration: 98,
        percentComplete: 43,
        start: 43214,
        finish: 43312,
        billable: false
    },
    {
        title: "Task308",
        duration: 34,
        percentComplete: 77,
        start: 43260,
        finish: 43294,
        billable: false
    },
    {
        title: "Task309",
        duration: 93,
        percentComplete: 41,
        start: 43308,
        finish: 43401,
        billable: false
    },
    {
        title: "Task310",
        duration: 75,
        percentComplete: 67,
        start: 43427,
        finish: 43137,
        billable: true
    },
    {
        title: "Task311",
        duration: 8,
        percentComplete: 60,
        start: 43160,
        finish: 43167,
        billable: false
    },
    {
        title: "Task312",
        duration: 95,
        percentComplete: 73,
        start: 43286,
        finish: 43381,
        billable: false
    },
    {
        title: "Task313",
        duration: 40,
        percentComplete: 94,
        start: 43199,
        finish: 43239,
        billable: false
    },
    {
        title: "Task314",
        duration: 89,
        percentComplete: 43,
        start: 43326,
        finish: 43415,
        billable: false
    },
    {
        title: "Task315",
        duration: 33,
        percentComplete: 35,
        start: 43103,
        finish: 43136,
        billable: true
    },
    {
        title: "Task316",
        duration: 77,
        percentComplete: 6,
        start: 43436,
        finish: 43148,
        billable: false
    },
    {
        title: "Task317",
        duration: 86,
        percentComplete: 45,
        start: 43455,
        finish: 43175,
        billable: false
    },
    {
        title: "Task318",
        duration: 56,
        percentComplete: 54,
        start: 43130,
        finish: 43186,
        billable: false
    },
    {
        title: "Task319",
        duration: 83,
        percentComplete: 96,
        start: 43235,
        finish: 43318,
        billable: false
    },
    {
        title: "Task320",
        duration: 19,
        percentComplete: 4,
        start: 43150,
        finish: 43169,
        billable: true
    },
    {
        title: "Task321",
        duration: 66,
        percentComplete: 41,
        start: 43285,
        finish: 43351,
        billable: false
    },
    {
        title: "Task322",
        duration: 31,
        percentComplete: 4,
        start: 43223,
        finish: 43254,
        billable: false
    },
    {
        title: "Task323",
        duration: 46,
        percentComplete: 25,
        start: 43167,
        finish: 43213,
        billable: false
    },
    {
        title: "Task324",
        duration: 90,
        percentComplete: 100,
        start: 43152,
        finish: 43242,
        billable: false
    },
    {
        title: "Task325",
        duration: 78,
        percentComplete: 71,
        start: 43119,
        finish: 43197,
        billable: true
    },
    {
        title: "Task326",
        duration: 96,
        percentComplete: 16,
        start: 43286,
        finish: 43382,
        billable: false
    },
    {
        title: "Task327",
        duration: 33,
        percentComplete: 78,
        start: 43184,
        finish: 43217,
        billable: false
    },
    {
        title: "Task328",
        duration: 77,
        percentComplete: 47,
        start: 43261,
        finish: 43338,
        billable: false
    },
    {
        title: "Task329",
        duration: 2,
        percentComplete: 75,
        start: 43143,
        finish: 43145,
        billable: false
    },
    {
        title: "Task330",
        duration: 83,
        percentComplete: 95,
        start: 43251,
        finish: 43334,
        billable: true
    },
    {
        title: "Task331",
        duration: 44,
        percentComplete: 86,
        start: 43129,
        finish: 43173,
        billable: false
    },
    {
        title: "Task332",
        duration: 70,
        percentComplete: 89,
        start: 43437,
        finish: 43142,
        billable: false
    },
    {
        title: "Task333",
        duration: 10,
        percentComplete: 83,
        start: 43161,
        finish: 43171,
        billable: false
    },
    {
        title: "Task334",
        duration: 100,
        percentComplete: 69,
        start: 43237,
        finish: 43337,
        billable: false
    },
    {
        title: "Task335",
        duration: 42,
        percentComplete: 67,
        start: 43413,
        finish: 43455,
        billable: true
    },
    {
        title: "Task336",
        duration: 59,
        percentComplete: 73,
        start: 43213,
        finish: 43272,
        billable: false
    },
    {
        title: "Task337",
        duration: 16,
        percentComplete: 43,
        start: 43121,
        finish: 43137,
        billable: false
    },
    {
        title: "Task338",
        duration: 99,
        percentComplete: 91,
        start: 43169,
        finish: 43268,
        billable: false
    },
    {
        title: "Task339",
        duration: 2,
        percentComplete: 67,
        start: 43465,
        finish: 43102,
        billable: false
    },
    {
        title: "Task340",
        duration: 25,
        percentComplete: 28,
        start: 43295,
        finish: 43320,
        billable: true
    },
    {
        title: "Task341",
        duration: 29,
        percentComplete: 10,
        start: 43428,
        finish: 43457,
        billable: false
    },
    {
        title: "Task342",
        duration: 26,
        percentComplete: 23,
        start: 43403,
        finish: 43429,
        billable: false
    },
    {
        title: "Task343",
        duration: 31,
        percentComplete: 20,
        start: 43426,
        finish: 43457,
        billable: false
    },
    {
        title: "Task344",
        duration: 59,
        percentComplete: 33,
        start: 43338,
        finish: 43397,
        billable: false
    },
    {
        title: "Task345",
        duration: 52,
        percentComplete: 98,
        start: 43148,
        finish: 43199,
        billable: true
    },
    {
        title: "Task346",
        duration: 76,
        percentComplete: 49,
        start: 43243,
        finish: 43319,
        billable: false
    },
    {
        title: "Task347",
        duration: 32,
        percentComplete: 35,
        start: 43444,
        finish: 43114,
        billable: false
    },
    {
        title: "Task348",
        duration: 62,
        percentComplete: 32,
        start: 43383,
        finish: 43445,
        billable: false
    },
    {
        title: "Task349",
        duration: 87,
        percentComplete: 33,
        start: 43106,
        finish: 43193,
        billable: false
    },
    {
        title: "Task350",
        duration: 76,
        percentComplete: 3,
        start: 43160,
        finish: 43236,
        billable: true
    },
    {
        title: "Task351",
        duration: 23,
        percentComplete: 28,
        start: 43262,
        finish: 43285,
        billable: false
    },
    {
        title: "Task352",
        duration: 100,
        percentComplete: 97,
        start: 43366,
        finish: 43101,
        billable: false
    },
    {
        title: "Task353",
        duration: 97,
        percentComplete: 22,
        start: 43170,
        finish: 43267,
        billable: false
    },
    {
        title: "Task354",
        duration: 41,
        percentComplete: 41,
        start: 43311,
        finish: 43352,
        billable: false
    },
    {
        title: "Task355",
        duration: 45,
        percentComplete: 43,
        start: 43200,
        finish: 43245,
        billable: true
    },
    {
        title: "Task356",
        duration: 24,
        percentComplete: 26,
        start: 43132,
        finish: 43156,
        billable: false
    },
    {
        title: "Task357",
        duration: 21,
        percentComplete: 47,
        start: 43263,
        finish: 43284,
        billable: false
    },
    {
        title: "Task358",
        duration: 52,
        percentComplete: 20,
        start: 43279,
        finish: 43331,
        billable: false
    },
    {
        title: "Task359",
        duration: 94,
        percentComplete: 40,
        start: 43174,
        finish: 43268,
        billable: false
    },
    {
        title: "Task360",
        duration: 39,
        percentComplete: 15,
        start: 43396,
        finish: 43435,
        billable: true
    },
    {
        title: "Task361",
        duration: 12,
        percentComplete: 18,
        start: 43190,
        finish: 43202,
        billable: false
    },
    {
        title: "Task362",
        duration: 65,
        percentComplete: 46,
        start: 43197,
        finish: 43262,
        billable: false
    },
    {
        title: "Task363",
        duration: 72,
        percentComplete: 17,
        start: 43272,
        finish: 43344,
        billable: false
    },
    {
        title: "Task364",
        duration: 96,
        percentComplete: 43,
        start: 43116,
        finish: 43212,
        billable: false
    },
    {
        title: "Task365",
        duration: 18,
        percentComplete: 65,
        start: 43378,
        finish: 43396,
        billable: true
    },
    {
        title: "Task366",
        duration: 83,
        percentComplete: 52,
        start: 43191,
        finish: 43274,
        billable: false
    },
    {
        title: "Task367",
        duration: 27,
        percentComplete: 82,
        start: 43459,
        finish: 43121,
        billable: false
    },
    {
        title: "Task368",
        duration: 54,
        percentComplete: 52,
        start: 43333,
        finish: 43387,
        billable: false
    },
    {
        title: "Task369",
        duration: 75,
        percentComplete: 55,
        start: 43362,
        finish: 43437,
        billable: false
    },
    {
        title: "Task370",
        duration: 68,
        percentComplete: 82,
        start: 43224,
        finish: 43292,
        billable: true
    },
    {
        title: "Task371",
        duration: 53,
        percentComplete: 93,
        start: 43288,
        finish: 43341,
        billable: false
    },
    {
        title: "Task372",
        duration: 77,
        percentComplete: 97,
        start: 43137,
        finish: 43213,
        billable: false
    },
    {
        title: "Task373",
        duration: 87,
        percentComplete: 78,
        start: 43346,
        finish: 43433,
        billable: false
    },
    {
        title: "Task374",
        duration: 16,
        percentComplete: 92,
        start: 43119,
        finish: 43135,
        billable: false
    },
    {
        title: "Task375",
        duration: 75,
        percentComplete: 86,
        start: 43433,
        finish: 43143,
        billable: true
    },
    {
        title: "Task376",
        duration: 89,
        percentComplete: 28,
        start: 43444,
        finish: 43168,
        billable: false
    },
    {
        title: "Task377",
        duration: 82,
        percentComplete: 57,
        start: 43133,
        finish: 43215,
        billable: false
    },
    {
        title: "Task378",
        duration: 97,
        percentComplete: 13,
        start: 43450,
        finish: 43182,
        billable: false
    },
    {
        title: "Task379",
        duration: 96,
        percentComplete: 98,
        start: 43162,
        finish: 43258,
        billable: false
    },
    {
        title: "Task380",
        duration: 10,
        percentComplete: 73,
        start: 43312,
        finish: 43322,
        billable: true
    },
    {
        title: "Task381",
        duration: 23,
        percentComplete: 65,
        start: 43374,
        finish: 43397,
        billable: false
    },
    {
        title: "Task382",
        duration: 22,
        percentComplete: 64,
        start: 43183,
        finish: 43205,
        billable: false
    },
    {
        title: "Task383",
        duration: 23,
        percentComplete: 30,
        start: 43356,
        finish: 43379,
        billable: false
    },
    {
        title: "Task384",
        duration: 78,
        percentComplete: 44,
        start: 43314,
        finish: 43392,
        billable: false
    },
    {
        title: "Task385",
        duration: 52,
        percentComplete: 6,
        start: 43163,
        finish: 43215,
        billable: true
    },
    {
        title: "Task386",
        duration: 79,
        percentComplete: 89,
        start: 43352,
        finish: 43431,
        billable: false
    },
    {
        title: "Task387",
        duration: 18,
        percentComplete: 91,
        start: 43207,
        finish: 43225,
        billable: false
    },
    {
        title: "Task388",
        duration: 68,
        percentComplete: 23,
        start: 43454,
        finish: 43157,
        billable: false
    },
    {
        title: "Task389",
        duration: 90,
        percentComplete: 92,
        start: 43145,
        finish: 43234,
        billable: false
    },
    {
        title: "Task390",
        duration: 52,
        percentComplete: 98,
        start: 43393,
        finish: 43445,
        billable: true
    },
    {
        title: "Task391",
        duration: 60,
        percentComplete: 50,
        start: 43334,
        finish: 43394,
        billable: false
    },
    {
        title: "Task392",
        duration: 52,
        percentComplete: 22,
        start: 43147,
        finish: 43198,
        billable: false
    },
    {
        title: "Task393",
        duration: 36,
        percentComplete: 77,
        start: 43104,
        finish: 43140,
        billable: false
    },
    {
        title: "Task394",
        duration: 54,
        percentComplete: 42,
        start: 43368,
        finish: 43422,
        billable: false
    },
    {
        title: "Task395",
        duration: 14,
        percentComplete: 17,
        start: 43211,
        finish: 43225,
        billable: true
    },
    {
        title: "Task396",
        duration: 72,
        percentComplete: 39,
        start: 43168,
        finish: 43240,
        billable: false
    },
    {
        title: "Task397",
        duration: 69,
        percentComplete: 84,
        start: 43303,
        finish: 43372,
        billable: false
    },
    {
        title: "Task398",
        duration: 97,
        percentComplete: 60,
        start: 43271,
        finish: 43368,
        billable: false
    },
    {
        title: "Task399",
        duration: 64,
        percentComplete: 3,
        start: 43458,
        finish: 43157,
        billable: false
    },
    {
        title: "Task400",
        duration: 18,
        percentComplete: 37,
        start: 43128,
        finish: 43146,
        billable: true
    },
    {
        title: "Task401",
        duration: 81,
        percentComplete: 39,
        start: 43206,
        finish: 43287,
        billable: false
    },
    {
        title: "Task402",
        duration: 14,
        percentComplete: 93,
        start: 43455,
        finish: 43104,
        billable: false
    },
    {
        title: "Task403",
        duration: 66,
        percentComplete: 52,
        start: 43403,
        finish: 43104,
        billable: false
    },
    {
        title: "Task404",
        duration: 76,
        percentComplete: 82,
        start: 43410,
        finish: 43121,
        billable: false
    },
    {
        title: "Task405",
        duration: 87,
        percentComplete: 38,
        start: 43250,
        finish: 43337,
        billable: true
    },
    {
        title: "Task406",
        duration: 77,
        percentComplete: 24,
        start: 43364,
        finish: 43441,
        billable: false
    },
    {
        title: "Task407",
        duration: 2,
        percentComplete: 60,
        start: 43163,
        finish: 43165,
        billable: false
    },
    {
        title: "Task408",
        duration: 95,
        percentComplete: 68,
        start: 43265,
        finish: 43360,
        billable: false
    },
    {
        title: "Task409",
        duration: 5,
        percentComplete: 47,
        start: 43135,
        finish: 43140,
        billable: false
    },
    {
        title: "Task410",
        duration: 86,
        percentComplete: 4,
        start: 43226,
        finish: 43312,
        billable: true
    },
    {
        title: "Task411",
        duration: 90,
        percentComplete: 69,
        start: 43129,
        finish: 43218,
        billable: false
    },
    {
        title: "Task412",
        duration: 44,
        percentComplete: 23,
        start: 43349,
        finish: 43393,
        billable: false
    },
    {
        title: "Task413",
        duration: 87,
        percentComplete: 81,
        start: 43253,
        finish: 43340,
        billable: false
    },
    {
        title: "Task414",
        duration: 89,
        percentComplete: 33,
        start: 43411,
        finish: 43135,
        billable: false
    },
    {
        title: "Task415",
        duration: 85,
        percentComplete: 70,
        start: 43435,
        finish: 43155,
        billable: true
    },
    {
        title: "Task416",
        duration: 47,
        percentComplete: 41,
        start: 43293,
        finish: 43340,
        billable: false
    },
    {
        title: "Task417",
        duration: 8,
        percentComplete: 85,
        start: 43159,
        finish: 43166,
        billable: false
    },
    {
        title: "Task418",
        duration: 94,
        percentComplete: 45,
        start: 43388,
        finish: 43117,
        billable: false
    },
    {
        title: "Task419",
        duration: 98,
        percentComplete: 51,
        start: 43225,
        finish: 43323,
        billable: false
    },
    {
        title: "Task420",
        duration: 86,
        percentComplete: 17,
        start: 43310,
        finish: 43396,
        billable: true
    },
    {
        title: "Task421",
        duration: 72,
        percentComplete: 28,
        start: 43307,
        finish: 43379,
        billable: false
    },
    {
        title: "Task422",
        duration: 45,
        percentComplete: 19,
        start: 43164,
        finish: 43209,
        billable: false
    },
    {
        title: "Task423",
        duration: 83,
        percentComplete: 37,
        start: 43325,
        finish: 43408,
        billable: false
    },
    {
        title: "Task424",
        duration: 21,
        percentComplete: 19,
        start: 43426,
        finish: 43447,
        billable: false
    },
    {
        title: "Task425",
        duration: 49,
        percentComplete: 65,
        start: 43306,
        finish: 43355,
        billable: true
    },
    {
        title: "Task426",
        duration: 66,
        percentComplete: 22,
        start: 43369,
        finish: 43435,
        billable: false
    },
    {
        title: "Task427",
        duration: 50,
        percentComplete: 7,
        start: 43448,
        finish: 43133,
        billable: false
    },
    {
        title: "Task428",
        duration: 99,
        percentComplete: 47,
        start: 43175,
        finish: 43274,
        billable: false
    },
    {
        title: "Task429",
        duration: 4,
        percentComplete: 61,
        start: 43309,
        finish: 43313,
        billable: false
    },
    {
        title: "Task430",
        duration: 12,
        percentComplete: 14,
        start: 43129,
        finish: 43141,
        billable: true
    },
    {
        title: "Task431",
        duration: 72,
        percentComplete: 7,
        start: 43267,
        finish: 43339,
        billable: false
    },
    {
        title: "Task432",
        duration: 93,
        percentComplete: 30,
        start: 43391,
        finish: 43119,
        billable: false
    },
    {
        title: "Task433",
        duration: 96,
        percentComplete: 93,
        start: 43385,
        finish: 43116,
        billable: false
    },
    {
        title: "Task434",
        duration: 67,
        percentComplete: 55,
        start: 43159,
        finish: 43226,
        billable: false
    },
    {
        title: "Task435",
        duration: 66,
        percentComplete: 92,
        start: 43121,
        finish: 43186,
        billable: true
    },
    {
        title: "Task436",
        duration: 52,
        percentComplete: 3,
        start: 43124,
        finish: 43176,
        billable: false
    },
    {
        title: "Task437",
        duration: 21,
        percentComplete: 85,
        start: 43182,
        finish: 43203,
        billable: false
    },
    {
        title: "Task438",
        duration: 23,
        percentComplete: 15,
        start: 43215,
        finish: 43238,
        billable: false
    },
    {
        title: "Task439",
        duration: 45,
        percentComplete: 87,
        start: 43342,
        finish: 43387,
        billable: false
    },
    {
        title: "Task440",
        duration: 72,
        percentComplete: 96,
        start: 43166,
        finish: 43238,
        billable: true
    },
    {
        title: "Task441",
        duration: 54,
        percentComplete: 57,
        start: 43248,
        finish: 43302,
        billable: false
    },
    {
        title: "Task442",
        duration: 63,
        percentComplete: 37,
        start: 43458,
        finish: 43156,
        billable: false
    },
    {
        title: "Task443",
        duration: 3,
        percentComplete: 69,
        start: 43404,
        finish: 43407,
        billable: false
    },
    {
        title: "Task444",
        duration: 17,
        percentComplete: 43,
        start: 43193,
        finish: 43210,
        billable: false
    },
    {
        title: "Task445",
        duration: 38,
        percentComplete: 14,
        start: 43157,
        finish: 43195,
        billable: true
    },
    {
        title: "Task446",
        duration: 56,
        percentComplete: 11,
        start: 43342,
        finish: 43398,
        billable: false
    },
    {
        title: "Task447",
        duration: 63,
        percentComplete: 35,
        start: 43105,
        finish: 43168,
        billable: false
    },
    {
        title: "Task448",
        duration: 79,
        percentComplete: 86,
        start: 43366,
        finish: 43445,
        billable: false
    },
    {
        title: "Task449",
        duration: 72,
        percentComplete: 39,
        start: 43310,
        finish: 43382,
        billable: false
    },
    {
        title: "Task450",
        duration: 81,
        percentComplete: 17,
        start: 43409,
        finish: 43125,
        billable: true
    },
    {
        title: "Task451",
        duration: 65,
        percentComplete: 45,
        start: 43410,
        finish: 43110,
        billable: false
    },
    {
        title: "Task452",
        duration: 75,
        percentComplete: 69,
        start: 43324,
        finish: 43399,
        billable: false
    },
    {
        title: "Task453",
        duration: 82,
        percentComplete: 16,
        start: 43449,
        finish: 43166,
        billable: false
    },
    {
        title: "Task454",
        duration: 15,
        percentComplete: 100,
        start: 43460,
        finish: 43110,
        billable: false
    },
    {
        title: "Task455",
        duration: 85,
        percentComplete: 58,
        start: 43242,
        finish: 43327,
        billable: true
    },
    {
        title: "Task456",
        duration: 59,
        percentComplete: 28,
        start: 43318,
        finish: 43377,
        billable: false
    },
    {
        title: "Task457",
        duration: 32,
        percentComplete: 89,
        start: 43392,
        finish: 43424,
        billable: false
    },
    {
        title: "Task458",
        duration: 25,
        percentComplete: 16,
        start: 43127,
        finish: 43152,
        billable: false
    },
    {
        title: "Task459",
        duration: 89,
        percentComplete: 97,
        start: 43407,
        finish: 43131,
        billable: false
    },
    {
        title: "Task460",
        duration: 16,
        percentComplete: 66,
        start: 43405,
        finish: 43421,
        billable: true
    },
    {
        title: "Task461",
        duration: 30,
        percentComplete: 71,
        start: 43137,
        finish: 43167,
        billable: false
    },
    {
        title: "Task462",
        duration: 52,
        percentComplete: 41,
        start: 43296,
        finish: 43348,
        billable: false
    },
    {
        title: "Task463",
        duration: 69,
        percentComplete: 47,
        start: 43109,
        finish: 43178,
        billable: false
    },
    {
        title: "Task464",
        duration: 26,
        percentComplete: 44,
        start: 43210,
        finish: 43236,
        billable: false
    },
    {
        title: "Task465",
        duration: 81,
        percentComplete: 17,
        start: 43430,
        finish: 43146,
        billable: true
    },
    {
        title: "Task466",
        duration: 99,
        percentComplete: 77,
        start: 43297,
        finish: 43396,
        billable: false
    },
    {
        title: "Task467",
        duration: 85,
        percentComplete: 99,
        start: 43162,
        finish: 43247,
        billable: false
    },
    {
        title: "Task468",
        duration: 23,
        percentComplete: 86,
        start: 43208,
        finish: 43231,
        billable: false
    },
    {
        title: "Task469",
        duration: 98,
        percentComplete: 41,
        start: 43316,
        finish: 43414,
        billable: false
    },
    {
        title: "Task470",
        duration: 51,
        percentComplete: 1,
        start: 43439,
        finish: 43125,
        billable: true
    },
    {
        title: "Task471",
        duration: 13,
        percentComplete: 60,
        start: 43414,
        finish: 43427,
        billable: false
    },
    {
        title: "Task472",
        duration: 68,
        percentComplete: 39,
        start: 43400,
        finish: 43103,
        billable: false
    },
    {
        title: "Task473",
        duration: 74,
        percentComplete: 15,
        start: 43385,
        finish: 43459,
        billable: false
    },
    {
        title: "Task474",
        duration: 60,
        percentComplete: 42,
        start: 43224,
        finish: 43284,
        billable: false
    },
    {
        title: "Task475",
        duration: 94,
        percentComplete: 10,
        start: 43273,
        finish: 43367,
        billable: true
    },
    {
        title: "Task476",
        duration: 76,
        percentComplete: 71,
        start: 43131,
        finish: 43207,
        billable: false
    },
    {
        title: "Task477",
        duration: 58,
        percentComplete: 14,
        start: 43435,
        finish: 43128,
        billable: false
    },
    {
        title: "Task478",
        duration: 74,
        percentComplete: 26,
        start: 43429,
        finish: 43138,
        billable: false
    },
    {
        title: "Task479",
        duration: 30,
        percentComplete: 28,
        start: 43147,
        finish: 43176,
        billable: false
    },
    {
        title: "Task480",
        duration: 95,
        percentComplete: 54,
        start: 43421,
        finish: 43151,
        billable: true
    },
    {
        title: "Task481",
        duration: 35,
        percentComplete: 70,
        start: 43268,
        finish: 43303,
        billable: false
    },
    {
        title: "Task482",
        duration: 10,
        percentComplete: 62,
        start: 43333,
        finish: 43343,
        billable: false
    },
    {
        title: "Task483",
        duration: 45,
        percentComplete: 66,
        start: 43326,
        finish: 43371,
        billable: false
    },
    {
        title: "Task484",
        duration: 93,
        percentComplete: 97,
        start: 43364,
        finish: 43457,
        billable: false
    },
    {
        title: "Task485",
        duration: 15,
        percentComplete: 79,
        start: 43359,
        finish: 43374,
        billable: true
    },
    {
        title: "Task486",
        duration: 84,
        percentComplete: 95,
        start: 43231,
        finish: 43315,
        billable: false
    },
    {
        title: "Task487",
        duration: 73,
        percentComplete: 50,
        start: 43460,
        finish: 43168,
        billable: false
    },
    {
        title: "Task488",
        duration: 33,
        percentComplete: 35,
        start: 43249,
        finish: 43282,
        billable: false
    },
    {
        title: "Task489",
        duration: 3,
        percentComplete: 79,
        start: 43434,
        finish: 43437,
        billable: false
    },
    {
        title: "Task490",
        duration: 49,
        percentComplete: 70,
        start: 43155,
        finish: 43203,
        billable: true
    },
    {
        title: "Task491",
        duration: 46,
        percentComplete: 1,
        start: 43352,
        finish: 43398,
        billable: false
    },
    {
        title: "Task492",
        duration: 15,
        percentComplete: 37,
        start: 43406,
        finish: 43421,
        billable: false
    },
    {
        title: "Task493",
        duration: 2,
        percentComplete: 9,
        start: 43297,
        finish: 43299,
        billable: false
    },
    {
        title: "Task494",
        duration: 90,
        percentComplete: 78,
        start: 43353,
        finish: 43443,
        billable: false
    },
    {
        title: "Task495",
        duration: 60,
        percentComplete: 13,
        start: 43385,
        finish: 43445,
        billable: true
    },
    {
        title: "Task496",
        duration: 58,
        percentComplete: 94,
        start: 43406,
        finish: 43464,
        billable: false
    },
    {
        title: "Task497",
        duration: 45,
        percentComplete: 64,
        start: 43151,
        finish: 43195,
        billable: false
    },
    {
        title: "Task498",
        duration: 34,
        percentComplete: 52,
        start: 43380,
        finish: 43414,
        billable: false
    },
    {
        title: "Task499",
        duration: 68,
        percentComplete: 74,
        start: 43175,
        finish: 43243,
        billable: false
    }
];
