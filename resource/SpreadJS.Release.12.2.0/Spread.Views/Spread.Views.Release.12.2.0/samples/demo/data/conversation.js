var data = [{
        "time": "July 23,2018 20:30:00",
        "content": "I am about to get involved with a VB.NET project in which there could be a choice in going either with WinForms or WPF. Or, something else?? What is the today current best practicechoice, in your opinion? WinForms or WPF? Why?"
    },

    {
        "time": "July 23,2018 20:40:00",
        "content": "Unless you're familiar with wpf, its a love/hate relationship. Things that are easy in forms could take much longer in forms. Yet the benefits of wpf is definitely worth it if you stick it out. I would say wpf makes the hard things easy and the easy things hard for a startup. There's no right or wrong to answer your question"
    }, {
        "time": "July 23,2018 20:42:00",
        "content": "I don't think there is an easy answer here. It depends on your skills, how fast you need to do the project, whether there are any critical project requiremnents that might make one technology more appealing than the other."
    }, {
        "time": "July 23,2018 21:02:00",
        "content": "Well my take on it is what are you building and does the architecture fit? Square peg round hole kind've situation no sense forcing something because you want to use a neat feature or technology simply for the sake. What are the requirements, how will you deploy it? Are there security considerations you need to take into account. What about the devices?"
    }, {
        "time": "July 23,2018 21:12:00",
        "end": "June 2,2018 13:30:00",
        "content": "It depends on your skill and your client's requirements. we got a client looking for software development services. They want to go with windows based application but after reviewing their requirements, we suggested them to go for web based application. So it depends on the scope of work."
    }, {
        "time": "July 23,2018 21:30:00",
        "content": "Can you provide an example of what would be easier in WinForms but more difficult in WPF? And, in that example, detail the complications?"
    }, {
        "time": "July 23,2018 22:10:00",
        "content": "Also, I have heard that it is difficult to design the UI in Visual Studio 2012, in WPF design mode. That is, compared with WinForms in the same environment. Are there visual editors for WPF?"
    }, {
        "time": "July 23,2018 22:22:00",
        "content": "check http://kaxaml.com/ and http://www.microsoft.com/expression/eng/"
    }, {
        "time": "July 24,2018 12:35:00",
        "content": "Jatin, those look great. I'll try those. I am tasked with proof of concept between WinForm vs. WPF, so I am diving in."
    }, {
        "time": "July 24,2018 18:42:00",
        "content": "What's the future of the app? Keep the UI technology as separate from the applications intelligence, keep the UI thin, that way it doesn't really matter what UI technology you use.When the day comes it needs to port to any platform."
    }, {
        "time": "July 24,2018 19:17:00",
        "content": "If this application is for internal use, just use winforms. If this application is customer facing and they want the application to visually stand out then use WPF."
    }, {
        "time": "July 24,2018 20:28:00",
        "content": "Well, I was able to port nearly all of our current classes and modules, and I was able to spin up a few pages for a Navigation Window application. I really love the xaml, so far. I've figured out how to create a login page, and then a main page that will serve as the framework for the whole app. I am thinking of using some sort of Ribbon Bar for navigation, etc. I'm loving WPF and I think it is the right choice."
    }, {
        "time": "July 24,2018 21:17:00",
        "content": "Our current application is entirely web-based. We are doing a proof of concept to move from web-based to application or combination application and apps that can be deployed on devices. The Surface with Windows8/10 is one of the targets. So, I think WPF will serve this well. Again, being able to port our core back-side classes so easily, and being able to spin up a few pages withing the WPF Navigation Window application really rocked, today. I'm highly pleased."
    }, {
        "time": "July 24,2018 22:46:00",
        "content": "I always found there are a lot of important features in WPF specially the spell checker for text boxes (yes you can get a 3rd party plug in but, its always good to get an inbuilt right?I know a lot of people use WPF because it will help with some development patterns but, I love the freely included controls.Then again, there is another problem... if you don't really need those flashy stuff why would you want to move to something new? I have seen Microsoft trying so hard to move away from Winforms with things like the windows 8 metro apps, but... we all know what happen to those...Just thinking out of the box, how about a chrome desktop app ?"
    }, {
        "time": "July 24,2018 23:16:00",
        "content": "Jatin Panchal: I think it is highly rude and very inappropriate that you sent an email message to me, trying to solicit a sale from me, of your services. Do you not think that there are hundreds if not thousands of people on here that would love to sell me the very same thing? What if every one of them came in here and started hunting my business? I did NOT indicate that I am searching for your services. I think you should hang your head in shame, and not do this sort of behavior again. Shame on you. "
    }, {
        "time": "July 25,2018 00:23:00",
        "content": "This WPF is fitting the bill, perfectly. I have had no issue, so far, in creating my graphical user interface. I have not had any issue porting my database and messaging classes. I have had no issue in utilizing .Net features, etc. This is going so very well that I wonder why I waited or had doubts.I am sure that WinForms plays a role in certain cases, but, since most of our code and functionality is already classed and discrete (not tied to any front-end GUI), it really does not matter which route we go... but WPF seems to have the greatest flexibility. I'm pretty happy with it. And, I am using VS2012 VB.NET -- Imagine when we upgrade to a more current version. :)I appreciate the feedback I am getting. It was very helpful."
    }, {
        "time": "July 25,2018 00:52:00",
        "content": "Getting an entire company to move to the next version of VS2012 takes a lot of persuading. In the meantime, I will suffer this. :)"
    }, {
        "time": "July 25,2018 10:41:00",
        "content": "Joshua: I tried to use Blend, but, it seems to be really limited. Of course, I am using the VS2012 Blend -- perhaps I should use a more current version. I think I can do that, right?And, I cannot get an Open in Blend to open any XAML file, nor the project file, from within VS2012 (and I have update 4)."
    }, {
        "time": "July 25,2018 15:33:00",
        "content": "I do , WPF,C# and MVVM , on my job system"
    }, {
        "time": "July 25,2018 18:19:00",
        "content": "Go for WPF and you will never need to look back... :)"
    }, {
        "time": "July 25,2018 20:09:00",
        "content": "It will not be wise to comment with out knowing your requirements. If you need something with a lifetime of 5-6 yrs with incremental development then go with the WPF. As it will be easy to port the same code to mobile devices as well as microsoft has a lot of focus on the XAML.MVVM, WCF, C# is what I use most whenever it comes to WPF."
    }, {
        "time": "July 25,2018 23:23:00",
        "content": "WPF because it's modern and you can apply patterns like MVVM. Which makes it easy to separate different layers and results in easier support for other platforms. I also suggest choosing c# because it has a wider support., speaker:Aboo Azarnoush"
    }, {
        "time": "July 26,2018 18:18:00",
        "content": "WPF because it's modern and you can apply patterns like MVVM. Which makes it easy to separate different layers and results in easier support for other platforms. I also suggest choosing c# because it has a wider support."
    }, {
        "time": "July 26,2018 19:03:00",
        "content": ", Wpf, c#. Mvvc."
    }, {
        "time": "July 26,2018 20:32:00",
        "content": "WPF is your best choice. Once you learned xaml, you can use the same knowledge to create UW 10 applications to target mobile and desktop."
    }, {
        "time": "July 26,2018 21:56:00",
        "content": "Rebecca Papenbrock: That was a thoughtful response. At this point, since I have used both WinForms as well as this week's exploration of XAML and WPF, I feel most comfortable and optimistic using WPF. We will be continuing forward in both WebForms (Using Telerik for a lot of our GUI), as well as the new WPF path. That way we are hitting more targets. Since the core code is independent from UI, we can create a core library (probably will be a DLL) and just maintain one instance, while wrapping that up in various UI frameworks. "
    }, {
        "time": "July 26,2018 22:51:00",
        "content": "Darrin Freeman: We have been a web-based/only application (what an old word), for a number of years. However, a majority of our users that are in the WAN are asking for something native to their device. We've decided to develop for BOTH sides; web-based as well as non-web-based. Because all of our business logic and core software engineering results in a core framework of our own, it is easy to wrap any UI around it."
    }, {
        "time": "July 26,2018 23:12:00",
        "content": "Barış Güzelsoy: I came aboard this enterprise effort, after the decision was made and then committed, to use VB.NET and ASP with Telerik. I have no issue with coding in whatever I have to. I've been programming in so many language flavors since 1983, that it no longer challenges me to have one or the other. Now, if I have to use Assembly/Machine language code environments, then I would complain a little bit. But, only a little bit. :)"
    }, {
        "time": "July 27,2018 00:42:00",
        "content": "My choise is both. If you want to compatible and performance, WinWorms. If you flexibility and elegance, WPF.It's funny to develop application in WPF XAML."
    }
];
