﻿var dataView;

window.onload = function () {
    showGanttView();
};

function showGanttView() {

    $("#btnGantt").addClass('active');
    $("#btnKanban").removeClass('active');
    $("#btnTimeline").removeClass('active');
    $("#btnGrid").removeClass('active');
    $("#kanbanBtnGroup").css("visibility", "hidden");

    var columns = [{
        id: 'id',
        caption: 'Id',
        dataField: 'id',
        width: 70,
        allowResizing: false
    }, {
        id: 'start',
        caption: 'Start Date',
        dataField: 'start',
        width: 100,
        dataType: 'date',
        format: 'mmm dd,yyyy',
        allowResizing: false
    }, {
        id: 'end',
        caption: 'End Date',
        dataField: 'end',
        width: 113,
        dataType: 'date',
        format: 'mmm dd,yyyy',
        allowResizing: false
    }, {
        id: 'percentComplete',
        caption: 'Percent Complete',
        dataField: 'percentComplete',
        visible: true,        
        width: 100,
        allowEditing: false,
        allowResizing: false
    }, {
        id: 'gantt',
        ganttColumn: {
            timeLineScale: 'month',
            scale: 300,
            start: 'start',
            end: 'end',
            text: 'title'
        },
        width: '*'
    }, {
        id: 'description',
        caption: 'Description',
        dataField: 'description',
        visible: false
    }, {
        id: 'user',
        caption: 'User',
        dataField: 'user',
        visible: false
    }, {
        id: 'predecessorID',
        caption: 'predecessorID',
        dataField: 'predecessorID',
        visible: false,
        allowEditing: false
    }, {
        id: 'parentID',
        caption: 'parentID',
        dataField: 'parentID',
        visible: false,
        allowEditing: false
    }];

    if (dataView)
        dataView.destroy();

    dataView = new GC.Spread.Views.DataView(document.getElementById('grid1'), data, columns, new GC.Spread.Views.Plugins.GridLayout({
        colHeaderHeight: 48,
        rowHeight: 48,
        allowEditing: true,
        editMode: 'none',
        showRowHeader: false,
        hierarchy: {
            keyField: 'id',
            parentField: 'parentID',
            collapsed: false,
            column: 'id',
            footer: {
                visible: false
            }
        }
    }));
}

function showKanbanView() {

    $("#btnGantt").removeClass('active');
    $("#btnKanban").addClass('active');
    $("#btnTimeline").removeClass('active');
    $("#btnGrid").removeClass('active');
    $("#kanbanBtnGroup").css("visibility", "visible");
    $("#btnDepartment").addClass('active');
    $("#btnUser").removeClass('active');

    var rowTemplate = '<div class="group-item-container">' +
        '<div class="group-item-container-inner {{? it.percentComplete==1}}finish{{?? it.percentComplete>=0.8}}eighty-per{{?? it.percentComplete>=0.5}}fifty-per{{?? it.percentComplete>=0.1}}thirty-per{{??}}start{{?}}">' +
        '<div data-column="title" class="group-item-title  {{? it.percentComplete==1}}finish-head{{?? it.percentComplete>=0.8}}eighty-per-head{{?? it.percentComplete>=0.5}}fifty-per-head{{?? it.percentComplete>=0.1}}thirty-per-head{{??}}start-head{{?}}"></div>' +
        '<div data-column="photo" class="group-photo-container"></div>' +
        '<div data-column="description" class="group-item-description"></div>' +
        '</div></div>';

    var photoPresenter = '<img class="employee-photo" src={{=it.photo}} />';

    var columns = [
        { id: 'title', name: 'title', dataField: 'title' },
        { id: 'description', name: 'description', dataField: 'description' },
        { id: 'user', name: 'user', dataField: 'user' },
        { id: 'photo', dataField: 'photo', presenter: photoPresenter },
        { id: 'percentComplete', dataField: 'percentComplete' }
    ];

    $.each(data, function (key, val) {
        for (var prop in val) {
            if (prop === 'start' || prop === 'end') {
                val[prop] = new Date(val[prop]);
            }
        }
        val.photo = './images/' + val.user + ".jpg";
    });

    var TrellisGrouping = new GC.Spread.Views.Plugins.TrellisGrouping({
        panelUnitWidth: 190
    });
    //debugger;
    TrellisGrouping.dragDropping.addHandler(function (sender, args) {
        if (args.status == "beforeDropping" && $("#btnUser").hasClass("active")) {
            //debugger;
            var txt = document.elementFromPoint(args.event.clientX, args.event.clientY).innerText;
            var item = dataView.data.sourceCollection.filter(function (it) {
                return (txt.indexOf(it.description) != -1) || (txt.indexOf(it.title) != -1);
            });
            if (item.length > 0 && item[0].user != args.dataItem.user) {
                args.cancel = true;
            }
        }
    });;

    dataView.destroy();

    dataView = new GC.Spread.Views.DataView(document.getElementById('grid1'), data, columns, new GC.Spread.Views.Plugins.GridLayout({
        grouping: [{ field: 'department', header: { height: 24 } }, { field: 'stage', header: { height: 24 } }],
        rowTemplate: rowTemplate,
        rowHeight: 140,
        groupStrategy: TrellisGrouping
    }));
}

var timeFormatter = 'HH:mmtt';
var showGroupHeader = false;
var axisLocation = 'middle';
var headerLocation = 'top';
var index = 0;

function showTimelineView() {
    $("#btnGantt").removeClass('active');
    $("#btnKanban").removeClass('active');
    $("#btnTimeline").addClass('active');
    $("#btnGrid").removeClass('active');
    $("#kanbanBtnGroup").css("visibility", "hidden");

    var presenter = '<img class="employee-photo" src={{=it.photo}} />';
    var columns = [{
        id: 'topic',
        dataField: 'topic'
    }, {
        id: 'start',
        dataField: 'start',
        format: 'MMMM dd, HH:mmtt'
    }, {
        id: 'end',
        dataField: 'end',
        format: 'MMMM dd, HH:mmtt'
    }, {
        id: 'speaker',
        dataField: 'speaker'
    }, {
        id: 'content',
        dataField: 'content'
    }, {
        id: 'photo',
        dataField: 'photo',
        presenter: presenter
    }];

    var groupDetailEventTemplate = '<div style="margin:5px;">' +
        '<div style="font-size:14px;font-weight: bold;">{{=it.title}}</div>' +
        '<div style="font-size:14px;font-weight: normal;">{{=it.description}}</div>' +
        '<blockquote style="display: block;font-size:13px;font-family: "Helvetic Neue, Helvetica, Arial">User:{{=it.user}}</blockquote>' +
        '<span style="margin-right: auto" >End:{{=it.end}}</span>' +
        '</div>';

    $.each(data, function (key, val) {
        for (var prop in val) {
            if (prop === 'start' || prop === 'end') {
                val[prop] = new Date(val[prop]);
            }
        }
        val.photo = './images/' + val.user + ".jpg";
    });

    var excelFormatter = new GC.Spread.Formatter.GeneralFormatter('mmmm dd');
    var TimelineGrouping = new GC.Spread.Views.Plugins.TimelineGrouping({});

    dataView.destroy();

    dataView = new GC.Spread.Views.DataView(document.getElementById('grid1'), data, columns, new GC.Spread.Views.Plugins.GridLayout({
        grouping: {
            field: 'start',
            converter: function (field) {
                return excelFormatter.format(field);
            }
        },
        autoRowHeight: true,
        rowTemplate: groupDetailEventTemplate,
        groupStrategy: TimelineGrouping
    }));
}

function showGridView() {

    $("#btnGantt").removeClass('active');
    $("#btnKanban").removeClass('active');
    $("#btnTimeline").removeClass('active');
    $("#btnGrid").addClass('active');
    $("#kanbanBtnGroup").css("visibility", "hidden");

    var columns = [{
        id: 'id',
        caption: 'Id',
        dataField: 'id',
        width: 80
    }, {
        id: 'start',
        caption: 'Start Date',
        dataField: 'start',
        width: 100,
        dataType: 'date',
        format: 'mmm dd,yyyy'
    }, {
        id: 'end',
        caption: 'End Date',
        dataField: 'end',
        width: 100,
        dataType: 'date',
        format: 'mmm dd,yyyy'
    }, {
        id: 'percentComplete',
        caption: 'Percent Complete',
        dataField: 'percentComplete',
        visible: true,
        allowEditing: false
    }, {
        id: 'title',
        caption: 'Title',
        dataField: 'title',
        visible: true,
        allowEditing: false
    },
    {
        id: 'description',
        caption: 'Description',
        dataField: 'description',
        visible: true
    }, {
        id: 'stage',
        caption: 'stage',
        dataField: 'stage',
        visible: true
    },
    {
        id: 'user',
        caption: 'User',
        dataField: 'user',
        visible: true
    }, {
        id: 'predecessorID',
        caption: 'predecessorID',
        dataField: 'predecessorID',
        visible: false,
        allowEditing: false
    }, {
        id: 'parentID',
        caption: 'parentID',
        dataField: 'parentID',
        visible: false,
        allowEditing: false
    }];

    dataView.destroy();

    dataView = new GC.Spread.Views.DataView(document.getElementById('grid1'), data, columns, new GC.Spread.Views.Plugins.GridLayout({
        colHeaderHeight: 40,
        rowHeight: 40,
        showRowHeader: false
    }));
}

function getImage() {
    var imageName;
    imageName = imageNames[index % 5];
    index++;
    return imageName + '.jpg';
}

function byDepartment() {
    $("#btnDepartment").addClass('active');
    $("#btnUser").removeClass('active');
    dataView.data.groupDescriptors = [{ field: 'department', header: { height: 24 } }, { field: 'stage', header: { height: 24 } }];
    dataView.invalidate();
}

function byUser() {
    $("#btnDepartment").removeClass('active');
    $("#btnUser").addClass('active');
    dataView.data.groupDescriptors = [{ field: 'user', header: { height: 24 } }, { field: 'stage', header: { height: 24 } }];
    dataView.invalidate();
}