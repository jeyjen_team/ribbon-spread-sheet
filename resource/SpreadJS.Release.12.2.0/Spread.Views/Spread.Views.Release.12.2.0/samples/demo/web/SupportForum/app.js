// define some components
var dataView;
var topicDetailView;
var currentItem;
var router = new VueRouter();

function handleMouseClick(event) {
    var hitTestInfo = dataView.hitTest(event);
    if (hitTestInfo.area === "viewport" && hitTestInfo.row >= 0) {
        currentItem = dataView.data.getItem(hitTestInfo.row);
        router.go({
            name: 'topic',
            query: {
                id: currentItem.id
            }
        });
    }
}

window.addEventListener('resize', function() {
    dataView.invalidate();
});

var cardColumns = [
    { id: 'title', dataField: 'title' },
    { id: 'description', dataField: 'description', format: removeAllHTMLTags },
    { id: 'idcard', dataField: 'idcard', presenter: '<image class="employee-photo shadow" src="{{=it.idcard}}"></image>' }
];
var listColumns = [
    { id: 'title', dataField: 'title' },
    { id: 'description', dataField: 'description', format: removeAllHTMLTags },
    { id: 'idcard', dataField: 'idcard', presenter: '<image class="employee-photo-small shadow" src="{{=it.idcard}}"></image>' }
];

function removeAllHTMLTags(input) {
    var div = document.createElement('div');
    div.innerHTML = input.value;
    return div.textContent || div.innerText || '';
}

function changeToCard() {
    $('#disp-cards').addClass('active');
    $('#disp-lists').removeClass('active');
    dataView.options.cardHeight = 130;
    dataView.options.rowTemplate = '#topicListTmplCard';
    dataView.columns = cardColumns;
    dataView.invalidate();
}

function changeToList() {
    $('#disp-cards').removeClass('active');
    $('#disp-lists').addClass('active');
    dataView.options.cardHeight = 80;
    dataView.options.rowTemplate = '#topicListTmplList';
    dataView.columns = listColumns;
    dataView.invalidate();
}

var dataSource;
$.getJSON('data/supportforum.json', function(data) {
    dataSource = data;
    var AllTopics = Vue.extend({
        template: '#allTopic',
        attached: function() {
            dataView = new GC.Spread.Views.DataView(document.getElementById('grid1'), dataSource, cardColumns, new GC.Spread.Views.Plugins.CardLayout({
                cardHeight: 130,
                rowTemplate: '#topicListTmplCard'
            }));
            document.getElementById('grid1').addEventListener('click', handleMouseClick);
        }
    });

    var columns2 = [
        { id: 'date', caption: 'Date', dataField: 'date', format: 'MMMM dd' },
        { id: 'user', caption: 'user', dataField: 'user' },
        { id: 'post', caption: 'post', dataField: 'post' },
        { id: 'idcard', caption: 'idcard', dataField: 'idcard' }
    ];

    var gHLeftTemplate = '<div class="idcardcontainer">' +
        '<image class="employee-photo shadow" src="{{=it.eval(\'=first([idcard]\')}}"></image>' +
        '</div>';

    var Topic = Vue.extend({
        template: '#topicDetail',
        attached: function() {
            if (currentItem) {
                var TimelineGrouping = new GC.Spread.Views.Plugins.TimelineGrouping({
                    axisLocation: 'left',
                    groupHeaderLocation: 'onAxis',
                    gutter: 120
                });

                for (var i = 0, len = currentItem.posts.length; i < len; i++) {
                    var tpData = currentItem.posts[i];
                    for (var prop in tpData) {
                        if (prop === 'date') {
                            tpData[prop] = new Date(tpData[prop]);
                        }
                    }
                }

                var excelFormatter = new  GC.Spread.Formatter.GeneralFormatter();
                detail = new GC.Spread.Views.DataView(document.getElementById('detailGrid'), currentItem.posts, columns2, new GC.Spread.Views.Plugins.GridLayout({
                    grouping: {
                        field: "date",
                        header: {
                            height: 60,
                            template: gHLeftTemplate,
                            visible: true
                        },
                        converter: function(field) {
                            return excelFormatter.format(field);
                        }
                    },
                    autoRowHeight: true,
                    rowTemplate: '#topicDetailPostTmpl',
                    groupStrategy: TimelineGrouping
                }));
            }
        },
        route: {
            activate: function(transition) {
                transition.next();
            },
            data: function(transition) {
                transition.next({
                    post: currentItem
                });
            },

        }
    });

    var App = Vue.extend({});

    router.map({
        '/allTopics': {
            component: AllTopics
        },
        '/topic': {
            name: 'topic',
            component: Topic
        }
    })

    router.start(App, '#app');
    router.go({
        path: '/allTopics'
    })
});
