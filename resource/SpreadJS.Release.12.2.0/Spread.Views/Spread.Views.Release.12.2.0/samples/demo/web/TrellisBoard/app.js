var dataview;
var TrellisGrouping;
var colors = ['#00D8FF', '#74ACC8', '#808080', '#F5A623', '#D0021B'];
var priorityPresenter = '<div style="display: inline-block;background: {{=colors[it.priority/100 -1]}}; width:100%;height:3px;margin:3px 3px 3px 0;"></div>';
var imagePresenter = '<div style="padding:8px; height:auto"><img style="width:100%;" src={{=it.image}} /></div>';
var evaluationPresenter = '<div style="padding-bottom: 5px"><span title="Evaluation" style="background: {{=colors[it.priority/100 -1]}};">{{=it.evaluation}}</span></div>';

var columns = [
    {id: 'image', dataField: 'image', presenter: imagePresenter},
    {id: 'priority', dataField: 'priority', presenter: priorityPresenter},
    {id: 'description', dataField: 'description'},
    {id: 'evaluation', dataField: 'evaluation', presenter: evaluationPresenter}
];

$(document).ready(function() {
    $.getJSON('data/support.json', function(data) {
        var panelUnitWidth = getPanelWidth();

        TrellisGrouping = new GC.Spread.Views.Plugins.TrellisGrouping({
            panelUnitWidth: panelUnitWidth
        });
        dataview = new GC.Spread.Views.DataView(document.getElementById('grid1'), data, columns, new GC.Spread.Views.Plugins.GridLayout({
            grouping: [{
                field: 'category',
                header: {
                    height: 35,
                    template: '<div class="trellis-card-header"><span>{{=it.name}}</span><div class="group-header-options"><span class="group-remove fa fa-trash-o"></span></div></div>'
                },
                footer: {
                    visible: true,
                    height: 24,
                    template: '<div class="trellis-card-add">Add a item...</div>'
                },
                preDefinedGroups: ['STORIES', 'TO DO/ISSUES', 'PROCESSING', 'DONE']
            }],
            rowTemplate: '#trellis-card',
            groupStrategy: TrellisGrouping,
            autoRowHeight: true
        }));

        document.addEventListener('click', function(event) {
            var hitTestInfo = dataview.hitTest(event);
            var target = event.target;
            while (target) {
                if (hasClass(target, 'gc-row') && hitTestInfo.groupInfo.area === 'groupContent') {
                    startCardModfiy(hitTestInfo);
                    break;
                } else if (hasClass(target, 'trellis-card-add')) {
                    startCardAdd(hitTestInfo);
                    break;
                } else if (hasClass(target, 'card-remove')) {
                    removeCard(hitTestInfo);
                    break;
                } else if (hasClass(target, 'card-edit')) {
                    startCardModfiy(hitTestInfo);
                    break;
                } else if (hasClass(target, 'group-remove')) {
                    removeGroup(hitTestInfo);
                    break;
                } else {
                    target = target.parentNode;
                }
            }
        });

        //region Add a new card
        var hiddenFooter = null;

        function startCardAdd(hitTestInfo) {
            closeNewCard();
            var editArea = createElement('<div id="trellis-add-card-editing" class="add-card">' +
            '<textarea class="add-textarea"></textarea>' +
            '<div class="add-card-footer"><div class="footer-button cancel-button"><span>Cancel</span></div><div class="footer-button ok-button"><span>OK</span></div></div>' +
            '</div>');
            var group = document.getElementById(dataview.uid + '-g' + hitTestInfo.groupInfo.path.join('_'));
            var groupBody = group.querySelector('.gc-trellis-group-body');
            groupBody.appendChild(editArea);
            var rect = editArea.getBoundingClientRect();
            var windowHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
            if (rect.top + rect.height > windowHeight) {
                editArea.style.bottom = (rect.top + rect.height - windowHeight) + 'px';
            }
            editArea.querySelector('textarea').focus();
            hiddenFooter = group.querySelector('#' + dataview.uid + '-gf' + hitTestInfo.groupInfo.path.join('_'));
            hiddenFooter.setAttribute('hidden', 'hidden');
            editArea.addEventListener('click', clickAddCardHandler);
            editArea.addEventListener('keydown', keydownCardHandler);
            document.addEventListener('click', closeNewCard);
        }

        function closeNewCard() {
            var editArea = document.getElementById('trellis-add-card-editing');
            if (editArea) {
                editArea.removeEventListener('click', clickAddCardHandler);
                editArea.parentNode.removeChild(editArea);
                document.removeEventListener('click', closeNewCard);
                if (hiddenFooter) {
                    hiddenFooter.removeAttribute('hidden');
                    hiddenFooter = null;
                }
            }
        }

        function saveNewCard(groupInfo) {
            var editArea = document.getElementById('trellis-add-card-editing');
            var editTextArea = editArea.querySelector('textarea');
            var newObj = {
                description: encodeValue(editTextArea.value),
                category: groupInfo.name,
                priority: 100,
                evaluation: 0.25
            };
            closeNewCard();
            dataview.data.insertDataItems(newObj);
            updateActivities({
                action: 'addCard',
                value: editTextArea.value,
                group: groupInfo.name
            });
        }

        function clickAddCardHandler(event) {
            event.stopPropagation();
            var hitTestInfo = dataview.hitTest(event);
            var target = event.target;
            while (target) {
                if (hasClass(target, 'ok-button')) {
                    var groupInfo = getGroupInfo(hitTestInfo.groupInfo.path);
                    saveNewCard(groupInfo);
                    break;
                } else if (hasClass(target, 'cancel-button')) {
                    closeNewCard();
                    break;
                } else if (hasClass(target, 'edit-overlay')) {
                    closeNewCard();
                    break;
                } else {
                    target = target.parentNode;
                }
            }
        }

        function keydownCardHandler(event) {
            event.stopPropagation();
        }

        //endregion

        //region Remove a card
        function removeCard(hitTestInfo) {
            var row = hitTestInfo.groupInfo.row;
            var groupInfo = getGroupInfo(hitTestInfo.groupInfo.path);
            var item = groupInfo.getItems()[row];
            dataview.data.removeDataItems(groupInfo.getSourceRowIndex(row));
            updateActivities({
                action: 'removeCard',
                group: groupInfo.name,
                value: item.description
            })
        }

        //endregion

        //region Modify a card
        var modifyHitTest;

        function startCardModfiy(hitTestInfo) {
            //add overlay
            var modifyOverlay = createElement('<div id="modify-overlay" class="modify-overlay"></div>');
            modifyOverlay.addEventListener('click', closeCardModify);
            document.body.appendChild(modifyOverlay);
            //add modify dialog
            modifyHitTest = hitTestInfo;
            var row = hitTestInfo.groupInfo.row;
            var groupInfo = getGroupInfo(hitTestInfo.groupInfo.path);
            var originalData = groupInfo.getItem(row);
            var labelColors = [
                {priority: 'lowest', color: '#00D8FF', weight: 100},
                {priority: 'low', color: '#74ACC8', weight: 200},
                {priority: 'medium', color: '#808080', weight: 300},
                {priority: 'high', color: '#F5A623', weight: 400},
                {priority: 'highest', color: '#D0021B', weight: 500}
            ];
            var i;
            var length;
            var colorPickersLabel = '<div class="text-label">Priority</div>';
            var colorPickersHTML = '<div class="color-picker-group">';
            var dataColors = originalData.dataItem.priority;
            var checked;
            for (i = 0, length = labelColors.length; i < length; i++) {
                checked = dataColors && dataColors === 100 * (1 + colors.indexOf(labelColors[i].color)) ? ' checked' : '';

                colorPickersHTML += '<div title="' + labelColors[i].priority + '" data-color="' + labelColors[i].color + '" style="background-color:' + labelColors[i].color + '" class="color-picker-item' + checked + '">' + labelColors[i].weight + '</div>';
            }
            colorPickersHTML += '</div>';
            var descriptionEditor = '<div class="description-editor"><div class="text-label">Description</div><textarea placeholder="Here is Description...">' + (originalData.description ? originalData.description : '') + '</textarea></div>';
            var evaluationPicker = '<div class="evaluation-picker"><div class="text-label">Workloads</div><div><input class="workInput" type="number" min="0.25" max="5" step="0.25"  placeholder="Here is Evaluation..." value="' + (!isNaN(originalData.evaluation) ? originalData.evaluation : '') + '" /></div></div>';
            var okCancel = '<div class="modify-card-footer"><button type="button" class="btn btn-default cancel-button">Cancel</button><button type="button" class="btn btn-default ok-button">OK</button></div>';
            var dialogHTML = '<div id="trellis-modify-card-editing" class="modify-card-dialog" style="position: absolute;">' + colorPickersLabel + colorPickersHTML + descriptionEditor + evaluationPicker + okCancel + '</div>';
            var dialogElement = createElement(dialogHTML);
            document.body.appendChild(dialogElement);

            var rect = dialogElement.getBoundingClientRect();
            var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
            var windowHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
            var left = parseInt((windowWidth - rect.width) / 2 + window.pageXOffset);
            var top = parseInt((windowHeight - rect.height) / 4 + window.pageYOffset);
            dialogElement.style.left = left + 'px';
            dialogElement.style.top = top + 'px';
            dialogElement.addEventListener('click', clickEditingHandler);
            dialogElement.querySelector('textarea').focus();
        }

        function commitCardModify() {
            var dialog = document.getElementById('trellis-modify-card-editing');
            var newLabelsElement = dialog.querySelector('.color-picker-item.checked');
            var label = newLabelsElement && newLabelsElement.getAttribute('data-color');
            var descriptionEditor = dialog.querySelector('.description-editor textarea');
            var evaluationEditor = dialog.querySelector('.evaluation-picker input');
            var row = modifyHitTest.groupInfo.row;
            var groupInfo = getGroupInfo(modifyHitTest.groupInfo.path);
            var originalData = groupInfo.getItem(row);
            var oldData = _.clone(originalData);
            _.assign(originalData, {
                priority: 100 * (colors.indexOf(label) + 1),
                description: encodeValue(descriptionEditor.value),
                evaluation: parseFloat(evaluationEditor.value)
            });
            dataview.invalidate();
            closeCardModify();
            updateActivities({
                action: 'editCard',
                oldValue: oldData,
                value: originalData
            })
        }

        function closeCardModify() {
            var dialog = document.getElementById('trellis-modify-card-editing');
            dialog.removeEventListener('click', clickEditingHandler);
            dialog.parentNode.removeChild(dialog);
            var overlay = document.getElementById('modify-overlay');
            overlay.removeEventListener('click', closeCardModify);
            overlay.parentNode.removeChild(overlay);
            modifyHitTest = null;
        }

        function clickEditingHandler(event) {
            var target = event.target;
            var modifyDialog = document.getElementById('trellis-modify-card-editing');
            while (target) {
                if (hasClass(target, 'color-picker-item')) {
                    if (hasClass(target, 'checked')) {
                        removeClass(target, 'checked');
                    } else {
                        var ele = modifyDialog.querySelector('.color-picker-item.checked');
                        removeClass(ele, 'checked');
                        addClass(target, 'checked');
                    }
                    break;
                } else if (hasClass(target, 'ok-button')) {
                    commitCardModify();
                    break;
                } else if (hasClass(target, 'cancel-button')) {
                    closeCardModify();
                    break;
                } else {
                    target = target.parentNode;
                }
            }
        }

        //endregion

        //region Moving a card
        var oldHitInfo;
        TrellisGrouping.dragDropping.addHandler(function(sender, args) {
            if (args.status === 'beforeDragging') {
                oldHitInfo = args.hitInfo;
            }
            if (args.status === 'beforeDropping') {
                var oldGroup = oldHitInfo.groupInfo;
                if (args.hitInfo && args.hitInfo.groupInfo) {
                    var newGroup = args.hitInfo.groupInfo;
                    if (!_.isEqual(newGroup.path, oldGroup.path) || newGroup.row !== oldGroup.row) {
                        updateActivities({
                            action: 'movingCard',
                            newGroup: getGroupInfo(newGroup.path).name,
                            oldGroup: getGroupInfo(oldGroup.path).name
                        });
                    }
                }
            }
        });
        //endregion

        //region Remove group
        function removeGroup(hitTestInfo) {
            var len = dataview.data.groups.length;
            if (len <= 1) {
                return;
            }
            var path = hitTestInfo.groupInfo.path;
            var groupInfo = getGroupInfo(path);
            var groupDescriptors = dataview.data.groupDescriptors;
            _.remove(groupDescriptors[0].preDefinedGroups, function(value) {
                return value === groupInfo.name;
            });

            dataview.data.suspendRefresh();
            dataview.data.groupDescriptors = groupDescriptors;
            var indexes = _.map(groupInfo.getItems(), 'sourceIndex');
            indexes = indexes.sort(function(a, b) {
                return b - a;
            });
            _.each(indexes, function(index) {
                dataview.data.removeDataItems(index);
            })
            dataview.data.refresh();
            dataview.data.resumeRefresh();
            dataview.data.removeEmptyGroup();
            TrellisGrouping.options.panelUnitWidth = getPanelWidth();
            dataview.invalidate();
            updateActivities({
                action: 'removeGroup',
                group: groupInfo.name
            })
        }

        //endregion

        window.addEventListener('resize', function() {
            TrellisGrouping.options.panelUnitWidth = getPanelWidth();
            dataview.invalidate();
        });
    })
});

//region Activities

function showMenuPanel() {
    $('#trellis-menu').toggleClass('hide');
}
function commitNewGroup() {
    var nameInput = document.getElementById('add-group-container').querySelector('input');
    var name = nameInput.value;
    if (!name) {
        return;
    }
    name = name.toUpperCase();
    var groups = dataview.data.groups;
    var existedGroup = _.find(groups, function(group) {
        return group.name === name;
    });

    if (!existedGroup) {
        var descriptors = dataview.data.groupDescriptors;
        if (descriptors.length) {
            if (descriptors[0].preDefinedGroups) {
                descriptors[0].preDefinedGroups.push(name);
            } else {
                descriptors[0].preDefinedGroups = [name];
            }
        }
        TrellisGrouping.options.panelUnitWidth = getPanelWidth();

        dataview.data.groupDescriptors = descriptors;
        updateActivities({
            action: 'addGroup',
            group: name
        });
        dataview.invalidate();
    }
}

function updateActivities(activity) {
    var content = '';
    var action = activity.action;
    switch (action) {
        case 'addCard':
            content = 'Add card ' + activity.value + ' to group ' + activity.group;
            break;
        case 'removeCard':
            content = 'Remove card ' + activity.value + ' from group ' + activity.group;
            break;
        case 'editCard':
            var oldValue = activity.oldValue;
            var value = activity.value;
            content = 'Edit card "<i>' + oldValue.description + '</i>":<br />';
            if (oldValue.priority !== value.priority) {
                content += ' change priority from ' + oldValue.priority + '  to ' + value.priority + '<br />';
            }
            if (oldValue.description !== value.description) {
                content += ' change description to "' + value.description + '"<br />';
            }
            if (oldValue.evaluation !== value.evaluation) {
                content += ' change evaluation from ' + oldValue.evaluation + '  to ' + value.evaluation + '<br />';
            }
            break;
        case 'movingCard':
            content += 'Move item from group ' + activity.oldGroup + ' to group ' + activity.newGroup;
            break;
        case 'addGroup':
            content += '<b>Add group </b>' + activity.group;
            break;
        case 'removeGroup':
            content += '<b>Remove group </b>' + activity.group;
            break;
    }

    var trellisMenu = document.getElementById('trellis-menu');

    var activityContainer = trellisMenu.querySelector('.trellis-menu-activities');
    if (!activityContainer) {
        var trellisBody = trellisMenu.querySelector('.trellis-menu-body');
        trellisBody.appendChild(createElement('<div class="activities-container"><div class="split-line"></div>' +
        '<div class="trellis-menu-group-label">Activity</div>' +
        '<div class="trellis-menu-activities"></div></div>'));
        activityContainer = trellisMenu.querySelector('.trellis-menu-activities');
    }
    var children = activityContainer.children;
    var element;
    if (children.length) {
        element = createElement('<div><span>' + content + '</span><div class="split-line"></div><div>');
        activityContainer.insertBefore(element, children[0]);
    } else {
        element = createElement('<div><span>' + content + '</span><div>');
        activityContainer.appendChild(element);
    }

}
//endregion

//region Helper
function createElement(html) {
    var div = document.createElement('div');
    div.innerHTML = html;
    var r = div.children[0];
    div = null;
    return r;
}

function getGroupInfo(path) {
    var groupInfos = dataview.data.groups;
    var currentGroup = groupInfos[path[0]];
    for (i = 1, len = path.length; i < len; i++) {
        if (currentGroup && currentGroup.groups) {
            currentGroup = currentGroup.groups[path[i]];
        } else {
            return null;
        }
    }
    return currentGroup;
}

function hasClass(e, className) {
    if (e && e.getAttribute) {
        var name = ' ' + className + ' ';
        return (' ' + e.getAttribute('class') + ' ').replace(/[\t\r\n\f]/g, ' ').indexOf(name) >= 0;
    }
    return false;
}

function addClass(e, className) {
    if (e && e.setAttribute && !hasClass(e, className)) {
        var cn = e.getAttribute('class');
        e.setAttribute('class', cn ? cn + ' ' + className : className);
    }
}

function removeClass(e, className) {
    if (e && e.setAttribute && hasClass(e, className)) {
        var rx = new RegExp('\\s?\\b' + className + '\\b', 'g');
        var cn = e.getAttribute('class');
        e.setAttribute('class', cn.replace(rx, ''));
    }
}

function getPanelWidth() {
    var windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    return Math.min(300, Math.max(100, parseInt((windowWidth - 50) / 4)));
}

function encodeValue(value) {
    var encodeTextarea = document.createElement('textarea');
    encodeTextarea.textContent = value;
    return encodeTextarea.innerHTML;
}
//endregion
