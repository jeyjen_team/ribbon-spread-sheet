var baseUrl = '//services.odata.org/V3/Northwind/Northwind.svc/';
var searchUrl;
var tableName;
var selectTop = 20;
var orderBys = [];
var selectedColumns = [];
var dataView;
var metaData = [];
var nodeName = isMSBrowser() ? 'childNodes' : 'children';
var parentName = isMSBrowser() ? 'parentNode' : 'parentElement';
window.onload = function() {
    beginLoad('Getting metadata from ' + window.location.protocol + baseUrl);
    $.ajax({
        url: '//services.odata.org/V3/Northwind/Northwind.svc/$metadata',
        crossDomain: true,
        success: function(data) {
            if (isMSBrowser()) {
                data = data.childNodes[0];
            }
            var entityTypes = [];
            var entitySets = [];
            getEntityInfo(data, entityTypes, entitySets);
            getMetaDataData(entityTypes, entitySets);
            var dataSource = document.getElementById('dataSource');
            _.each(metaData, function(md) {
                if (dataSource) {
                    dataSource.appendChild(createElement('<option  class="queryDropdownColor">' + md.tableName + '</option>'));
                }
            });
            endLoad();
        },
        error: function(xhr, status) {
            endLoad();
            if (status !== 'abort') {
                alert('Failed to load data from remote web site.');
            }
        }
    });
};

function getMetaDataData(entityTypes, entitySets) {
    for (var i = 0, len1 = entitySets.length; i < len1; i++) {
        var entitySet = entitySets[i];
        for (var j = 0, len2 = entityTypes.length; j < len2; j++) {
            var entityType = entityTypes[j];
            if (entitySet.EntityType === entityType.Name) {
                metaData.push({
                    tableName: entitySet.Name,
                    tableType: entityType.Name,
                    columns: entityType.columns
                });
            }
        }
    }
}

function isMSBrowser() {
    var isIE = /*@cc_on!@*/ false || !!document.documentMode;
    // Edge 20+
    var isEdge = !isIE && !!window.StyleMedia;
    return isIE || isEdge;
}

function getEntityInfo(data, entityTypes, entitySets) {
    var subData;
    var nameSpace;
    var type;
    var set;
    for (var i = 0, len = data.childElementCount; i < len; i++) {
        subData = data[nodeName][i];
        if (subData.nodeName == 'EntityType') {
            nameSpace = readNode(subData[parentName], null, true);
            type = {};
            readEntityType(subData, type, nameSpace);
            entityTypes.push(type);
        } else if (subData.nodeName === 'EntitySet') {
            set = {};
            readNode(subData, set);
            entitySets.push(set);
        } else {
            getEntityInfo(subData, entityTypes, entitySets);
        }
    }
}

function readEntityType(entityType, node, nameSpace) {
    readNode(entityType, node);
    node.Name = nameSpace + '.' + node.Name;
    node.columns = [];
    for (var i = 0, len = entityType.childElementCount; i < len; i++) {
        var column = {};
        if (entityType[nodeName][i].nodeName === 'Property') {
            readNode(entityType[nodeName][i], column);
            node.columns.push(column);
        }
    }
}

function readNode(xmlNode, node, onlyNameSpace) {
    for (var i = 0, len = xmlNode.attributes.length; i < len; i++) {
        var attribute = xmlNode.attributes[i];
        var proName = attribute['nodeName'];
        var proValue = attribute['nodeValue'];
        if (onlyNameSpace) {
            if (proName === 'Namespace') {
                return proValue;
            }
        } else {
            node[proName] = proValue;
        }
    }
}

$('#selectConditions #addSelectCondition').click(function(sender) {
    addRemoveClass($('#selectConditions'), 'listVisible');
});

$('#orderByConditions #addOrderByCondition').click(function(sender) {
    addRemoveClass($('#orderByConditions'), 'listVisible');
});

$('#top').val(selectTop.toString());

$('#dataSource').change(function(sender) {
    $('.hidden').removeClass('hidden');
    var dataSource = document.getElementById('dataSource');
    tableName = dataSource.options[dataSource.selectedIndex].value;
    updateConditions();
    updateUrl();

    var promtInfo = document.getElementById('promtInfo');
    if (promtInfo) {
        promtInfo.parentNode.removeChild(promtInfo);
    }
});

$('#top').change(function() {
    var selectRuls = document.getElementById('top');
    selectTop = parseInt(selectRuls.options[selectRuls.selectedIndex].getAttribute('value'));
    updateUrl();
});

$('#submitQuery').click(function() {
    searchUrl =  window.location.protocol + searchUrl;
    beginLoad('Getting data from ' + searchUrl);
    OData.defaultHttpClient.enableJsonpCallback = true;
    OData.read(searchUrl, function(data) {
        var sourceData = [];
        if (isMSBrowser()) {
            sourceData = data.results;
        } else {
            var pros = [];
            for (var pro in data.results[0]) {
                if (!_.startsWith(pro, '_')) {
                    pros.push(pro);
                }
            }
            _.each(data.results, function(r) {
                sourceData.push(_.pick(r, pros));
            });
        }
        if (sourceData.length > 0) {
            var containerHeight = 24 * sourceData.length + 26;
            $('#grid1')[0].style.height = containerHeight + 'px';
            destroyDataView();
            dataView = new GC.Spread.Views.DataView(document.getElementById('grid1'), sourceData, null, new GC.Spread.Views.Plugins.GridLayout());

            //focus data.view by default
            document.querySelector('#grid1').focus();
        }
        endLoad();
    }, function(err) {
        endLoad();
        alert('Failed to get data from ' + searchUrl);
    });
});

$('#clearQuery').click(function() {
    initElement();
    updateConditions();
    updateUrl();
});

function updateConditions() {
    initElement();
    for (var i = 0, len = metaData.length; i < len; i++) {
        if (metaData[i].tableName === tableName) {
            _.each(metaData[i].columns, function(col) {
                appendColumnElement(col.Name);
                appendOrderByElement(col.Name);
            });

            registerEvent();
            break;
        }
    }
}

function registerEvent() {
    $('#orderByFiltersList .queryOrderByValue').click(handleSortButtonClick);
    $('#selectFiltersList label').click(handleCheckBoxClick);
}

function initElement() {
    $('#selectFiltersList').empty();
    $('#orderByFiltersList').empty();
    $('#queryUrlText').empty();
    $('#selectConditions')[0].className = 'filterContainer';
    $('#orderByConditions')[0].className = 'filterContainer';
    destroyDataView();
    orderBys = [];
    selectedColumns = [];
}

function destroyDataView() {
    if (dataView) {
        dataView.destroy();
        dataView = null;
    }
}

function appendColumnElement(pro) {
    var ele = createElement('<label><input type="checkbox">' + pro + '</label>');
    document.getElementById('selectFiltersList').appendChild(ele);
}

function appendOrderByElement(pro) {
    var ele = '<label><div class="queryOrderByValueGroup btn-group" data-toggle="buttons">' + '<div data-toggle="radio" name="queryOrderByValueGroup" class="queryOrderByValue btn btn-sm btn-info" data-value="0" data-id="1"><i class="fa fa-lg fa-times"></i></div>' + '<div data-toggle="radio" name="queryOrderByValueGroup" class="queryOrderByValue btn btn-sm btn-info" data-value="1" data-id="1"><i class="fa fa-lg fa-sort-alpha-asc"></i></div>' + '<div data-toggle="radio" name="queryOrderByValueGroup"  class="queryOrderByValue btn btn-sm btn-info" data-value="2" data-id="1"><i class="fa fa-lg fa-sort-alpha-desc"></i></div>' + '</div>' + pro + '</label>';
    var sortEle = createElement(ele);
    document.getElementById('orderByFiltersList').appendChild(sortEle);
}

function handleCheckBoxClick(sender, args) {
    var labelEle = sender.srcElement.tagName === 'INPUT' ? sender.srcElement.parentElement : null;
    if (labelEle) {
        var checked = labelEle.children[0].checked;
        var columnName = labelEle.textContent;
        if (checked) {
            selectedColumns.push(columnName)
        } else {
            selectedColumns.splice(selectedColumns.indexOf(columnName), 1);
        }
        updateUrl();
    }
}

function handleSortButtonClick(sender, args) {
    var target = sender.currentTarget;
    var columnName = findLabel(target).textContent;
    _.remove(orderBys, function(order) {
        return order.column === columnName;
    });
    if (parseInt(target.attributes['data-value'].value) === 1) {
        orderBys.push({
            column: columnName
        });
    } else if (parseInt(target.attributes['data-value'].value) === 2) {
        orderBys.push({
            column: columnName,
            desc: true
        });
    }

    updateUrl();
}

function findLabel(element) {
    while (element.parentElement.tagName !== 'LABEL') {
        element = element.parentElement
    }

    return element.parentElement;
}

function addRemoveClass(element, className) {
    if (element[0].className.indexOf(className) > -1) {
        element.removeClass(className);
    } else {
        element.addClass(className);
    }
}

function updateUrl() {
    var block = $('#queryUrlText')[0];
    var selectCondition = '';
    var orderByCondtions = '$orderby=';
    var columnConditions = '$select=';
    var conditionCount = 0;
    var url = baseUrl;
    if (tableName) {
        url += tableName;
    }

    if (selectTop === 100000) {
        selectCondition += '/$count?';
    } else if (selectTop === 0) {
        selectCondition += '()?';
    } else {
        selectCondition += '?$';
        selectCondition += 'top=' + selectTop;
        selectCondition += '&';
    }
    url += selectCondition;

    if (orderBys.length > 0) {
        conditionCount++;
        _.each(orderBys, function(order) {
            orderByCondtions += order.column;
            if (order.desc) {
                orderByCondtions += ' desc';
            }
            orderByCondtions += ',';
        });
        orderByCondtions = _.trim(orderByCondtions, ',');
        url += orderByCondtions;
    }

    if (selectedColumns.length > 0) {
        conditionCount++;
        _.each(selectedColumns, function(column) {
            columnConditions += column;
            columnConditions += ',';
        });
        columnConditions = _.trim(columnConditions, ',');
        if (conditionCount > 1) {
            url += '&';
        }
        url += columnConditions;
    }

    url = _.trim(url, '?');
    url = _.trim(url, '&');
    searchUrl = url;
    block.value = url;
}

function createElement(html) {
    var div = document.createElement('div');
    div.innerHTML = html;
    var r = div.children[0];
    div = null;
    return r;
}

function beginLoad(text) {
    var icon = document.getElementById('custom-loadIcon');
    if (icon) {
        icon.style.display = '';
    }

        var background = createElement('<div id="backgroundOverlay" class="gc-popup-overlay"><div style="position: fixed;left:45%;top:45%"><div class="sk-cube-grid">' +
                '<div class="sk-cube sk-cube1"></div>' +
                '<div class="sk-cube sk-cube2"></div>' +
                '<div class="sk-cube sk-cube3"></div>' +
                '<div class="sk-cube sk-cube4"></div>' +
                '<div class="sk-cube sk-cube5"></div>' +
                '<div class="sk-cube sk-cube6"></div>' +
                '<div class="sk-cube sk-cube7"></div>' +
                '<div class="sk-cube sk-cube8"></div>' +
                '<div class="sk-cube sk-cube9"></div>' +
                '</div>' +
                '<h3 style="left:-35%;position: relative;color:white">' + text + '</h3>' +
            '</div></div>');

    $('#queryBuilderContainer').append(background);
}

function endLoad() {
    var icon = document.getElementById('custom-loadIcon');
    if (icon) {
        icon.style.display = 'none';
    }
    $('#backgroundOverlay').remove();
}
