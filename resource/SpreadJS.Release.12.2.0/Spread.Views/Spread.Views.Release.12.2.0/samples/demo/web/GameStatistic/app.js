var firstNames = ["Sophie", "Isabelle", "Emily", "Olivia", "Lily", "Chloe", "Isabella",
    "Amelia", "Jessica", "Sophia", "Ava", "Charlotte", "Mia", "Lucy", "Grace", "Ruby",
    "Ella", "Evie", "Freya", "Isla", "Poppy", "Daisy", "Layla"
];
var lastNames = ["Beckham", "Black", "Braxton", "Brennan", "Brock", "Bryson", "Cadwell",
    "Cage", "Carson", "Chandler", "Cohen", "Cole", "Corbin", "Dallas", "Dalton", "Dane",
    "Donovan", "Easton", "Fisher", "Fletcher", "Grady", "Greyson", "Griffin", "Gunner",
    "Hayden", "Hudson", "Hunter", "Jacoby", "Jagger", "Jaxon", "Jett", "Kade", "Kane",
    "Keating", "Keegan", "Kingston", "Kobe"
];
var games = ["Chess", "Cross and Circle game", "Downfall", "DVONN", "Fanorona", "Game of the Generals", "Ghosts",
    "Abalone", "Agon", "Backgammon", "Battleship", "Blockade", "Blood Bowl", "Bul", "Camelot", "Checkers",
    "Go", "Gipf", "Guess Who?", "Hare and Hounds", "Hex", "Hijara", "Isola", "Janggi (Korean Chess)", "Le Jeu de la Guerre",
    "Patolli", "Plateau", "Rithmomachy", "Senet", "Shogi", "Space Hulk", "Stratego", "Sugoroku",
    "Tablut", "Tantrix", "Wari", "Xiangqi (Chinese chess)", "YINSH", "Kalah", "Kamisado", "Liu po",
    "Lost Cities", "Mad Gab", "Master Mind", "Nine Men's Morris", "Obsession", "Othello"
];
var booleanValues = [true, false, null];
var countries = ["UK", "France", "Italy", "Germany"];
var languages = ["English", "French", "Italian", "(other)"];
var photos = ["https://flags.fmcdn.net/data/flags/mini/gb.png", "https://flags.fmcdn.net/data/flags/mini/fr.png",
    "https://flags.fmcdn.net/data/flags/mini/it.png", "https://flags.fmcdn.net/data/flags/mini/de.png"
];
var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

function createData() {
    var data = [];
    for (var i = 0; i < 1000; i++) {
        var rand = Math.round(Math.random() * 100);
        var tpData = {
            firstName: firstNames[rand % firstNames.length],
            lastName: lastNames[rand % lastNames.length],
            country: countries[rand % countries.length],
            language: languages[rand % languages.length],
            photo: photos[rand % photos.length],
            game: games[rand % games.length],
            bought: booleanValues[rand % booleanValues.length],
            rating: "rating" + (rand % 6)
        };
        _.each(monthNames, function(name) {
            tpData[name] = Math.round(Math.random() * 100000);
        });

        data.push(tpData);
    }
    return data;
}

function monthPresenter(str) {
    return "<div class='div' style='background-color:{{? it." + str + "<10000}}lightcoral{{?? it." + str + ">50000}}lightgreen{{?}}'>" + "${{=it." + str + "}}</div>";
}

var photoPresenter = '<img class="photo"  src="{{=it.photo}}">{{=" "+it.country}}</img>';
var starPresenter = '<div class="stars-box {{=it.rating}}"></div>';
var columns = [{
        caption: "Participant",
        columns: [
            { id: 'name', caption: 'Name', dataField: 'firstName,lastName', width: 120 },
            { id: 'country2', caption: 'Country', dataField: 'photo,country', presenter: photoPresenter, width: 100 },
            { id: 'language', caption: 'Language', dataField: 'language', width: 100 }
        ]
    }, {
        caption: "Game of Choice",
        columns: [
            { id: 'game', caption: 'Game of Choice', dataField: "game", width: 160 }, {
                id: "bought",
                caption: "Bought",
                dataField: "bought",
                presenter: '<span>{{?it.bought<1}}✔{{??it.bought==1}}✖{{?}}</span>',
                cssClass: "align-center",
                width: 60
            }
        ]
    },
    { id: 'rating', caption: "Rating", dataField: 'rating', width: 100, presenter: starPresenter }, {
        caption: "Monthly",
        isCollapsed: false,
        columns: createMonthColumns()
    }
];
var dataView = new GC.Spread.Views.DataView(document.getElementById('grid1'), createData(), columns, new GC.Spread.Views.Plugins.GridLayout({
    selectionMode: 'multiple',
    allowHeaderSelect: true,
    showToolPanel: false
}));

function createMonthColumns() {
    var columns = [];
    columns.push({
        id: 'totalWinnings',
        headerGroupShow: 'collapsed',
        caption: "Total Winnings",
        cssClass: 'align-right',
        dataField: '=[Jan]+[Feb]+[Mar]+[Apr]+[May]+[Jun]+[Jul]+[Aug]+[Sep]+[Oct]+[Nov]+[Dec]',
        format: '$#,#',
        width: 120
    });

    _.each(monthNames, function(name) {
        columns.push({
            id: name,
            caption: name,
            dataField: name,
            headerGroupShow: "expanded",
            presenter: monthPresenter(name),
            width: 70
        });
    });
    return columns;
}

function showToolPanel() {
    dataView.options.showToolPanel = !dataView.options.showToolPanel;
}

function filter() {
    var filterArray = ["firstName", "lastName", "country", "language", "game", "bought", "rating"];
    var filterInput = document.getElementById("input");
    var value = filterInput.value;
    var values = value ? value.split(/\s/).filter(function(str) {
        return str.length > 0;
    }) : [];
    var fragmentArr = [];

    var tempArr = [];
    values.forEach(function(value) {
        filterArray.forEach(function(fieldName) {
            tempArr.push('search("' + value + '",[' + fieldName + '],1,-1) >0');
        });
        fragmentArr.push('(' + tempArr.join('||') + ')');
        tempArr.length = 0;
    });

    dataView.data.filter(fragmentArr.join('&&')).do();

    filterInput.focus();
}

//focus data.view by default
document.querySelector('#grid1').focus();
