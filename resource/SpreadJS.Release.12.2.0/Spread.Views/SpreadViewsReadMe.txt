Unzip the evaluation or production zip file to use the product.

Main folder - This folder contains the dataview script and css files.
--common - This folder contains the common script file.
--locale - This folder contains the script files for different locales.
--plugins - This folder contains additional script files.
--theme - This folder contains additional theme css files.
--samples - This folder contains samples.

Main web site:
http://www.gcpowertools.com.cn/products/spreadjs/

Documentation:
http://www.gcpowertools.com.cn/docs/spreadstudio/spreadjs.htm

For technical support, refer to:
Product forum at http://gcdn.gcpowertools.com.cn/forum.php
Email sales.xa@grapecity.com 