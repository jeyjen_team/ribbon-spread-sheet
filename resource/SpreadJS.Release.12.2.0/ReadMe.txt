This zip file contains the following files. Unzip the appropriate zip file to use the product.

Spread.Sheets.Docs.x.x.x.zip - Spread.Sheets Document.
Spread.Views.Docs.x.x.x.zip - Spread.Views Document.
Spread.Sheets-Designer-x.x.x.dmg - Spread.Sheets Designer for Mac.
Spread.Sheets-Designer-x.x.x-x86_64.AppImage - Spread.Sheets Designer for Linux.
Spread.Sheets-Designer-Setup.x.x.x.exe - Spread.Sheets Designer for Windows.
Spread.Sheets.Release.x.x.x.zip - Spread.Sheets package.
Spread.Views.Release.x.x.x.zip - Spread.Views package.

Main web site:
http://www.gcpowertools.com.cn/products/spreadjs/

Documentation:
http://www.gcpowertools.com.cn/docs/spreadstudio/spreadjs.htm

For technical support, refer to:
Product forum at http://gcdn.gcpowertools.com.cn/forum.php
Email spread.xa@grapecity.com 